﻿using UnityEngine;

public static class Network {
    public static bool IsInternetAvaiable => Application.internetReachability != NetworkReachability.NotReachable;
}