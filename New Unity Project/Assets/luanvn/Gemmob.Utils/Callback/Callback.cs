﻿using System;
using System.Threading.Tasks;
using UnityEngine;

public class Callback {
    /**<summary> Normal callback to prevent crash if any null error occur </summary> */
    public static void Call(Action callback) {
        if (callback == null) return;
        try { callback.Invoke(); }
        catch { throw; }
    }

    /**<summary> Normal callback to prevent crash if any null error occur </summary> */
    public static void Call<T>(Action<T> callback, T value) {
        if (callback == null) return;
        try { callback.Invoke(value); }
        catch { throw; }
    }

    #region Schedule callback
    /**<summary> Use this if callback from an other Thread like Async </summary> */
    public static void CallSchedule(Action callback, float delayTime = 0, Action finish = null) {
        if (callback == null) return;
        Excutor.Schedule(() => {
            try {
                callback.Invoke();
                if (finish != null) finish.Invoke();
            }
            catch { throw; }
        }, delayTime);
    }

    /**<summary> Use this if callback from an other Thread like Async </summary> */
    public static void CallSchedule<T>(Action<T> callback, T value, float delayTime = 0, Action finish = null) {
        if (callback == null) return;
        Excutor.Schedule(() => {
            try {
                callback.Invoke(value);
                if (finish != null) finish.Invoke();
            }
            catch { throw; }
        }, delayTime);
    }
    #endregion
}