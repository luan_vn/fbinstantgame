﻿using System.Collections;
using System.Collections.Generic;
using DG.Tweening;
using Gemmob;
using UnityEngine;

public class BackgroundIngame3 : BackgroundIngameWithPath {
	[SerializeField] private BackgroundObject _meteor;
	[SerializeField] private List<BackgroundObject> _meteors;

	protected override void SetBg() {
		for (int i = 0; i < _layers.Count; i++) {
			_layers[i].SetSpeed(_layerInfors[i].Speed, _layerInfors[i].GameSpeed);
		}

		for (int i = 0; i < _objs.Count; i++) {
			_objs[i].SetSpeed(_objSpeed[i]);
			if (_objs[i].ObjMoveType == BackgroundObject.MoveType.Path) {
				_meteor = _objs[i];
			}
		}

        if(_meteor != null) {
            _meteors.Add(_meteor);
            _meteor.gameObject.SetActive(false);
            MoveMeteor();
        }
	}

	void MoveMeteor() {
		int meteorNum = Random.Range(1, 4);
		for (int i = 0; i < meteorNum; i++) {
			//BackgroundObject newMeteor = GameConfig.Instance.ItemPool(_meteor, _meteors, transform);
			//float speed = Random.Range(_meteor.Speed - 0.75f, _meteor.Speed + 0.75f);
			//SetPath(newMeteor.transform, speed);
		}
		StartCoroutine(WaitNextMeteor(5));
	}

	protected override void SetPath(Transform ship, float speed) {
		bool isFlip = Random.value > 0.5f;
		int pathIndex = Random.Range(0, _pathId.Count);
		PathManager.Points path = PathManager.Instance.Paths[_pathId[pathIndex]];
		float delta = Random.Range(-3f, 3f);
		List<Vector3> pos = new List<Vector3>();
		if (isFlip) {
			path.FlipXPositions.ForEach(point => { pos.Add(point + new Vector3(0, delta, 0)); });
		}
		else {
			path.Positions.ForEach(point => { pos.Add(point + new Vector3(0, delta, 0)); });
		}
		ship.position = pos[0];
		ship.DOPath(pos.ToArray(), speed, PathType.CatmullRom, PathMode.TopDown2D).SetSpeedBased(true)
			.SetEase(Ease.Linear).SetLookAt(0.1f).OnComplete(
				() => { ship.gameObject.SetActive(false); });
	}

	IEnumerator WaitNextMeteor(float time) {
		yield return Yielder.Wait(time);
		MoveMeteor();
	}
}
