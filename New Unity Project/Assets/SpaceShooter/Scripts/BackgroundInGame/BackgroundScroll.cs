﻿using DG.Tweening;
using UnityEngine;

public class BackgroundScroll : MonoBehaviour {
	private Renderer _render;
	private float _speed;

	private float _gameSpeed = 1;

	private readonly string _mainTex = "_MainTex";

	public void AssignVariable(Sprite sprite, float speed = 0, float gameSpeed = 0) {
		_render.material.mainTexture = sprite.texture;
		_speed = speed;
		_gameSpeed = gameSpeed;
	}

	public void AssignVariable(float speed = 0, float gameSpeed = 0) {
		_speed = speed;
		_gameSpeed = gameSpeed;
		DOVirtual.Float(_gameSpeed, 1, 1.5f, value => { _gameSpeed = value; });
	}

	void OnEnable() {
		_render.material.SetTextureOffset(_mainTex, Vector2.zero);
	}

	void Update() {
		_render.material.SetTextureOffset(_mainTex,
			_gameSpeed * _speed * Vector2.up * Time.deltaTime + _render.material.GetTextureOffset(_mainTex));
	}
}