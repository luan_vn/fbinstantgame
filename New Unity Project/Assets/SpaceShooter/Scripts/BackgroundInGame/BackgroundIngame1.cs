﻿using System.Collections;
using System.Collections.Generic;
using DG.Tweening;
using UnityEngine;

public class BackgroundIngame1 : BackgroundIngameWithPath {

	protected override void SetPath(Transform ship, float speed) {
		bool isFlip = Random.value > 0.5f;
		int pathIndex = Random.Range(0, _pathId.Count);
		List<Vector3> movePath = new List<Vector3>(isFlip
			? PathManager.Instance.Paths[_pathId[pathIndex]].FlipXPositions
			: PathManager.Instance.Paths[_pathId[pathIndex]].Positions);
		ship.position = movePath[0];
		ship.DOPath(movePath.ToArray(), speed, PathType.CatmullRom, PathMode.TopDown2D).SetSpeedBased(true)
			.SetEase(Ease.Linear).SetLookAt(0.1f).OnComplete(
				() => { StartCoroutine(WaitNextPath(ship, speed, 2)); });
	}

	void OnDisable() {
		StopAllCoroutines();
	}
}
