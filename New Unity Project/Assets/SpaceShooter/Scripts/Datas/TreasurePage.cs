﻿
using Gemmob;
using Polaris.Tutorial;
using Spine.Unity;
using System;
using UnityEngine;
using UnityEngine.UI;

public class TreasurePage : Frame {
    public const string showCard = "EarnedCardShow";
    private const string OPENED_FREE_KEY = "OpenedFreeKey";

    private const int _treasureNum = 3;
    private const int _rewardNum = 5;

    [Header("Default sprite")]
    public Sprite coinEarn;
    public Sprite gemEarn;
    public Sprite fuelEarn;
    public TreasureType selectedType;

    [Header("For open")]
    public GameObject openedPanel;
    public GameObject earnedCardSet;
    public GameObject openedEffect;
    public GameObject OpeningEffect;

    [Header("Main box")]
    public SkeletonGraphic showIcon;
    public SkeletonAnimation openedIcon;

    //[Header("Array reward")]
    //public TreasureCell[] arrCell = new TreasureCell[_treasureNum];
    public Transform SelectFrame;
    //[HideInInspector]
    //public EarnedCard[] arrEarnedCard = new EarnedCard[_rewardNum];

    [Header("Show information")]
    public Text txtPrice;
    public int countCard;
    //public ShowTreasureReward showRewards;
    public LocalCountDown CountDownBox;
    private bool _isGetTarget;
    private bool _isOpenedLegendCard;

    [Header("Set button")]
    [SerializeField] private ButtonAnimation _btnOpen;
    [SerializeField] private ButtonAnimation _btnBuy;
    [SerializeField] private ButtonAnimation _btnClaim;
    [SerializeField] private Text _txtBoxName;
    [SerializeField] private ButtonAnimation btnOpenAll;
    //[SerializeField] private EarnedCard[] listEarnedCard;

    public int IsOpenedFree;

    protected override void OnInitialize(HUD hud) {
        CountDownBox = CountDownController.Instance.CountDownBox;
        IsOpenedFree = PlayerPrefs.GetInt(OPENED_FREE_KEY);
        CheckOpen(TreasureType.Bad);
        GetPrice(TreasureType.Bad);
        GetEarnedCard();
        _btnBuy.onClick.AddListener(BuyMore);
        _btnOpen.onClick.AddListener(Open);
        _btnClaim.onClick.AddListener(Claim);
        btnOpenAll.onClick.AddListener(OpenAll);
        base.OnInitialize(hud);
    }

    void OnEnable() {
        CheckOpen(selectedType);
        _isOpenedLegendCard = false;
    }

    public void CheckOpen(TreasureType type) {
        if(ManagerData.Instance.Inventory.Treasures[(int)type].num <= 0) {
            _btnOpen.GetComponent<Image>().sprite = DataConfig.Instance.BtnDeactiveGray;
            _btnOpen.interactable = false;
        }
        else {
            _btnOpen.GetComponent<Image>().sprite = DataConfig.Instance.BtnActiveYellow;
            _btnOpen.interactable = true;

        }
    }

    public void GetPrice(TreasureType type) {
        //txtPrice.text = GameData.Instance.TreasureData.lsTreasureInfor[(int)type].price.ToString();
        int price = ManagerData.Instance.TreasureData.lsTreasureInfor[(int) type].price;
        txtPrice.text = PriceShower.ShowPrice(price);
    }

    void GetEarnedCard() {
        //for (int i = 0; i < _rewardNum; i++)
        //arrEarnedCard[i] = earnedCardSet.transform.GetChild(i).GetComponent<EarnedCard>();
    }

    void Claim() {
        OpeningEffect.SetActive(true);
        ManagerData.Instance.BackupData.AddCurrency();
        //Ani.Play(GameConfig.PageShowAnim);
        openedEffect.SetActive(false);
        //openedIcon.AnimationState.SetAnimation(0, openedIcon.AnimationName, false);
        openedIcon.Initialize(true);
        //openedIcon.AnimationState.Event += openedIcon.GetComponent<OpenTreasureEvent>().HandleEvent;
        countCard = 0;
        openedPanel.SetActive(false);
        _btnClaim.gameObject.SetActive(false);
        //for (int i = 0; i < _rewardNum; i++)
        //arrEarnedCard[i].backSide.SetActive(true);
        //UIManager.Instance.ProfilePage.GetAllCard();
        //if (_isOpenedLegendCard) {
        //	RatingController.Instance.ShowRatingPopup();
        //}
    }

    void OpenAll() {
        //foreach(var card in listEarnedCard)
        //{
        //	card.Flip();
        //}
    }

    void Open() {
        TreasureInfor infor = ManagerData.Instance.TreasureData.lsTreasureInfor[(int) selectedType];
        PlayerTreasure thisTreasure = ManagerData.Instance.Inventory.Treasures[(int) selectedType];
        if(thisTreasure.num > 0) {
            _isGetTarget = false;
            GetReward(selectedType, infor.lsRate[thisTreasure.time].ToArray(), infor.earnedTargetRate.ToArray());
            thisTreasure.num -= 1;
            if(!_isGetTarget) {
                thisTreasure.time += 1;
            }

            CheckOpen(selectedType);
            //arrCell[(int) selectedType].ShowNumber(thisTreasure.num);
            ManagerData.Instance.Inventory.SaveData();
            openedPanel.SetActive(true);
            if(selectedType == TreasureType.Bad && IsOpenedFree == 0) {
                ManagerData.Instance.Inventory.FirstTimeOpenBox();
                //arrCell[(int) TreasureType.Bad].txtTime.gameObject.SetActive(true);
                IsOpenedFree = 1;
                SaveOpenedFree();
                //arrCell[(int) TreasureType.Bad].txtTime.text = string.Empty;
                CountDownBox.StartCountDown();
                //CountDownBox.OnTick += () => { arrCell[(int) TreasureType.Bad].txtTime.text = CountDownBox.GetTime(); };
            }
        }
    }

    public void AddBox(TreasureType type, int num = 1) {
        PlayerTreasure thisTreasure = ManagerData.Instance.Inventory.Treasures[(int)selectedType];
        //GameData.Instance.Inventory.AddTreasure(type, num, UIManager.Instance.TreasurePage.CurrentNameMenu());
        CheckOpen(type);
        //arrCell[(int) type].ShowNumber(thisTreasure.num);
        ManagerData.Instance.Inventory.SaveData();

    }

    void BuyMore() {
        //if (GameData.Instance.PlayerInfo.UseGem(
        //    GameData.Instance.TreasureData.lsTreasureInfor[(int)selectedType].price, GameData.Instance.TreasureData.lsTreasureInfor[(int)selectedType].name, location, canDiscount: true)) {
        //    AddBox(selectedType);
        //    GameData.Instance.isAddTreasure = false;
        //} else {
        //    //[Tracking - Position]-[MenuShowup]
        //    GameTracking.Instance.TrackMenuShowup(UIManager.Instance.Announce.CurrentNameMenu());

        //    UIManager.Instance.Announce.StartAnnounce(
        //        "You don't have enough funds! \nDo you want to get more ?", TypeReward.Gem);
        //}
    }

    int GetItemIndex(float[] rate) {
        float random = UnityEngine.Random.Range(0, 99);
        int itemIndex;
        for(itemIndex = 0; itemIndex < rate.Length; itemIndex++) {
            float a = rate[itemIndex];
            if(random < a)
                break;
            random -= a;
        }
        return itemIndex;
    }

    int GetItemIndex(int[] rate) {
        float random = UnityEngine.Random.Range(0, 100);
        //Debug.Log(random);
        int itemIndex;
        for(itemIndex = 0; itemIndex < rate.Length; itemIndex++) {
            if(random < rate[itemIndex])
                break;
            random -= rate[itemIndex];
        }
        return itemIndex;
    }

    void ShowCardInfor(int index, string name, string effect, Sprite icon, Sprite frame) {
        //arrEarnedCard[index].txtName.text = name;
        //arrEarnedCard[index].txtEffect.text = effect;
        //arrEarnedCard[index].icon.sprite = icon;
        //arrEarnedCard[index].frame.sprite = frame;
        //arrEarnedCard[index].level.gameObject.SetActive(true);
    }

    void ShowItem(int index, TreasureReward name, int number, Sprite icon) {
        //arrEarnedCard[index].txtName.text = name.ToString().ToUpper();
        //arrEarnedCard[index].txtEffect.text = name + ": + " + number;
        //arrEarnedCard[index].icon.sprite = icon;
        //arrEarnedCard[index].frame.sprite = DataConfig.Instance.BoundCards[0];
        //arrEarnedCard[index].level.gameObject.SetActive(false);
        //arrEarnedCard[index].currency = name;
    }

    void GetCard(int rewardIndex, int index) {
        int cardIndex = GetItemIndex(ManagerData.Instance.TreasureData.lsCardRate[rewardIndex].ToArray());
        int thisCard = ManagerData.Instance.CardData.CardIndex((CardType)cardIndex, (EnumDefine.StarType)(rewardIndex + 1));
        if(!_isOpenedLegendCard) {
            _isOpenedLegendCard = (EnumDefine.StarType)(rewardIndex + 1) == EnumDefine.StarType.Legend;
        }
        //Card card = new Card();
        PlayerCard playerCard = new PlayerCard(thisCard);
        //playerCard.Numeral = card.GetEffect(0).ToList();
        CardInfo cardInfor = ManagerData.Instance.CardData.GetInfo(thisCard);
        playerCard.Numeral = cardInfor.Numeral;
        ShowCardInfor(index, cardInfor.NameShow, cardInfor.Info, cardInfor.Icon,
            DataConfig.Instance.BoundCards[rewardIndex]);
        ManagerData.Instance.Inventory.AddCard(playerCard);
        //QuestController.Instance.AddAchievementProgress(QuestType.CountCard, rewardIndex);
    }

    void GetReward(TreasureType selected, float[] normalRate, float[] earnedTargetRate) {
        //bool isGetTarget = false;
        TreasureInfor infor = ManagerData.Instance.TreasureData.lsTreasureInfor[(int)selected];
        PlayerTreasure thisTreasure = ManagerData.Instance.Inventory.Treasures[(int) selected];
        for(int i = 0; i < _rewardNum; i++) {
            int rewardIndex = GetItemIndex(normalRate);
            if(_isGetTarget || thisTreasure.time >= infor.timeToGetTarget - 1) {
                if(thisTreasure.time != 0) {
                    GetCard((int)infor.target, i);
                    thisTreasure.time = 0;
                    //isGetTarget = true;
                    _isGetTarget = true;
                    continue;
                }
                rewardIndex = GetItemIndex(earnedTargetRate);
            }

            if(rewardIndex <= (int)TreasureReward.LegendCard) {
                if(rewardIndex == (int)infor.target) {
                    //isGetTarget = true;
                    _isGetTarget = true;
                    GetCard((int)infor.target, i);
                    thisTreasure.time = 0;
                }
                else {
                    GetCard(rewardIndex, i);
                }
            }
            else {
                int earned = 0;
                switch(rewardIndex) {
                    case (int)TreasureReward.Coin:
                        //print("Get coin");
                        earned = infor.CoinEarned;
                        ShowItem(i, TreasureReward.Coin, earned, coinEarn);
                        ManagerData.Instance.BackupData.GetCurrency(0, earned);
                        //GameData.Instance.PlayerInfo.AddGold(earned);
                        break;
                    case (int)TreasureReward.Gem:
                        //print("Get gem");
                        earned = infor.GemEarned;
                        ShowItem(i, TreasureReward.Gem, earned, gemEarn);
                        ManagerData.Instance.BackupData.GetCurrency(1, earned);
                        //GameData.Instance.PlayerInfo.AddGem(earned);
                        break;
                    case (int)TreasureReward.Fuel:
                        //print("Get fuel");
                        earned = infor.FuelEarned;
                        ShowItem(i, TreasureReward.Fuel, earned, fuelEarn);
                        ManagerData.Instance.BackupData.GetCurrency(2, earned);
                        //GameData.Instance.PlayerInfo.AddFuel(earned);
                        break;
                }
            }
        }
        ManagerData.Instance.Inventory.SaveData();
        ManagerData.Instance.BackupData.SaveData();
    }

    public void ChangeIcon(int skinId) {
        showIcon.initialSkinName = (skinId + 1).ToString();
        showIcon.Initialize(true);
        openedIcon.initialSkinName = (skinId + 1).ToString();
        openedIcon.Initialize(true);
        //openedIcon.AnimationState.Event += openedIcon.GetComponent<OpenTreasureEvent>().HandleEvent;
    }

    protected override void OnShow(Action onCompleted = null, bool instant = false) {
        Camera.main.orthographic = false;
        base.OnShow(onCompleted, instant);
    }

    protected override void OnHide(Action onCompleted = null, bool instant = false) {
        base.OnHide(onCompleted, instant);
        CheckShowNotifi();
        Camera.main.orthographic = true;
    }

    public void CheckShowNotifi() {
        Inventory inventory = ManagerData.Instance.Inventory;
        for(int i = 0; i < inventory.Treasures.Count; i++) {
            if(inventory.Treasures[i].num > 0) {
                //IngameNotifiController.Instance.SetActiveNotifi(NotifiKey.Treasure, true);
                return;
            }
        }

        //IngameNotifiController.Instance.SetActiveNotifi(NotifiKey.Treasure, false);
    }

    public void SetSelectedFrame(Transform parent) {
        SelectFrame.SetParent(parent);
        SelectFrame.transform.localPosition = new Vector2(0, 12);
    }

    public void GetFreeBox(Text txtTime) {
        IsOpenedFree = 0;
        CountDownBox.OnTick = null;
        txtTime.text = "Free";
        SaveOpenedFree();
    }

    void SaveOpenedFree() {
        PlayerPrefs.SetInt(OPENED_FREE_KEY, IsOpenedFree);
        PlayerPrefs.Save();
    }

    public void ChangeBoxName() {
        _txtBoxName.text = ManagerData.Instance.TreasureData.lsTreasureInfor[(int)selectedType].name;
    }
}

public enum TreasureType {
    Bad, Normal, Good
}
