﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[Serializable]
[CreateAssetMenu(fileName = "BulletLevelsPosition", menuName = "Datas/BulletLevelsPosition")]
public class BulletLevelsPositionData : ScriptableObject
{
	public List<BulletLevelByRank> BulletLvlByRank;
}

[Serializable]
public class BulletLevelByRank {
	public List<BulletLevelPositionInfor> LvlPosInfors;
	public bool IsChangeSuper;
	public bool IsChangeBulletStyle;
	public float SuperTime;

	public BulletLevelByRank(int lvlNum) {
		LvlPosInfors = new List<BulletLevelPositionInfor>();
		for (int i = 0; i < lvlNum; i++) {
			LvlPosInfors.Add(new BulletLevelPositionInfor());
		}
	}
}

[Serializable]
public class BulletLevelPositionInfor
{
    public List<BulletPositionInfor> Infors;
    public float FireRate = .2f;

	public BulletLevelPositionInfor() {
		Infors = new List<BulletPositionInfor>();
	}
}

[Serializable]
public class BulletPositionInfor
{
    public Vector3 Offset;
    public Vector3 Angle;
    public float DamScale = 1;
    public float SizeScale = 1;
	public float SpeedBullet;
	public Sprite BulletIcon;
}