﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "TextureData",menuName = "Datas/TextureData")]
public class TextureData : ScriptableObject {
	public List<TextureInfor> AllTexture = new List<TextureInfor>();
}

[System.Serializable]
public class TextureInfor {
	public int Id;
	public string Name;
	public Sprite Sprite;
}
