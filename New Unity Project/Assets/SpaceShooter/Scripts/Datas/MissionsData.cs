﻿using Gemmob;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[Serializable]
[CreateAssetMenu(fileName = "MissionsData", menuName = "Data/MissionsData")]
public class MissionsData : ScriptableObject {
    private static MissionsData instance = null;
    public List<MissionData> missions = new List<MissionData>();
    public static MissionsData Instance {
        get {
            if(instance == null) {
                instance = Resources.Load<MissionsData>("MissionsData");
                return instance;
            }
            return instance;
        }
    }
}

[Serializable]

public class MissionData {
    [SerializeField] private EnumDefine.TypeMission typeMission = EnumDefine.TypeMission.Mission;
    public List<QuestData> quests = new List<QuestData>();

    public MissionData(List<QuestData> quests) {
        this.quests = quests;
    }

    public EnumDefine.TypeMission TYPE => typeMission;
}

[Serializable]
public class QuestData {
    public string Name;
    [SerializeField] private string id = string.Empty;
    public QuestType Type;
    public int ExtendId;
    public int Lvl;
    public bool IsAddByLvl;
    public List<QuestInfor> Infor = new List<QuestInfor>();

    public string ID => id;

    public bool IsDone(int idQuest) {
        return idQuest == Infor.Count;
    }
}

[Serializable]
public class QuestInfor {
    public string Description;
    public int Needed;
    public EnumDefine.IDCurrency RewardType;
    public int RewardNum;

    public bool IsDone(int process) {
        return process >= Needed;
    }
}

public enum QuestType {
    KillBoss, KillEnemy, OpenBox, CountShip, CountDrone, CountCard, CountCardLvl, UpgradeShip, UpgradeDrone, UpgradeCard, ClearBB, EndlessRound, UseCurrency, EndlessRank,
    Checkin, ClearLvl, LuckySpin, PlayMode, ClearMisson
}

[Serializable]
public class MissionsDataInfo {
    private string missionsKey = "missionsData";
    private Dictionary<QuestType, List<QuestDataInfo>> dtnrMissions = new Dictionary<QuestType, List<QuestDataInfo>>();

    public List<MissionDataInfo> missions = new List<MissionDataInfo>();

    public MissionsDataInfo() {
        missions = new List<MissionDataInfo>();
        var hardData = MissionsData.Instance.missions;
        foreach(var mission in hardData) {
            var missionInfo = new MissionDataInfo();
            missionInfo.SetTypeMission(mission.TYPE);
            foreach(var quest in mission.quests) {
                QuestDataInfo questInfo = new QuestDataInfo(quest.ID, quest.Type);
                missionInfo.quests.Add(questInfo);
            }
            missions.Add(missionInfo);
        }
    }

    public MissionsDataInfo LoadData() {
        MissionsDataInfo d = this;
        EventDispatcher.Instance.AddListener<StructDefine.QuestUpdateValue>(QuestUpdateValue);
        if(PlayerPrefs.HasKey(missionsKey)) {
            string jsonData = PlayerPrefs.GetString(missionsKey);
            d = JsonUtility.FromJson<MissionsDataInfo>(jsonData);
        }
        else {
            Save();
        }
        foreach(var mission in d.missions) {
            foreach(var quest in mission.quests) {
                var hasQuest = dtnrMissions.TryGetValue(quest.TYPE, out var questsInfo);
                if(hasQuest) {
                    questsInfo.Add(quest);
                }
                else {
                    dtnrMissions.Add(quest.TYPE, new List<QuestDataInfo>() { quest });
                }
            }
        }
        return d;

    }

    private void QuestUpdateValue(StructDefine.QuestUpdateValue quest) {
        var isMission = dtnrMissions.TryGetValue(quest.type, out var listQuest);
        if(isMission) {
            foreach(var item in listQuest) { item.ChangeValue(quest.amount); }
            EventDispatcher.Instance.Dispatch<StructDefine.QuestUpdateUI>(new StructDefine.QuestUpdateUI() { type = quest.type, amount = quest.amount });
            Save();
        }
    }

    public void ClaimQuest(EnumDefine.TypeMission typeMission, QuestType questType) {
        var mission = missions.Find(x => x.TYPE == typeMission);
        if(mission == null) { return; }
        var quest = mission.quests.Find(x => x.TYPE == questType);
        if(quest == null) { return; }
        quest.UpQuest();
        Save();
    }

    public QuestDataInfo GetQuestInfo(EnumDefine.TypeMission type, string id) {
        var mission = missions.Find(x => x.TYPE == type);
        if(mission == null) {
            Logs.Log(string.Format("Mission {0}-{1} chưa có data.", type, id));
            return new QuestDataInfo(id);
        }
        var quest = mission.quests.Find(x => x.ID == id);
        if(quest == null) {
            Logs.Log(string.Format("Mission {0}-{1} chưa có data.", type, id));
            return new QuestDataInfo(id);
        }
        return quest;
    }

    public void Save() {
        PlayerPrefs.SetString(missionsKey, JsonUtility.ToJson(this));
    }
}

[Serializable]
public class MissionDataInfo {
    [SerializeField] private EnumDefine.TypeMission typeMission = EnumDefine.TypeMission.Mission;
    public List<QuestDataInfo> quests = new List<QuestDataInfo>();

    public EnumDefine.TypeMission TYPE => typeMission;

    public void SetTypeMission(EnumDefine.TypeMission type) {
        typeMission = type;
    }
}

[Serializable]
public class QuestDataInfo {
    [SerializeField] private string id = string.Empty;
    [SerializeField] private QuestType typeQuest = QuestType.KillBoss;
    [SerializeField] private int idQuest = 0;
    [SerializeField] private int process = 0;

    public QuestDataInfo(string id, QuestType typeQuest = QuestType.KillBoss) {
        this.id = id;
        this.typeQuest = typeQuest;
    }

    public string ID => id;

    public QuestType TYPE => typeQuest;

    public int IDQuest => idQuest;

    public int Process => process;

    public void ChangeValue(int process) {
        this.process += process;
    }

    public void UpQuest() {
        ChangeQuest(idQuest + 1);
    }

    public void ChangeQuest(int idQuest) {
        this.idQuest = idQuest;
        process = 0;
    }
}