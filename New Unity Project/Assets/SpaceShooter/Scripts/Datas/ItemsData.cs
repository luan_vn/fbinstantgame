﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "ItemsData", menuName = "Datas/ItemsData")]
public class ItemsData : ScriptableObject
{
    private static ItemsData instance;
    public static ItemsData Instance
    {
        get
        {
            if (instance == null)
            {
                instance = Resources.Load<ItemsData>("ItemsData");
                return instance;
            }
            return instance;
        }

        set
        {
            instance = value;
        }
    }

    public List<ItemData> Items;
}

[Serializable]
public class ItemData {
	public string Name;
    public Sprite InRotate;
    public Sprite OutRotate;
    public Sprite Icon;
}
