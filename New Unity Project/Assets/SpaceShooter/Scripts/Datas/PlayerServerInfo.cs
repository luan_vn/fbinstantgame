﻿using System.Collections.Generic;
using UnityEngine;

public class PlayerServerInfo {
    public int Gold;
    public int Gem;
    public int IndexShip = 0;
    public int IndexDrone = -1;
    public List<int> NumberItem;
    public List<int> HighestScore;
    public int CurrentLevel = 0;

    //public List<ShipDataInfor> Ships = new List<ShipDataInfor>();
    //public List<ShipDataInfor> Drones = new List<ShipDataInfor>();

    public override string ToString() {
        return JsonUtility.ToJson(this);
    }
}
