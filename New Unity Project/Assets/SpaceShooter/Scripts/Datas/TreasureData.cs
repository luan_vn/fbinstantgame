﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "TreasureData", menuName = "Datas/TreasureData")]
public class TreasureData : ScriptableObject
{

    private static TreasureData instance;
    public static TreasureData Instance
    {
        get
        {
            if (instance == null)
            {
                instance = Resources.Load<TreasureData>("TreasureData");
                return instance;
            }
            return instance;
        }

        set
        {
            instance = value;
        }
    }

    public List<TreasureInfor> lsTreasureInfor = new List<TreasureInfor>();
	public List<CardRate> lsCardRate = new List<CardRate>();
	public List<TreasureTypeActive> lsCardActive = new List<TreasureTypeActive>();
}

[Serializable]
public class TreasureInfor
{
	public string name;
	public TreasureReward target;
	public int price;
	public int timeToGetTarget;
	public int[] coinEarned;
	public int[] gemEarned;
	public int[] fuelEarned;
	public Rate earnedTargetRate;
	public List<Rate> lsRate = new List<Rate>();

	public int ItemEarned(int min, int max)
	{
		int earned = UnityEngine.Random.Range(min, max + 1);
		return earned;
	}

	public int CoinEarned
	{
		get { return ItemEarned(coinEarned[0], coinEarned[1]); }
	}

	public int GemEarned
	{
		get { return ItemEarned(gemEarned[0], gemEarned[1]); }
	}

	public int FuelEarned
	{
		get { return ItemEarned(fuelEarned[0], fuelEarned[1]); }
	}

	public List<EnumDefine.StarType> lsTreasureReward()
	{
		List<EnumDefine.StarType> rs = new List<EnumDefine.StarType>();
		for (int i = 0; i < lsRate[0].ToArray().Length - 3; i++)
		{
			if (lsRate[0].ToArray()[i] > 0f)
				rs.Add((EnumDefine.StarType)(i + 1));
		}

		return rs;
	}
}

public enum TreasureReward
{
	CommonCard, RareCard, MythicalCard, HeroicCard, LegendCard,
	Coin,
	Gem,
	Fuel, None
}

[Serializable]
public class Rate
{
	public float Common;
	public float Rare;
	public float Mythical;
	public float Heroic;
	public float Legend;
	public float Coin;
	public float Gem;
	public float Fuel;

	public float[] ToArray()
	{
		return new[] {Common, Rare, Mythical, Heroic, Legend, Coin, Gem, Fuel};
	}
}

[Serializable]
public class CardRate
{
	public int CP;
	public int Shield;
	public int CD_Bom;
	public int Pow;
	public int Missile;
	public int Life;
	public int Coin;
	public int Drone;

	public int[] ToArray()
	{
		return new[] {CP, Shield, CD_Bom, Pow, Missile, Life, Coin, Drone};
	}
}

[Serializable]
public class TreasureTypeActive
{
	public string name;
	//public List<bool> lsActive = new List<bool>();
	public bool[] arrActive = new bool[5];
}

[Serializable]
public class PlayerTreasure {
	public int time;
	public int num;
}