﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "BulletLevelsPosition2TypeData", menuName = "Datas/BulletLevelsPosition2TypeData")]
public class BulletLevelsPosition2TypeData : ScriptableObject {
	public List<BulletLevelByRank> BulletLvlByRank;
	public List<BulletLevelByRank> BulletLvlByRank2Type;
}
