﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class PriceShower {

	/// <summary>
	/// Discount with vip pack
	/// </summary>
	/// <param name="target"></param>
	/// <param name="number"></param>
	/// <param name="isDiscount"></param>
	/// <returns></returns>
	public static string ShowPrice(this string target, int number, bool isDiscount = true) {
		int price = number;
		bool isVip = ManagerData.Instance.PlayerInfo.PlayerProfile.IsVip;
		if (isVip && isDiscount) {
			price = (int)(number * (100 - ManagerData.Instance.PackData.VipPack.Discount) / 100);
			if (price < 1) {
				price = 1;
			}
		}

		target = price.ToString();
		
		return target;
	}

	/// <summary>
	/// Discount with vip pack
	/// </summary>
	/// <param name="number"></param>
	/// <param name="isDiscount"></param>
	/// <returns></returns>
	public static string ShowPrice(int number, bool isDiscount = true) {
		int price = number;
		bool isVip = ManagerData.Instance.PlayerInfo.PlayerProfile.IsVip;
		if (isVip && isDiscount) {
			price = (int) (number * (100 - ManagerData.Instance.PackData.VipPack.Discount) / 100);
			if (price < 1) {
				price = 1;
			}
		}

		return price.ToString();
	}

	/// <summary>
	/// Discount without vip pack
	/// </summary>
	/// <param name="target"></param>
	/// <param name="number"></param>
	/// <param name="discount"></param>
	/// <returns></returns>
	public static string ShowPrice(this string target, int number, float discount) {
		int price = (int) (number * (100 - discount) / 100);
		target = price.ToString();
		return target;
	}

	/// <summary>
	/// Discount without vip pack
	/// </summary>
	/// <param name="number"></param>
	/// <param name="discount"></param>
	/// <returns></returns>
	public static string ShowPrice(int number, float discount) {
		int price = (int)(number * (100 - discount) / 100);
		return price.ToString();
	}
}
