﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BottomPanelCard : MonoBehaviour {
	public GameObject Try;
	public GameObject Type;

	private bool _canTry = false;

	public void ChangeTab() {
		//_canTry = EquipmentPage.TypeItemSelected != EquipmentType.Card;
		////Try.SetActive(EquipmentPage.TypeItemSelected != EquipmentType.Card);
		//if (EquipmentPage.TypeItemSelected == EquipmentType.Card) {
		//	Try.SetActive(false);
		//}
		//Type.SetActive(EquipmentPage.TypeItemSelected == EquipmentType.Card);
	}

	public void DeactiveBtn() {
		Try.SetActive(false);
		//Type.SetActive(false);
	}

	public void CheckTryShip(IItemShowable haventBuy) {
		var item = haventBuy as AircraftDataShow;

		Try.SetActive(item != null && item.CurrentLevel == -1 && _canTry);
	}
}
