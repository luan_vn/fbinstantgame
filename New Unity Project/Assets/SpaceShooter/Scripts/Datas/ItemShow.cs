﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ItemShow : MonoBehaviour {
	private IItemShowable _data;

	private ButtonAnimation _myBta;
	[SerializeField] private bool _isStretchFull = false;
	[SerializeField] Image _icon;
	[SerializeField] Text _textLevel;
	[SerializeField] Image _border;
	[SerializeField] GameObject _starGroup;
	[SerializeField] GameObject _frameSelected;
	[SerializeField] GameObject _blackPanel;
	[SerializeField] GameObject _tick;
	private Action<IItemShowable> _showData;

	public IItemShowable Data {
		set {
			_data = value;
			gameObject.SetActive(_data != null);
			if (_data != null) {
				UpdateUI();
				UpdateSelectedOrNot();

				//if (EquipmentPage.OldIndexItemSelect != -2 && _data.OrderInList == EquipmentPage.OldIndexItemSelect) {
				//	// Bo comment la chay sai
				//	if (_myBta && _myBta.onClick != null)
				//		_myBta.onClick.Invoke();
				//}
			}
		}
		get {
			return _data;
		}
	}

	private void SetSizeIcon() {
		var iconRect = _icon.GetComponent<RectTransform>();
		iconRect.anchoredPosition = Vector3.zero;
		var rectTransform = GetComponentInParent<GridLayoutGroup>();
		if (rectTransform == null)
			return;
		var sizeDelta = rectTransform.cellSize;
		float ratioX = (sizeDelta.x - 30) / iconRect.sizeDelta.x;
		iconRect.sizeDelta = new Vector2(((sizeDelta.x - 30)), iconRect.sizeDelta.y * ratioX);

		float ratioY = (sizeDelta.y - 30) / iconRect.sizeDelta.y;
		if (ratioY <= 1)
			iconRect.sizeDelta = new Vector2(iconRect.sizeDelta.x * ratioY, (sizeDelta.y - 30));
	}

	private void Awake() {
	}
	private void OnEnable() {
		SetSizeIcon();
	}

	//public void SetShowData(Action<IItemShowable> itemShow) {
	//	_showData = itemShow;
	//	_myBta = _myBta ?? GetComponent<ButtonAnimation>();
	//	_myBta.onClick.RemoveAllListeners();
	//	_myBta.onClick.AddListener(() => {
	//		if (UIManager.IsLockClick)
	//			return;

 //           //[Tracking - Position]-[ButtonPress]
 //          // string location = UIManager.Instance.EquipmentPage.CurrentNameMenu();
 //           string btnID = string.Empty;
 //           if(Data is CardInfo) {
 //               btnID = "btn_card";
 //           }
 //           else if(Data is AircraftDataShow aircraftData) {
 //               btnID = aircraftData.TypeShip == EnumDefine.TypeShip.Ship ? $"btn_ship{aircraftData.Order + 1}" : $"btn_drone{aircraftData.Order + 1}";
 //           }
 //          // GameTracking.Instance.TrackButtonPress(location, btnID);

 //           EquipmentPage.OldIndexItemSelect = _data.OrderInList;
	//	//	EquipmentPage.IsChange = false;
	//		_showData(_data);
	//		UpdateSelectedOrNot();
	//	});
	//}

	private void UpdateUI() {
		_myBta = _myBta ?? GetComponent<ButtonAnimation>();
		if (_data == null)
			return;

		_icon.sprite = _data.IconShow;
		_icon.SetNativeSize();
		var iconRect = _icon.GetComponent<RectTransform>();
		if (!_isStretchFull) {
			iconRect.anchorMax = Vector2.one * 0.5f;
			iconRect.anchorMin = Vector2.one * 0.5f;
			iconRect.anchoredPosition = Vector3.zero;
		} else {// Day la icon upgrade card
			iconRect.anchorMax = Vector2.one;
			iconRect.anchorMin = Vector2.zero;
			iconRect.offsetMax = Vector3.zero;
			iconRect.offsetMin = Vector3.zero;
		}

		SetSizeIcon();
		_textLevel.text = _data.LevelShow;

		if (!_border)
			_border = transform.Find("Frame").GetComponent<Image>();

		//if (_border)
		//	_border.sprite =
		//		DataConfig.Instance.BoundIconCards[_data is AircraftDataShow ? _data.BorderShow - 1 : _data.NumberStarShow - 1];

		if (_border) {
			_border.color =
				DataConfig.Instance.BoundCardColor[_data is AircraftDataShow ? _data.BorderShow - 1 : _data.NumberStarShow - 1];
		}


		//if (_data is CardInfo)
		_starGroup.SetActive(!(_data is CardInfo));
		int numberStart = 0;
		if (_data is AircraftDataShow) {
			numberStart = _data.NumberStarShow;
		}
		for (int i = 0; i < _starGroup.transform.childCount; i++) {
			_starGroup.transform.GetChild(i).gameObject.SetActive(i < numberStart);
		}

	}

	public void UpdateSelectedOrNot() {
		if (!_frameSelected || !_blackPanel || !_tick) {
			return;
		}

		_tick.SetActive(false);
		_frameSelected.SetActive(false);
		_blackPanel.SetActive(false);

		var data = _data as CardInfo;
		var dataAircraft = _data as AircraftDataShow;
		if (data != null) {
			if (data.Order == -1) {
				_blackPanel.SetActive(true);
			}
			if (!_data.IsSelected) {
				return;
			}
			//if (EquipmentPage.TypeShop == TypeCardPage.None) {
			//	_frameSelected.SetActive(true);
			} else {
				_blackPanel.SetActive(true);
				_tick.SetActive(true);
			}
		//} else if (dataAircraft != null) {
		//	if (dataAircraft.CurrentLevel < 0) {
		//		_blackPanel.SetActive(true);
		//	}

		//	_tick.SetActive(dataAircraft.IsEquip);
		//	if (!_data.IsSelected)
		//		return;

		//	_frameSelected.SetActive(true);
		//}
		//Debug.LogError("Here is error fix add card into ingredient");
		//if (EquipmentPage.TypeShop == TypeCardPage.Upgrade)
		//	_data.IsSelected = UIManager.Instance.EquipmentPage.PageUpper.AddItemToIngredient(_data);
	}
}
