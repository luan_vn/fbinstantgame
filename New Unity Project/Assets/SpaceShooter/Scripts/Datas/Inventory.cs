﻿using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

[Serializable]
public class Inventory {
	private const string Key = "Inventory";

	[SerializeField] private string _currentVer;
	public List<PlayerCard> Cards;
	public List<int> HalloweenItems;
	public int Candies;
	public List<PlayerTreasure> Treasures;
	public int firstTimeOpenBox;
	public int DailyChallengeExTicket;

	[ContextMenu("Save data")]
	public void SaveData() {
		PlayerPrefs.SetString(Key, JsonUtility.ToJson(this));
	}

	public void LoadData() {
		string data = PlayerPrefs.GetString(Key);
		_currentVer = Application.version;

		if (data != string.Empty) {
			var inventory = JsonUtility.FromJson<Inventory>(data);
			Cards = inventory.Cards;
			HalloweenItems = inventory.HalloweenItems;
			Candies = inventory.Candies;
			Treasures = inventory.Treasures;
			firstTimeOpenBox = inventory.firstTimeOpenBox;
			DailyChallengeExTicket = inventory.DailyChallengeExTicket;
		}
		if (Cards == null)
			Cards = new List<PlayerCard>();
		if (HalloweenItems == null || HalloweenItems.Count == 0) {
			HalloweenItems = new List<int>() { 0, 0, 0, 0, 0 };
			//HalloweenItems = new List<int>() {1000, 1000, 1000, 1000, 1000}; // hack pumpkin
		}

		if (Treasures == null || Treasures.Count == 0) {
			Treasures = new List<PlayerTreasure>();
			Treasures.Add(new PlayerTreasure());
			Treasures.Add(new PlayerTreasure());
			Treasures.Add(new PlayerTreasure());
			//Treasures[(int)TreasureType.Bad].num += 1;
		}
		GetFavoriteIndex();
	}

	void GetFavoriteIndex() {
		for (int i = 0; i < Cards.Count; i++) {
			if (Cards[i].IsFavorite.Count == 0) {
				Cards[i].IsFavorite = new List<bool>() {false, false, false};
			}
		}

		SaveData();
	}

	public void AddCard(PlayerCard card) {
        GameTracking.Instance.TrackingCardsChange(ItemChangeAction.Gain, card.Level, card.Index.ToString(), CardPurpose.None);
		Cards.Add(card);
	}

	public List<IItemShowable> GetAllCard(int exceptIndex = -1, int itemWasSelected = -1) {
		List<IItemShowable> info = new List<IItemShowable>();
		var data = ManagerData.Instance.CardData;

		for (int i = 0; i < Cards.Count; i++) {
			if (i == exceptIndex)
				continue;

			CardInfo c = data.GetInfo(Cards[i].Index);
			c.Order = i;
			c.Level = Cards[i].Level;
			c.CurrentExp = Cards[i].CurrentExp;
			c.Numeral = Cards[i].Numeral;
			if (itemWasSelected == i)
				c.IsSelected = true;

			//info.Add(c);
		}
		return info;
	}


	public void RemoveCard(int indexOfCard, bool isSelected = false) {
        if(!isSelected) {
            GameTracking.Instance.TrackingCardsChange(ItemChangeAction.Spend, Cards[indexOfCard].Level, Cards[indexOfCard].Index.ToString(), CardPurpose.Fuse);
        }
        Cards.RemoveAt(indexOfCard);
	}

	public void RemoveCard(List<int> indexOfCard) {
		var indexOrder = indexOfCard.OrderByDescending(i => i);
		foreach (var index in indexOrder) {
            GameTracking.Instance.TrackingCardsChange(ItemChangeAction.Spend, Cards[index].Level, Cards[index].Index.ToString(), CardPurpose.Fuse);
            Cards.RemoveAt(index);
		}
	}

	public void AddHalloweenItem(HalloweenItemType type, int addNum, string location, string gameMode = "null", string level = "null") {
        GameTracking.Instance.TrackingItemsChange(ItemChangeAction.Gain, ItemTypeTracking.Ticket, $"halloweeenItem:{type}", addNum, location, gameMode, level);
        HalloweenItems[(int)type] += addNum;
	}

	public bool UseHalloweenItem(HalloweenItemType type, int useNum, string location, string gameMode = "null", string level = "null") {

		if (HalloweenItems[(int)type] >= useNum) {
            GameTracking.Instance.TrackingItemsChange(ItemChangeAction.Spend, ItemTypeTracking.Ticket, $"halloweeenItem:{type}", useNum, location, gameMode, level);
            HalloweenItems[(int)type] -= useNum;
			return true;
		}

		return false;
	}

	public bool IsEnoughPumpkin(HalloweenItemType type, int useNum) {
		if (HalloweenItems[(int)type] >= useNum) {
			return true;
		}

		return false;
	}

	public void AddCandy(int addNum, string location, string gameMode = "null", string level = "null") {
        GameTracking.Instance.TrackingItemsChange(ItemChangeAction.Gain, ItemTypeTracking.Ticket, "candy", addNum, location, gameMode, level);
		Candies += addNum;
	}

	public bool UseCandy(int useNum, string location, string gameMode = "null", string level = "null") {
		if (Candies > 0) {
            GameTracking.Instance.TrackingItemsChange(ItemChangeAction.Spend, ItemTypeTracking.Ticket, "candy", useNum, location, gameMode, level);
            Candies -= useNum;
			return true;
		}

		return false;
	}

	public void AddTreasure(TreasureType type, int addNum, string location, string gameMode = "null", string level = "null") {
		Treasures[(int)type].num += addNum;
        GameTracking.Instance.TrackingItemsChange(ItemChangeAction.Gain, ItemTypeTracking.Ticket, $"Treasure{(int)type}", 1, location);
        ManagerData.Instance.isAddTreasure = true;
	}

	public void FirstTimeOpenBox() {
		if (firstTimeOpenBox == 0) {
			//UIManager.Instance.TreasurePage.CountDownBox.StartCountDown();
			firstTimeOpenBox = 1;
			SaveData();
		}
	}

	public void AddExTicket(int number, string location, string gameMode = "null", string level = "null") {
        GameTracking.Instance.TrackingItemsChange(ItemChangeAction.Gain, ItemTypeTracking.Ticket, "dailyChallengeTicket", number, location, gameMode, level);
		DailyChallengeExTicket += number;
		SaveData();
	}

	public bool UseExTicket(int number, string location, string gameMode = "null", string level = "null") {
		if (DailyChallengeExTicket >= number) {
            GameTracking.Instance.TrackingItemsChange(ItemChangeAction.Spend, ItemTypeTracking.Ticket, "dailyChallengeTicket", number, location, gameMode, level);
            DailyChallengeExTicket -= number;
			SaveData();
			return true;
		}

		return false;
	}
}
