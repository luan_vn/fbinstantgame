﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Random = UnityEngine.Random;

[CreateAssetMenu(fileName = "BlackMarketData", menuName = "Datas/BlackMarketData")]
public class BlackMarketData : ScriptableObject {

    private static BlackMarketData instance;
    public static BlackMarketData Instance
    {
        get
        {
            if (instance == null)
            {
                instance = Resources.Load<BlackMarketData>("BlackMarketData");
                return instance;
            }
            return instance;
        }

        set
        {
            instance = value;
        }
    }

    public List<BlackMarketSlot> Slot = new List<BlackMarketSlot>();
}

[Serializable]
public class BlackMarketSlot {
	public AllCurrency UnlockCurrency;
	public int UnlockNum;
	public AllCurrency NoAdsUnlockCurrency;
	public int NoAdsUnlockNum;
	public List<BlackMarketSlotInfor> SlotInfor = new List<BlackMarketSlotInfor>();

	public BlackMarketSlotInfor GetInfor() {
		float random = Random.Range(0, 99f);
		int inforIndex;
		for (inforIndex = 0; inforIndex < SlotInfor.Count; inforIndex++) {
			float rate = SlotInfor[inforIndex].Rate;
			if (random < rate) {
				break;
			}

			random -= rate;
		}

		return SlotInfor[inforIndex];
	}

	public int GetInforId() {
		float random = Random.Range(0, 99f);
		int inforIndex;
		for (inforIndex = 0; inforIndex < SlotInfor.Count; inforIndex++) {
			float rate = SlotInfor[inforIndex].Rate;
			if (random < rate) {
				break;
			}

			random -= rate;
		}

		return inforIndex;
	}
}

[Serializable]
public class BlackMarketSlotInfor {
	public Sprite ItemIcon;
	public AllRewardType ItemType;
	public int ItemExtendId;
	public int ItemNum;
	public AllCurrency CurrencyType;
	public int CurrencyNum;
	public AllCurrency NoAdsCurrencyType;
	public int NoAdsCurrencyNum;
	public float Rate;
}

