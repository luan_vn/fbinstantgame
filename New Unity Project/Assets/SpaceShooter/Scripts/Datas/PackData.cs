﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "PackData", menuName = "Datas/PackData")]
public class PackData : ScriptableObject {

    private static PackData instance;
    public static PackData Instance
    {
        get
        {
            if (instance == null)
            {
                instance = Resources.Load<PackData>("PackData");
                return instance;
            }
            return instance;
        }

        set
        {
            instance = value;
        }
    }

	public VipPack VipPack;
	public PackInfor[] PackInfor;
}

[Serializable]
public class VipPack {
	public string Key;
    public string oldKey;
	public int Days;
    public float Discount;
    public RewardVip[] RewardVips;
}

[Serializable]
public class PackInfor {
	public string Key;
    public string OldKey;
    public bool Consumable;
    public PackType PackType;
	public float SafeOff;
	public float Price;
	public float OldPrice;
	public RewardInfor[] RewardInfors;
}


[Serializable]
public class RewardInfor {
    [SerializeField] private string name;
	public AllRewardType RewardType;
	public int ExtendId;
	public int Number;
    public Sprite Icon;
    public Sprite CategoryIcon;
}

[Serializable]
public class RewardVip
{
    public AllRewardVipType RewardType;
    public bool PerDay;
    public int ExtendId;
    public float Number;
    public Sprite Icon;
    public string Description;
}


