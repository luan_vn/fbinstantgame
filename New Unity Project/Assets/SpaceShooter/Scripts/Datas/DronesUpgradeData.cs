﻿using System;
using System.Collections;
using System.Collections.Generic;
using Gemmob;
using UnityEngine;

[CreateAssetMenu(fileName = "DronesUpgradeData", menuName = "Datas/DronesUpgradeData")]
public class DronesUpgradeData : ScriptableObject {

    private static DronesUpgradeData instance;
    public static DronesUpgradeData Instance
    {
        get
        {
            if (instance == null)
            {
                instance = Resources.Load<DronesUpgradeData>("DronesUpgradeData");
                return instance;
            }
            return instance;
        }

        set
        {
            instance = value;
        }
    }

    public List<DroneUpgradeData> Drones = new List<DroneUpgradeData>();

	public int GetIndex(string shipName) {
		return Drones.FindIndex(ship => String.Compare(ship.Name, shipName, StringComparison.Ordinal) == 0);
	}
}

[Serializable]
public class DroneUpgradeData {
    public int Id;
	public Sprite Icon;
	public List<Sprite> Icons = new List<Sprite>();
	public Sprite ClassIcon;
	public Sprite EvolveItemIcon;
	public string Name;
	public string Description;
	public float BulletDame;
	public int NumberStar;
	public int CombatPower;
	public int VideoCount;
	public int CoinPrice;
	public int GemPrice;
	public int LevelUnlock;
	public string BuyDescription;
	public List<AircraftEvolveData> EvolveDatas = new List<AircraftEvolveData>();
	public Region Region;
	public SpecialEvent Event;

	public List<DroneLevelData> Levels = new List<DroneLevelData>();
	public List<DroneLevelData> OldLevels = new List<DroneLevelData>();

	public string GetBase(int currentLevel) {
		int realLevel = currentLevel > Levels.Count ? Levels.Count : currentLevel;
		return currentLevel == -1 || currentLevel == 0 ? CombatPower.ToString() : Levels[realLevel - 1].CombatPower.ToString();
	}

	public string ToString(int level) {
		var head = "";
		var isCp = GamePlayState.Instance.TypeGame == TypeGame.CombatPower;
		var tail = string.Empty;
		if (level == -1) {
			head = Description;
			tail = isCp ? "Base power: " + CombatPower : "Base damage: " + BulletDame * 100;
		} else {
			var data = Levels[level >= Levels.Count ? Levels.Count - 1 : level];
			tail = isCp ? (level >= Levels.Count ? "Power: " : "Next power: ") + data.CombatPower : (level >= Levels.Count ? "Damage: " : "Next damage: ") + data.Dam * 100;
		}

		return head + "\n" + tail;
	}
}

[Serializable]
public class DroneLevelData {
	public float FireRate;
	public float Dam;
	public float BulletSpeed;
	public int CombatPower;
	public int Coin;
	public int Gem;
}