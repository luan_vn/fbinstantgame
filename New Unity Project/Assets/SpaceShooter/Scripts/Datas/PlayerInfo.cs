﻿using Gemmob;
using System;
using System.Collections.Generic;
using UnityEngine;
using static EnumDefine;

[Serializable]
public class PlayerInfo {
    private string playerData = "playerData";
    public bool DebugTest;
    public static Action AfterLoadData;
    public static int MaximumFuel = 20;
    public List<string> BoughtIapPacks;
    public List<CurrencyInfo> currencies = null;
    public List<ShipEquipedInfo> shipEquipeds = null;
    public List<SkillEquipedInfo> skillEquipeds = null;

    [SerializeField] private int _gold;
    [SerializeField] private int _gem;
    [SerializeField] private int _hotdeal = 20;


    [SerializeField] private IDSkill idActiveSkill = IDSkill.Drone;

    [SerializeField] private int levelUnlock = 0;
    [SerializeField] private bool _isPlayedCurrentLevel;
    [SerializeField] private List<int> _numberItem;

    [SerializeField] private List<int> _highestScore;
    [SerializeField] private int _halloweenBestWave;
    [SerializeField] private int _halloweenLastWave;
    [SerializeField] private List<int> _bonusBuyCoin;
    [SerializeField] private List<int> _bonusBuyGem;
    [SerializeField] private string _openDay = string.Empty;
    [SerializeField] private int _adsWatchedPerDay;

    [SerializeField] private PlayerCard _card;
    [SerializeField] private EndlessInfor _playerEndlessInfor;
    [SerializeField] private PlayerProfile playerProfile;
    [SerializeField] private UnlockModeData _unlockModeData;


    #region Properties
    public int Gold {
        get { return _gold; }
        private set {
            _gold = value;
        }
    }

    public int Gem {
        get { return _gem; }
        private set {
            _gem = value;
        }
    }

    public int Fuel {
        get { return _hotdeal; }
        private set {
            _hotdeal = value;
            //if (GameEvent.OnFuelChanged != null)
            //	GameEvent.OnFuelChanged.Invoke(_fuel, MaximumFuel);
        }
    }

    public void UpgradeLevelUnlock(int value = 1) {
        levelUnlock += value;
        Save();
    }

    public void SetShipEquiped(Direct direct, bool equiped, IDShip idShip = IDShip.Ship_01) {
        var equipedInfo = shipEquipeds.Find(x => x.CompareDirect(direct));
        if(equiped) {
            if(equipedInfo == null) {
                shipEquipeds.Add(new ShipEquipedInfo(direct, idShip));
            }
            else {
                equipedInfo.UpdateEquiped(direct, idShip);
            }
        }
        else {
            if (equipedInfo == null) {
                Logs.Log(string.Format("Direct {0} not yet", direct));
            }
            else {
                shipEquipeds.Remove(equipedInfo);
            }
        }
        Save();
    }

    public void SetSkillEquiped(Direct direct, bool equiped, IDSkill idSkill = IDSkill.Shield) {
        var equipedInfo = skillEquipeds.Find(x => x.CompareDirect(direct));
        if(equiped) {
            if(equipedInfo == null) {
                skillEquipeds.Add(new SkillEquipedInfo(direct, idSkill));
            }
            else {
                equipedInfo.UpdateEquiped(direct, idSkill);
            }
        }
        else {
            if(equipedInfo == null) {
                Logs.Log(string.Format("Direct {0} not yet", direct));
            }
            else {
                skillEquipeds.Remove(equipedInfo);
            }
        }
        Save();
    }

    public bool ShipEquipedAtDirect(Direct direct, out ShipEquipedInfo shipEquipedInfo) {
        shipEquipedInfo = shipEquipeds.Find(x => x.CompareDirect(direct));
        if(shipEquipedInfo == null) {
            Logs.Log(string.Format("Direct {0} not set up", direct));
            return false;
        }
        return true;
    }

    public bool SkillEquipedAtDirect(Direct direct, out SkillEquipedInfo skillEquipedInfo) {
        skillEquipedInfo = skillEquipeds.Find(x => x.CompareDirect(direct));
        if(skillEquipedInfo == null) {
            Logs.Log(string.Format("Direct {0} not set up", direct));
            return false;
        }
        return true;
    }

    public bool IsShipEquiped(IDShip idShip, out ShipEquipedInfo shipEquipedInfo) {
        shipEquipedInfo = shipEquipeds.Find(x => x.CompareIDShip(idShip));
        if (shipEquipedInfo == null) {
            Logs.Log(string.Format("Ship {0} not equiped", idShip));
            return false;
        }
        return true;
    }

    public bool AddShipEquiped(ShipEquipedInfo shipEquiped) {
        var exist = shipEquipeds.Exists(x => x.Direct == shipEquiped.Direct);
        if(exist) {
            Logs.Log(string.Format("Direct {0} Equiped", shipEquiped.Direct));
            return false;
        }
        else {
            shipEquipeds.Add(shipEquiped);
            Save();
            return true;
        }
    }


    public bool IsSkillEquiped(IDSkill idSkill, out SkillEquipedInfo skillEquipedInfo) {
        skillEquipedInfo = skillEquipeds.Find(x => x.CompareIDShip(idSkill));
        if(skillEquipedInfo == null) {
            Logs.Log(string.Format("Skill {0} not equiped", idSkill));
            return false;
        }
        return true;
    }


    public int GetCurrency(IDCurrency idCurrency) {
        var currencyInfo = currencies.Find(x => x.IDCurrency == idCurrency);
        if(currencyInfo == null) {
            currencyInfo = new CurrencyInfo(idCurrency, 0);
            currencies.Add(currencyInfo);
        }
        return currencyInfo.Amount;
    }

    public int AddCurrency(IDCurrency idCurrency, int amount) {
        var currencyInfo = currencies.Find(x => x.IDCurrency == idCurrency);
        if (currencyInfo == null) {
            currencyInfo = new CurrencyInfo(idCurrency, amount);
            currencies.Add(currencyInfo);
        }
        else {
            currencyInfo.AddCurrency(amount);
        }
        EventDispatcher.Instance.Dispatch(new StructDefine.CurrencyChange() {idCurrency = idCurrency,amount = amount });
        return currencyInfo.Amount;
    }


    public int LevelUnlock => levelUnlock;

    public List<int> NumberItem {
        get { return _numberItem; }
        set { _numberItem = value; }
    }

    public List<int> HighestScore {
        get { return _highestScore ?? (_highestScore = new List<int>()); }
        set { _highestScore = value; }
    }

    public PlayerCard Card {
        get { return _card; }
        set {
            if(value == null)
                return;

            if(_card == null)
                _card = new PlayerCard(value.Index);

            _card.Index = value.Index;
            _card.Level = value.Level;
            _card.CurrentExp = value.CurrentExp;
            _card.Numeral = value.Numeral;
            _card.IsFavorite = value.IsFavorite;
        }
    }

    public PlayerProfile PlayerProfile {
        get { return playerProfile; }
        set {
            if(value == null)
                return;
            if(playerProfile == null) {
                playerProfile = new PlayerProfile();
            }

            playerProfile.AvatarID = value.AvatarID;
            playerProfile.FavoriteShipID = value.FavoriteShipID;
            playerProfile.IsVip = value.IsVip;
            playerProfile.Name = value.Name;
        }
    }

    public EndlessInfor PLayerEndlessInfor {
        get { return _playerEndlessInfor; }
        set {
            if(value == null)
                return;
            if(_playerEndlessInfor == null) {
                _playerEndlessInfor = new EndlessInfor();
            }

            _playerEndlessInfor.TotalScore = value.TotalScore;
            _playerEndlessInfor.BestWave = value.BestWave;
            _playerEndlessInfor.CurRank = value.CurRank;
            _playerEndlessInfor.SkipItemNumber = value.SkipItemNumber;
            if(_playerEndlessInfor.SkipItemNumber == null || _playerEndlessInfor.SkipItemNumber.Count == 0) {
                _playerEndlessInfor.SkipItemNumber = new List<int> { 0, 0, 0, 0 };
            }
        }
    }
    #endregion

    public CardInfo GetCardInfo {
    	get {
    		CardInfo c = ManagerData.Instance.CardData.GetInfo(_card.Index);

    		if (c == null)
    			return null;

    		c.Order = -1;
    		c.Level = _card.Level;
    		c.CurrentExp = _card.CurrentExp;
    		c.Numeral = _card.Numeral;

    		return c;
    	}
    }

    public int HalloweenBestWave {
        get { return _halloweenBestWave; }
    }

    public int HalloweenLastPlay {
        get { return _halloweenLastWave; }
    }

    public List<int> BonusBuyCoin {
        get { return _bonusBuyCoin; }
        set { _bonusBuyCoin = value; }
    }

    public List<int> BonusBuyGem {
        get { return _bonusBuyGem; }
        set { _bonusBuyGem = value; }
    }

    public string OpenDay {
        get { return _openDay; }
        set { _openDay = value; }
    }

    public int AdsWatchedPerDay {
        get { return _adsWatchedPerDay; }
        set { _adsWatchedPerDay = value; }
    }

    public UnlockModeData UnlockModeData { get => _unlockModeData; set => _unlockModeData = value; }
    public bool IsPlayedCurrentLevel { get => _isPlayedCurrentLevel; set => _isPlayedCurrentLevel = value; }


    public PlayerInfo() {
        Card = new PlayerCard(-1);
        playerProfile = new PlayerProfile();
        currencies = new List<CurrencyInfo>() { new CurrencyInfo(IDCurrency.Coin, 10000), new CurrencyInfo(IDCurrency.Gem, 10000)};
        shipEquipeds = new List<ShipEquipedInfo>() { new ShipEquipedInfo(Direct.Center, IDShip.Ship_01) };
        skillEquipeds = new List<SkillEquipedInfo>();
    }

    public void Save() {
        PlayerPrefs.SetString(playerData, JsonUtility.ToJson(this));
    }

    public PlayerInfo LoadData() {
        PlayerInfo data = this;
        if(PlayerPrefs.HasKey(playerData)) {
            string jsonData = PlayerPrefs.GetString(playerData);
            data = JsonUtility.FromJson<PlayerInfo>(jsonData);
        }
        else {
            
            Save();
        }
        return data;
    }



    public int GetNumberItem(int index) {
        if(index < 0 || index >= _numberItem.Count)
            return -1;

        return _numberItem[index];
    }

    //public void FuelRecovery() {
    //	if (Fuel < MaximumFuel)
    //		Fuel += 1;
    //	SaveData();
    //	if (Fuel < MaximumFuel)
    //		CountDownController.Instance.CountDownFuel.StartCountDown();
    //}

    //public void AddBoughtIapPack(string key) {
    //	BoughtIapPacks.Add(key);
    //	SaveData();
    //}

    //public decimal GetAllSpent() {
    //	decimal money = 0;
    //	for (int i = 0; i < BoughtIapPacks.Count; i++) {
    //		money += UnityIAP.Instance.GetLocalPrice(BoughtIapPacks[i]).localizedPrice;
    //	}

    //	return money;
    //}

    //public bool CheckBoughtIap(string key) {
    //	return BoughtIapPacks.Exists(s => s.Equals(key));
    //}

    //#region Currency

    int CalculateNeeded(int needed, bool canDiscount) {
        if(playerProfile.IsVip && canDiscount) {
            needed = (int)(needed * (100 - ManagerData.Instance.PackData.VipPack.Discount) / 100);
            if(needed < 1) {
                return 1;
            }
            else
                return needed;
        }

        return needed;
    }

    public bool UseGem(int needed, string itemName, string location, string gameMode = "null", string level = "null", bool canDiscount = false, string detail = "") {
        needed = CalculateNeeded(needed, canDiscount);
        bool isEnough = Gem >= needed;

        if(isEnough) {
            Gem -= needed;
            GameTracking.Instance.TrackingSpendVirtualCurrency(itemName, VirtualCurrencyName.Gem, needed.ToString(), location, gameMode, level);
            //Tracking.LogCurrency(-needed, CurrencyType.Gem, area);
            Save();

            if(EffectCurrency.Instance)
                EffectCurrency.Instance.GemJump(-needed);
        }
        else {
            //		//TODO : Show popup to shop
        }

        return isEnough || DebugTest;
    }

    public bool UseFuel(int needed, string location) {
        if(playerProfile.IsVip) {
            needed = 0;
        }
        bool isEnough = Fuel >= needed;
        bool isFull = Fuel >= MaximumFuel;
        if(isEnough) {
            Fuel -= needed;
            if(needed > 0) {
                GameTracking.Instance.TrackingSpendVirtualCurrency("null", VirtualCurrencyName.Fuel, needed.ToString(), location);
            }

            Save();

            if(EffectCurrency.Instance)
                EffectCurrency.Instance.FuelJump(-needed);
        }
        else {
            //TODO : Show popup to shop
        }

        if(Fuel < MaximumFuel && isFull)
            CountDownController.Instance.CountDownFuel.StartCountDown();

        return isEnough || DebugTest;
    }

    public bool CheckGold(int needed) {
        return Gold >= needed || DebugTest;
    }

    public bool CheckGem(int needed) {
        return Gem >= needed || DebugTest;
    }

    public bool CheckGem(float needed) {
        return Gem >= needed || DebugTest;
    }

    public bool CheckFuel(int needed) {
        if(playerProfile.IsVip) {
            return true;
        }
        return Fuel >= needed || DebugTest;
    }

    public bool AddGem(int numberAdd, string location, string gameMode = "null", string level = "null") {
        if(numberAdd == 0)
            return true;

        Gem += numberAdd;
        GameTracking.Instance.TrackingEarnVirtualCurrency(VirtualCurrencyName.Gem, numberAdd.ToString(), location, gameMode, level);
        //Tracking.LogCurrency(numberAdd, CurrencyType.Gem, area, detail);

        if(EffectCurrency.Instance)
            EffectCurrency.Instance.GemJump(numberAdd);

        Save();

        return true;
    }

    public bool AddGold(int numberAdd, string location, string gameMode = "null", string level = "null") {
        if(numberAdd == 0)
            return true;

        Gold += numberAdd;

        GameTracking.Instance.TrackingEarnVirtualCurrency(VirtualCurrencyName.Gold, numberAdd.ToString(), location, gameMode, level);
        ///Tracking.LogCurrency(numberAdd, CurrencyType.Coin, area, detail);


       // EffectCurrency.Instance.CoinJump(numberAdd);

        Save();

        return true;
    }

    public bool AddFuel(int numberAdd, string location, string gameMode = "null", string level = "null") {
        if(numberAdd == 0)
            return true;

        Fuel += numberAdd;

        GameTracking.Instance.TrackingEarnVirtualCurrency(VirtualCurrencyName.Fuel, numberAdd.ToString(), location, gameMode, level);

        if(EffectCurrency.Instance)
            EffectCurrency.Instance.FuelJump(numberAdd);

        Save();

        return true;
    }

    //#endregion

    //#region Item
    //public bool UseItem(int indexItemUse, string location, string gameMode = "null", string level = "null", bool isRecycleInBB = false) {
    //	if (indexItemUse <= -1 || indexItemUse >= _numberItem.Count)
    //		return false;

    //	bool isEnought = _numberItem[indexItemUse] > 0;

    //	// Neu chi con 1 item thi vao choi BB, item do = 0, chien thang Boss do, sang choi Boss khac thi item = 0, k recyce duoc item luc vao choi
    //	if (isEnought && !isRecycleInBB) {
    //           GameTracking.Instance.TrackingItemsChange(ItemChangeAction.Spend, ItemTypeTracking.Booster, indexItemUse == 3 ? "bomb" : $"powerLevel{indexItemUse}", 1, location, gameMode, level);
    //		_numberItem[indexItemUse]--;
    //		SaveData();
    //	}
    //	// Only BB : Solution for problem
    //	if (isRecycleInBB && !isEnought) {
    //		return true;
    //	}

    //	return isEnought || DebugTest;
    //}

    //public bool CanUseItem(int indexItemUse) {
    //	return _numberItem[indexItemUse] > 0 || DebugTest;
    //}

    //public void AddItem(int indexItem, int infoNumberItem, string location, string gameMode = "null", string level = "null") {
    //	if (indexItem <= -1 && indexItem >= _numberItem.Count)
    //		return;

    //       GameTracking.Instance.TrackingItemsChange(ItemChangeAction.Gain, ItemTypeTracking.Booster, indexItem == 3 ? "bomb" : $"powerLevel{indexItem}", infoNumberItem, location, gameMode, level);
    //       _numberItem[indexItem] += infoNumberItem;
    //	SaveData();
    //       //GamePlayState.Instance.SetActiveNotifiStore(AllRewardType.IngameItem, indexItem);

    //       if (IngameNotifiController.Instance)
    //       {
    //           IngameNotifiController.Instance.SetActiveNotifiStorage(AllRewardType.IngameItem, indexItem);
    //       }
    //       else
    //       {
    //           GamePlayState.Instance.SetActiveNotifiStore(AllRewardType.IngameItem, indexItem);
    //       }

    //   }

    //public void AddEndlessReward(EndlessRewardType type, int number, int itemIndex = -1, string location = "null", string gameMode = "null", string level = "null") {
    //	if (type == EndlessRewardType.Coin) {
    //		Gold += number;
    //		//Tracking.LogCurrency(number, CurrencyType.Coin, ChangeCurrencyArea.Endless);
    //		if (EffectCurrency.Instance)
    //			EffectCurrency.Instance.CoinJump(number);
    //	} else if (type == EndlessRewardType.Gem) {
    //		Gem += number;
    //		//Tracking.LogCurrency(number, CurrencyType.Gem, ChangeCurrencyArea.Endless);
    //		if (EffectCurrency.Instance)
    //			EffectCurrency.Instance.GemJump(number);
    //	} else {
    //           GameTracking.Instance.TrackingItemsChange(ItemChangeAction.Gain, ItemTypeTracking.Booster, itemIndex == 3 ? "bomb" : $"powerLevel{itemIndex}", number, location, gameMode, level);
    //           _numberItem[itemIndex] += number;
    //           // GamePlayState.Instance.SetActiveNotifiStore(AllRewardType.IngameItem, itemIndex);

    //           if (IngameNotifiController.Instance)
    //           {
    //               IngameNotifiController.Instance.SetActiveNotifiStorage(AllRewardType.IngameItem, itemIndex);
    //           }
    //           else
    //           {
    //               GamePlayState.Instance.SetActiveNotifiStore(AllRewardType.IngameItem, itemIndex);
    //           }
    //       }
    //}

    //#endregion

    //public void GetReward(CurrencyReward reward) {
    //	//AddGold(reward.Gold);
    //	//AddGem(reward.Gem);
    //	//AddFuel(reward.Fuel);

    //	//SaveData();
    //}

    //public void FinishLevel() {
    //	if (GamePlayState.Instance.ModePlay == ModePlay.Normal) {
    //		while (HighestScore.Count <= GamePlayState.Instance.CurrentLevelSelect)
    //			HighestScore.Add(0);

    //		if (HighestScore[GamePlayState.Instance.CurrentLevelSelect] < GamePlayState.Instance.InGameScore)
    //			HighestScore[GamePlayState.Instance.CurrentLevelSelect] = GamePlayState.Instance.InGameScore;
    //	}

    //	SaveData();
    //}

    //public void EndlessCheckBestRound() {
    //	BackupData backupData = GameData.Instance.BackupData;
    //	if (_playerEndlessInfor.BestWave < backupData.EndlessRound) {
    //		_playerEndlessInfor.BestWave = backupData.EndlessRound;
    //		QuestController.Instance.AddAchievementProgress(QuestType.EndlessRound, addNum: _playerEndlessInfor.BestWave,
    //			isAdd: false);
    //	}
    //}

    //public void FinishEndless() {
    //	BackupData backupData = GameData.Instance.BackupData;
    //	_playerEndlessInfor.TotalScore += backupData.EndlessScore;
    //	EndlessCheckBestRound();
    //	backupData.ClearEndless();
    //	SaveData();
    //}

    //public void AddEndlessSkipItem(int itemId, int number, string location, string gameMode = "null", string level = "null") {
    //       GameTracking.Instance.TrackingItemsChange(ItemChangeAction.Gain, ItemTypeTracking.Booster, $"skipItem{itemId}", number, location, gameMode, level);

    //	_playerEndlessInfor.SkipItemNumber[itemId] += number;
    //	SaveData();
    //       GamePlayState.Instance.SetActiveNotifiStore(AllRewardType.SkipEndless, itemId);

    //       if (IngameNotifiController.Instance)
    //       {
    //           IngameNotifiController.Instance.SetActiveNotifiStorage(AllRewardType.SkipEndless, itemId);
    //       }
    //       else
    //       {
    //           GamePlayState.Instance.SetActiveNotifiStore(AllRewardType.SkipEndless, itemId);
    //       }
    //   }

    //public void UseEndlessSkipItem(int itemId, int number, string location, string gameMode = "null", string level = "null") {
    //       GameTracking.Instance.TrackingItemsChange(ItemChangeAction.Spend, ItemTypeTracking.Booster, $"skipItem{itemId}", number, location, gameMode, level);
    //       _playerEndlessInfor.SkipItemNumber[itemId] -= number;
    //	SaveData();
    //}

    //public void RestoreOnline(PlayerServerInfo info) {
    //	Gold = info.Gold;
    //	Gem = info.Gem;
    //	_indexShip = info.IndexShip;
    //	_indexDrone = info.IndexDrone;
    //	_currentLevel = info.CurrentLevel;

    //	NumberItem = info.NumberItem;
    //	HighestScore = info.HighestScore;

    //	SaveData();
    //}

    //public void BackupOnline(ref PlayerServerInfo info) {
    //	info.Gold = _gold;
    //	info.Gem = _gem;
    //	info.IndexShip = _indexShip;
    //	info.IndexDrone = _indexDrone;

    //	info.CurrentLevel = _currentLevel;

    //	info.NumberItem = NumberItem;
    //	info.HighestScore = _highestScore;
    //}

    //public void FinishHalloween(int wave) {
    //	_halloweenLastWave = wave;
    //	if (wave > _halloweenBestWave) {
    //		_halloweenBestWave = wave;
    //	}
    //	SaveData();
    //}

    public void GetFirstTimeBonus(int id, List<int> bonusType, GameObject bonusSet) {
        if(bonusSet) {
            bonusSet.SetActive(bonusType[id] != 1);
        }
    }

    //public void CheckBonusBuy(int id, List<int> bonusType, GameObject bonusSet) {
    //	if (bonusType[id] == 2) {
    //		bonusType[id] = 1;
    //		bonusSet.SetActive(false);
    //		SaveData();
    //	}
    //}

    public void CheckBonusBuy(int id, List<int> lsBonus, Action<bool> setActiveBonus) {
        if(lsBonus[id] == 2) {
            lsBonus[id] = 1;
            setActiveBonus(false);
            Save();
        }
    }

    //#endregion

    //#region PlayerProfile

    //public void ChangeName(string newName) {
    //	_playerProfile.Name = newName;
    //	SaveData();
    //}

    //public void ChangeFavoriteShip(int shipID) {
    //	_playerProfile.FavoriteShipID = shipID;
    //	SaveData();
    //}

    //public void ChangeAvatar(int avatarID) {
    //	_playerProfile.AvatarID = avatarID;
    //	SaveData();
    //}

    //public void SetVip(bool isVip) {
    //	_playerProfile.IsVip = isVip;
    //	SaveData();
    //}

    //#endregion

    public void GetAllRewardType(AllRewardType type, int extendId, int number, string location = "null", string gameMode = "null", string level = "null") {
        Inventory inventory = ManagerData.Instance.Inventory;
        AircraftData curAircraftData = ManagerData.Instance.AircraftData;
        switch(type) {
            case AllRewardType.Coin:
                AddGold(number, location, gameMode, level);
                break;
            case AllRewardType.Gem:
                AddGem(number, location, gameMode, level);
                break;
            case AllRewardType.Fuel:
                AddFuel(number, location, gameMode, level);
                break;
            case AllRewardType.Box:
                inventory.AddTreasure((TreasureType)extendId, number, location, gameMode, level);
                inventory.SaveData();
                break;
            case AllRewardType.Candy:
                inventory.AddCandy(number, location, gameMode, level);
                inventory.SaveData();
                break;
            //case AllRewardType.IngameItem:
            //	AddItem(extendId, number, location, gameMode, level);
            //	break;
            //case AllRewardType.Ship:
            //	curAircraftData.BuyShip(extendId);

            //	break;
            //case AllRewardType.Drone:
            //	curAircraftData.BuyDrone(extendId);
            //	UIManager.Instance.EquipmentPage.LoadDataCardFromInventory();
            //	break;
            //   case AllRewardType.SkipEndless:
            //	AddEndlessSkipItem(extendId, number, location, gameMode, level);
            //	break;
            case AllRewardType.Pumpkin:
                inventory.AddHalloweenItem((HalloweenItemType)extendId, number, location, gameMode, level);

                break;
            case AllRewardType.DailyChallengeTicket:
                inventory.AddExTicket(number, location, gameMode, level);
                break;
            //case AllRewardType.ShipPart:
            //    curAircraftData.AddAircraftPart(true, extendId, number, location);
            //    break;
            //case AllRewardType.DronePart:
            //    curAircraftData.AddAircraftPart(false, extendId, number, location);
            //    break;
            case AllRewardType.UnlockLvl:
                if(number >= 0) {
                    levelUnlock = number;
                    Save();
                }
                break;
        }
    }

    //#region AdsWatched

    public void AddWatchedAds() {
        AdsWatchedPerDay += 1;
        Save();
    }

    //public void SetAdsProperty() {
    //	if (AdsWatchedPerDay < 2) {
    //		Users.SetUserProperty("AdsWatcher", "<2");
    //	}
    //	else if (AdsWatchedPerDay >= 5) {
    //           Users.SetUserProperty("AdsWatcher", ">=5");
    //	}
    //	else {
    //           Users.SetUserProperty("AdsWatcher", "normal");
    //	}

    //	AdsWatchedPerDay = 0;
    //	SaveData();
    //}

    //#endregion

    //#region For editor

    //public void LoadDataForEditor() {
    //	string data = PlayerPrefs.GetString(_playerData);
    //	PlayerInfo d = new PlayerInfo();

    //	if (data != string.Empty)
    //		d = JsonUtility.FromJson<PlayerInfo>(data);
    //	if (d != null) {
    //		IndexShip = d.IndexShip;
    //		Fuel = d.Fuel;
    //		Gold = d.Gold;
    //		Gem = d.Gem;
    //	}
    //}

    //#endregion
}

public class CurrencyReward {
    public int Gold;
    public int Gem;
    public int Fuel;

    public CurrencyReward() { }

    public CurrencyReward(int gold = 0, int gem = 0, int fuel = 0) {
        Gold = gold;
        Gem = gem;
        Fuel = fuel;
    }
}

[Serializable]
public class EndlessInfor {
    public int TotalScore;
    public int BestWave;
    public int CurRank;
    public List<int> SkipItemNumber;
}

[Serializable]
public class PlayerProfile {
    public bool IsVip;
    public string Name;
    public int AvatarID;
    public int FavoriteShipID;
}

public enum AllRewardType {
    Coin, Gem, Fuel, Box, Card, Candy, IngameItem, SkipEndless, Ship, Drone, Pumpkin, DailyChallengeTicket,
    ShipPart, DronePart, UnlockLvl, None
}
public enum AllRewardVipType {
    SafeOff, FuelInfinity, Gem, Coin, Bomd, NoADS
}
public enum AllCurrency {
    Coin, Gem, Ads
}
public static class GetAllRewardType {
    public static AllRewardType ConverToAllRewardType(AllRewardVipType vipType) {
        if(vipType == AllRewardVipType.Bomd) {
            return AllRewardType.IngameItem;
        }
        else if(vipType == AllRewardVipType.Coin) {
            return AllRewardType.Coin;
        }
        else if(vipType == AllRewardVipType.FuelInfinity) {
            return AllRewardType.None;
        }
        else if(vipType == AllRewardVipType.Gem) {
            return AllRewardType.Gem;
        }
        else if(vipType == AllRewardVipType.NoADS) {
            return AllRewardType.None;
        }
        else if(vipType == AllRewardVipType.SafeOff) {
            return AllRewardType.None;
        }
        return AllRewardType.None;
    }
}
[Serializable]
public class UnlockModeData {
    public bool isUnlockEndlessMode = false;
    public bool hasPlayEndlessMode = false;
    public bool isUnlocDailyMode = false;
    public bool hasPlayDailyMode = false;
    public bool isUnlockHalloweenMode = false;
    public bool hasPlayHalloweenMode = false;
    public bool isUnlockBossBattleMode = false;
    public bool hasPlayBossBattleMode = false;
}

[Serializable]
public class ShipEquipedInfo {
    [SerializeField] private IDShip idShip = IDShip.Ship_01;
    [SerializeField] private Direct direct = Direct.Left;
    public ShipEquipedInfo(Direct direct, IDShip idShip) {
        this.direct = direct;
        this.idShip = idShip;
    }
    public IDShip IDShip => idShip;
    public Direct Direct => direct;
    public void UpdateEquiped(Direct direct, IDShip idShip) {
        this.direct = direct;
        this.idShip = idShip;
    }

    public bool CompareDirect(Direct direct) {
        return Direct == direct;
    }

    public bool CompareIDShip(IDShip idShip) {
        return IDShip == idShip;
    }
}

[Serializable]
public class SkillEquipedInfo {
    [SerializeField] private IDSkill idSkill = IDSkill.Shield;
    [SerializeField] private Direct direct = Direct.Left;
    public SkillEquipedInfo(Direct direct, IDSkill idSkill) {
        this.direct = direct;
        this.idSkill = idSkill;
    }
    public IDSkill IDSkill => idSkill;
    public Direct Direct => direct;
    public void UpdateEquiped(Direct direct, IDSkill idSkill) {
        this.direct = direct;
        this.idSkill = idSkill;
    }

    public bool CompareDirect(Direct direct) {
        return Direct == direct;
    }

    public bool CompareIDShip(IDSkill idSkill) {
        return IDSkill == idSkill;
    }
}
