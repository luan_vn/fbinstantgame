﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "StartItemData", menuName = "Datas/StartItemData")]
public class StartItemData : ScriptableObject
{

    private static StartItemData instance;
    public static StartItemData Instance
    {
        get
        {
            if (instance == null)
            {
                instance = Resources.Load<StartItemData>("StartItemData");
                return instance;
            }
            return instance;
        }

        set
        {
            instance = value;
        }
    }

    public List<StartItemInfo> Items;
}

[Serializable]
public class StartItemInfo
{
    public string Name;
    public Sprite Icon;

    public string Description;
    public int CurrentLevel;

    public List<ItemPackInfo> Cost;
}

[Serializable]
public class ItemPackInfo
{
    public string Description;
    public string SubDescription;
    public int Cost;
    public int NumberItem;
}