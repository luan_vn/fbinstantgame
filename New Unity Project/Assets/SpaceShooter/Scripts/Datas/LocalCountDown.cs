﻿using System;
using System.Collections;
using System.Collections.Generic;
using Gemmob.Common;
using Spine;
using UnityEngine;

public class LocalCountDown : MonoBehaviour {
    public bool IsAutoLoad = true;
    public bool IsContinue = false;
    public int MaxTime;
    public Action OnStateChanged;
    public Action OnComplete;
    public Action OnTick;
    public Action<int> OnLoadDataComplete;
    public int NumberTimeCount = 0;

    private Coroutine _count;
    [SerializeField] private bool _isDebug = false;

    public bool Done {
        get { return PlayerPrefs.GetInt(_done, 1) == 1; }
        set {
            PlayerPrefs.SetInt(_done, value ? 1 : 0);

            if (OnStateChanged != null)
                OnStateChanged.Invoke();
        }
    }

    public TimeSpan TimeCount;
    private readonly TimeSpan _deltaTime = new TimeSpan(0, 0, 0, -1);
    private string _lastTime;
    private string _countTime;
    private string _done;

    void Awake() {
        // Set key
        LoadKeyPP();
    }

    void OnEnable() {
        if (IsAutoLoad)
            LoadData();
    }

    public void LoadData() {
        if (_lastTime == null || _lastTime.Equals(string.Empty))
            LoadKeyPP();

        if (!Done || IsContinue) {
            // Restore
            RestoreCountDown();
            if (!Done) {
                StartCountDown(false);
            }
        }
    }

    void OnDisable() {
        SaveCountDown();
        StopAllCoroutines();
    }

    void OnDestroy() {
        SaveCountDown();
        StopAllCoroutines();
        RemoveAllAction();
    }

    void OnApplicationPause(bool pauseStatus) {
        if (pauseStatus) {
            _count = null;
            StopAllCoroutines();
            SaveCountDown();
        }
    }

    public void StartCountDown(bool fromStart = true) {
	    Done = false;
        if (fromStart) {
            // Reset time
            TimeCount = TimeSpan.FromSeconds(MaxTime);
        }

        // Start Timer
        if (_count == null)
            _count = StartCoroutine(Tick());
    }

    IEnumerator Tick() {
        while (true) {
            TimeCount = TimeCount.Add(_deltaTime);

            if (OnTick != null)
                OnTick.Invoke();

            if (TimeCount.TotalSeconds <= 0) {
                Done = true;
                _count = null;
                StopAllCoroutines();
                if (OnComplete != null)
                    OnComplete.Invoke();
            }
#if !UNITY_EDITOR
            yield return new WaitForSecondsRealtime(1);       
#else
            yield return new WaitForSeconds(1);
#endif
        }
    }

    public void StopImmediately() {
        Done = true;
        _count = null;
        StopAllCoroutines();
        if (OnStateChanged != null)
            OnStateChanged.Invoke();
    }

    public string GetTime() {
        if (TimeCount.TotalSeconds < 3600)
            return string.Format("{0}:{1:00}", TimeCount.Minutes, TimeCount.Seconds);
        if (TimeCount.TotalSeconds < 86400)
            return string.Format("{0}:{1:00}:{2:00}", TimeCount.Hours, TimeCount.Minutes, TimeCount.Seconds);
        return string.Format("{0}:{1:00}:{2:00}", TimeCount.Days * 24 + TimeCount.Hours, TimeCount.Minutes, TimeCount.Seconds);
    }

    public string GetTimeFull() {
        return string.Format("{0:00}:{1:00}:{2:00}", TimeCount.Days * 24 + TimeCount.Hours, TimeCount.Minutes, TimeCount.Seconds);
    }

    void SaveCountDown() {
        PlayerPrefs.SetString(_lastTime, DateTime.Now.ToString());
        PlayerPrefs.SetInt(_countTime, (int)TimeCount.TotalSeconds);
    }

    void RestoreCountDown() {
        var timeSave = GetTimeSaved();
        if (_isDebug)
            Debug.Log("Time save: " + timeSave);
        var timePass = DateTime.Now > timeSave ? DateTime.Now - timeSave : new TimeSpan();
        var saveCountDown = PlayerPrefs.GetInt(_countTime);
        if (saveCountDown == 0)
            saveCountDown = MaxTime;

        NumberTimeCount = 0;
        if (timePass.TotalSeconds > saveCountDown)
            if (!IsContinue) {
                Done = true;
                StopAllCoroutines();
                if (OnComplete != null)
                    OnComplete.Invoke();
            } else {
                NumberTimeCount = Mathf.FloorToInt(((float)timePass.TotalSeconds - saveCountDown) / MaxTime) + 1;
                var currentTime = MaxTime * NumberTimeCount + saveCountDown - timePass.TotalSeconds;
                TimeCount = TimeSpan.FromSeconds(currentTime);
            } else {
            Done = false;
            TimeCount = TimeSpan.FromSeconds(saveCountDown - timePass.TotalSeconds);
            if (TimeCount.TotalSeconds > MaxTime)
                TimeCount = TimeSpan.FromSeconds(MaxTime);
        }
    }

    public DateTime GetTimeSaved() {
        string str = PlayerPrefs.GetString(_lastTime);
        if (str == string.Empty)
            str = DateTime.Now.ToString();
        return DateTime.Parse(str);
    }

    private void LoadKeyPP() {
        _lastTime = name + "l";
        //_lastTime = "lasttime";
        _countTime = name + "c";
        _done = name + "d";
    }

    private void RemoveAllAction() {
        OnTick = null;
        OnComplete = null;
        OnStateChanged = null;
        OnLoadDataComplete = null;
    }
}
