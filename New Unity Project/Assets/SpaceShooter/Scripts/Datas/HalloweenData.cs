﻿using System;
using System.Collections;
using System.Collections.Generic;
using Gemmob.Common;
using UnityEngine;

[CreateAssetMenu(fileName = "HalloweenData",menuName = "Datas/HalloweenData")]
public class HalloweenData : ScriptableObject
{

    private static HalloweenData instance;
    public static HalloweenData Instance
    {
        get
        {
            if (instance == null)
            {
                instance = Resources.Load<HalloweenData>("HalloweenData");
                return instance;
            }
            return instance;
        }

        set
        {
            instance = value;
        }
    }

    public List<ItemData> itemInfor = new List<ItemData>();
	//public HalloweenItemRate rate;
	public List<HalloweenItemRate> rate = new List<HalloweenItemRate>();
	public List<HalloweenRewardInfor> rewardInfor = new List<HalloweenRewardInfor>();
	public List<HalloweenBuyPack> buyPack = new List<HalloweenBuyPack>();
	public List<HalloweenEnemyPow> enemyPow = new List<HalloweenEnemyPow>();

	float[] Rate(int wave) {
		if (wave <= rate[0].fromWave) 
			return rate[0].ToArray();
		else if (wave <= rate[1].fromWave)
			return rate[1].ToArray();
		else if (wave <= rate[2].fromWave)
			return rate[2].ToArray();
		else if (wave <= rate[3].fromWave)
			return rate[3].ToArray();
		else return rate[4].ToArray();
	}

	public int GetItem(int wave)
	{
		float random = UnityEngine.Random.Range(0, 99f);
		int item;
		float[] arrRate = Rate(wave);
		for (item = 0; item < arrRate.Length; item++)
		{
			float check = arrRate[item];
			if (random < check) 
				break;
			random -= check;
		}
		return item;
	}
}

[Serializable]
public class HalloweenItemRate {
	public int fromWave;
	public float common;
	public float rare;
	public float epic;
	public float heroic;
	public float legendary;

	public float[] ToArray()
	{
		return new[] {common, rare, epic, heroic, legendary};
	}
}

[Serializable]
public class PlayerHalloweenItem
{
	public int number;
}

[Serializable]
public class PlayerHalloweenCandy
{
	public int number;
}

public enum HalloweenItemType
{
	Common, Rare, Epic, Heroic, Legendary
}

[Serializable]
public class HalloweenRewardInfor
{
	public AllRewardType Type;
	public int ExtendId;
	public int number;
	public Sprite icon;
	public HalloweenItemNeeded needed;
}

[Serializable]
public class HalloweenItemNeeded
{
	public HalloweenItemType type;
	public int number;
}

[Serializable]
public class HalloweenBuyPack {
	public int number;
	public int price;
}

[Serializable]
public class HalloweenEnemyStats {
	public float boss;
	public float enemy;
}

[Serializable]
public class HalloweenEnemyPow {
	public HalloweenEnemyStats HpScale;
	public HalloweenEnemyStats fireRate;
	public HalloweenEnemyStats atkTime;
}
