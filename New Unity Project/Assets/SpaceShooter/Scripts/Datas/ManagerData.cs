﻿using System;
using System.Collections.Generic;
using Gemmob;
using Gemmob.Common;
using UnityEngine;
using static EnumDefine;

public class ManagerData : SingletonFreeAlive<ManagerData> {
    public static string Region = "None";
    private static bool isRemoveAds = false;
    private const string PreviousVersion = "PreviousVersion";

    public PlayerInfo PlayerInfo;
    public AircraftData AircraftData;
    public SkillsInfo skillsInfo = null;
    public MissionsDataInfo missionInfo = null;
    public UserDailyRewardInfor dailyRewardInfor = null;
    public LevelsDataInfo levelInfo = null;
    public Inventory Inventory;
    public BackupData BackupData;
    public GiveAwaySaver GiveAwaySaver;
    public DailyChallengeSaver DailyChallengeSaver;

    [HideInInspector] public bool isAddTreasure;

    [ContextMenu("ChangeMissionValue")]
    private void ChangeMissionValue() {
        EventDispatcher.Instance.Dispatch(new StructDefine.QuestUpdateValue() { type = QuestType.UpgradeDrone, amount = 1 });
    }
    public ShopCoinData ShopCoinData {
        get {
            return ShopCoinData.Instance;
        }
    }
    public StartItemData StartItemData {
        get {
            return StartItemData.Instance;
        }
    }
    public DailyRewardData DailyRewardData {
        get {
            return DailyRewardData.Instance;
        }
    }
    public SpinData SpinData {
        get {
            return SpinData.Instance;
        }
    }
    public SpritesData ImageData {
        get {
            return SpritesData.Instance;
        }
    }

    public CardData CardData {
        get {
            return CardData.Instance;
        }
    }
    public TreasureData TreasureData {
        get {
            return TreasureData.Instance;
        }
    }

    public DailyChallengeData DailyChallengeData {
        get {
            return DailyChallengeData.Instance;
        }
    }
    public BlackMarketData BlackMarketData {
        get {
            return BlackMarketData.Instance;
        }
    }
    public EndlessData EndlessData {
        get {
            return EndlessData.Instance;
        }
    }
    public PackData PackData {
        get {
            return PackData.Instance;
        }
    }
    public AvatarData AvatarData {
        get {
            return AvatarData.Instance;
        }
    }
    public VipController VipController;
    public BlackMarketSlotData BlackMarketSlotData;
    public UnlockLevelModeData UnlockLevelModeData {
        get {
            return UnlockLevelModeData.Instance;
        }
    }
    private TimeSpan _timer;

    protected override void OnAwake() {
        _timer = DateTime.Now.TimeOfDay;
        LoadData();
        //BackupData.AddCurrency();

        //CheckVersion();
    }


    public void OnReset() {
        Resources.UnloadUnusedAssets();
    }


    public static bool IsRemoveAds {
        get {
            return isRemoveAds;
        }
        set {
            if(isRemoveAds == value)
                return;

            isRemoveAds = value;
            PlayerPrefs.SetInt("IsRemoveAds", isRemoveAds ? 1 : 0);
        }
    }

    private bool _isSaved = false;
    private bool _isVideo = false;
    private bool _watchFirstTime = true;

    private void Start() {
        UnityIAP.Instance.Preload();
    }

    void OnApplicationPause(bool isPause) {
        if(!isPause && _isSaved)
            LoadAds(CurrentLoad);
        else {
            SavePlayerInfo();
            _isSaved = true;
        }
    }

    [ContextMenu("Load data")]
    void LoadData() {
        Debug.Log("Start load data");
        //CountDownController.Instance.CountDownFuel.LoadData();
        AircraftData = new AircraftData().LoadShips();
        PlayerInfo = new PlayerInfo().LoadData();
        skillsInfo = new SkillsInfo().LoadData();
        missionInfo = new MissionsDataInfo().LoadData();
        dailyRewardInfor = new UserDailyRewardInfor().LoadData();
        levelInfo = new LevelsDataInfo().LoadData();
    }

    public UIShipPreview GetShipPreview(EnumDefine.IDShip idShip) {
        return Resources.Load<UIShipPreview>(string.Format("Prefabs/ShipPreview/{0}", idShip.ToString().Replace("_", "-")));
    }

    public void CurrentLoad() {
        CountDownController.Instance.LoadData();
        // PlayerInfo.LoadData();
        Inventory.LoadData();
    }

    [ContextMenu("Use fuek")]
    public void UseFuels() {
        //  PlayerInfo.UseFuel(1, "contextMenu");
    }

    [ContextMenu("SavePlayerInfo")]
    public void SavePlayerInfo() {
        AircraftData.Save();
        PlayerInfo.Save();
    }



    public float CalculateHpScale(float levelCp) {
        float playerCp = GetPlayerCp();
        return levelCp > playerCp ? (levelCp / playerCp) * 2 : (levelCp / playerCp) * 1.5f;
    }

    public int GetPlayerCp() {
        var cpShip = 0;
        if(PlayerInfo.shipEquipeds.Count >= 0) {
            foreach(var item in PlayerInfo.shipEquipeds) {
                var hardData = ShipsData.Instance.GetUpgradeShip(item.IDShip);
                var softData = AircraftData.GetDataShipInfo(item.IDShip);
                cpShip += hardData.GetLevelData(softData.Level).statses.GetStats(EnumDefine.IDStats.Power).Value;
            }
        }
        var cpCard = Mathf.CeilToInt(GetNumeralCard(CardType.CombatPower, 0));
        return cpShip + cpShip + cpCard;
    }

    void LoadAds(Action afterLoad) {
        afterLoad.Invoke();

        _isVideo = false;
    }

    public void DontLoadAds() {
        _isVideo = true;
    }

    #region Load data from card 
    [ContextMenu("Get infor card")]
    public Card GetInformationCard() {
        if(PlayerInfo.Card.Index <= -1) {
            return null;
        }

        var card = CardData.GetInfo(PlayerInfo.Card.Index);
        return card;
    }

    public bool IsHaveCard() {
        return PlayerInfo.Card.Index >= 0;
    }

    public float GetNumeralCard(CardType type, int index) {
        var indexFake = index < 0 ? 0 :
            index >= PlayerInfo.Card.Numeral.Count ? PlayerInfo.Card.Numeral.Count - 1 : index;

        return CheckCardType(type) ? PlayerInfo.Card.Numeral[indexFake] : 0;
    }

    public float GetPercentCard(CardType type, int index) {
        var indexFake = index < 0 ? 0 :
            index >= PlayerInfo.Card.Numeral.Count ? PlayerInfo.Card.Numeral.Count - 1 : index;

        return CheckCardType(type) ? (1 + PlayerInfo.Card.Numeral[indexFake] / 100) : 1;
    }

    public bool CheckCardType(CardType type) {
        var cardIndex = PlayerInfo.Card.Index;
        return cardIndex >= 0 && cardIndex <= CardData.Cards.Count && CardData.Cards[cardIndex].Name == type;
    }

    private void CheckVersion() {
        var currentVer = PlayerPrefs.GetString(PreviousVersion, "");

        if(currentVer != Application.version) {
            PlayerPrefs.SetString(PreviousVersion, Application.version);

            ResetCardData();
        }
    }

    private void ResetCardData() {
        if(PlayerInfo.Card.Index != -1 && CardData.Cards[PlayerInfo.Card.Index].IsNeedUpdated) {
            PlayerInfo.Card.Numeral = CardData.GetInfo(PlayerInfo.Card.Index, PlayerInfo.Card.Level).Numeral;
        }

        if(Inventory.Cards != null)
            for(int i = 0; i < Inventory.Cards.Count; i++) {
                Inventory.Cards[i].Numeral = CardData.GetInfo(Inventory.Cards[i].Index, Inventory.Cards[i].Level).Numeral;
            }
    }

    #endregion

    #region Add item for player 
    public void AddnewItem(TypeReward type, int number, bool isSave = true, string location = "null", string gamePlay = "null", string level = "null") {
        switch(type) {
            //case TypeReward.Coin:
            //	PlayerInfo.AddGem(number);
            //	break;
            //case TypeReward.Gem:
            //	PlayerInfo.AddGem(number);
            //	break;
            case TypeReward.Fuel:
                //   PlayerInfo.AddFuel(number, location, gamePlay, level);
                break;
            case TypeReward.Item3:
                //  PlayerInfo.AddItem(0, number, location, gamePlay, level);
                break;
            case TypeReward.Item5:
                // PlayerInfo.AddItem(1, number, location, gamePlay, level);
                break;
            case TypeReward.Item10:
                //  PlayerInfo.AddItem(2, number, location, gamePlay, level);
                break;
            case TypeReward.Ship:
                //AircraftData.BuyShip(number, false);
                break;
            case TypeReward.Bomb:
                //    PlayerInfo.AddItem(3, number, location, gamePlay, level);
                break;
        }

        // if (isSave)
        //  PlayerInfo.SaveData();
    }

    #endregion

    #region Function for Aircraft

    #region Upgrade and Buy ship

    private int GetLevelShip(EnumDefine.IDShip idShip) {
        var dataShip = ShipsData.Instance.GetUpgradeShip(idShip);
        return dataShip.TotalLevel;
    }

    private bool UpgradeAircraftByCoin(int number) {

        return true;
    }

    private bool UpgradeShipByGem(int number) {

        return true;
    }

    //public bool Upgrade(EquipmentType type, int order, bool isCoin) {
    //    bool isUseCurrencySuccess = false;

    //    switch(type) {
    //        case EquipmentType.Drone:
    //            var currentLevelDrone = AircraftData.Drones[order].Level;
    //            var valueCostDrone = isCoin ?
    //                currentLevelDrone == -1 ? DronesUpgradeData.Drones[order].CoinPrice : DronesUpgradeData.Drones[order].Levels[currentLevelDrone].Coin :
    //                currentLevelDrone == -1 ? DronesUpgradeData.Drones[order].GemPrice : DronesUpgradeData.Drones[order].Levels[currentLevelDrone].Gem;
    //            //  string location = UIManager.Instance.EquipmentPage.CurrentNameMenu();
    //            //  isUseCurrencySuccess =
    //            //   isCoin
    //            //   ? PlayerInfo.UseGold(valueCostDrone, $"upgradeDrone{order}", location, canDiscount: true)
    //            //   : PlayerInfo.UseGem(valueCostDrone, $"upgradeDrone{order}", location, canDiscount: true);

    //            if(isUseCurrencySuccess) {
    //                if(currentLevelDrone == -1) {
    //                    AircraftData.BuyDrone((EnumDefine.IDDrone)order);
    //                }
    //                else {
    //                    AircraftData.UpgradeDrone(order, GetLevelDrone(order), isCoin ? Currency.Gold : Currency.Gem);
    //                }

    //                //  Tracking.LogAircraft(order, false, CurrentAircraftData.GetAircraftInfor(false, order).Level);
    //            }

    //            return isUseCurrencySuccess;
    //        case EquipmentType.Ship:
    //            var currentLevelShip = AircraftData.Ships[order].Level;
    //            var valueCostShip = isCoin ?
    //                currentLevelShip == -1 ? ShipsUpgradeData.GetUpdateShip((EnumDefine.IDShip)order).CoinPrice : ShipsUpgradeData.GetUpdateShip((EnumDefine.IDShip)order).Levels[currentLevelShip].Coin :
    //                currentLevelShip == -1 ? ShipsUpgradeData.GetUpdateShip((EnumDefine.IDShip)order).GemPrice :  ShipsUpgradeData.GetUpdateShip((EnumDefine.IDShip)order).Levels[currentLevelShip].Gem;
    //            //  string location1 = UIManager.Instance.EquipmentPage.CurrentNameMenu();
    //            // isUseCurrencySuccess =
    //            //   isCoin
    //            //     ? PlayerInfo.UseGold(valueCostShip, $"upgradeShip{order}", location1, canDiscount: true)
    //            //    : PlayerInfo.UseGem(valueCostShip, $"upgradeShip{order}", location1, canDiscount: true);

    //            if(isUseCurrencySuccess) {
    //                if(currentLevelShip == -1) {
    //                    AircraftData.BuyShip(order);
    //                }
    //                else {
    //                    AircraftData.UpgradeShip(order, GetLevelShip(order), isCoin ? Currency.Gold : Currency.Gem);
    //                }

    //                //   Tracking.LogAircraft(order, true, CurrentAircraftData.GetAircraftInfor(true, order).Level);
    //            }

    //            return isUseCurrencySuccess;
    //    }

    //    return false;
    //}

    //public bool WatchVideo(EquipmentType type, int order) {
    //    bool isWatchFinish = false;

    //    switch(type) {
    //        case EquipmentType.Drone:
    //            AircraftData.Drones[order].VideoSeen++;
    //            isWatchFinish = AircraftData.Drones[order].VideoSeen >= DronesUpgradeData.Drones[order].VideoCount;

    //            if(isWatchFinish)
    //                AircraftData.UpgradeDrone(order, GetLevelDrone(order), Currency.Video);

    //            return isWatchFinish;
    //        case EquipmentType.Ship:

    //            ++AircraftData.Ships[order].VideoSeen;
    //            AircraftData.SaveShip();
    //            Debug.LogError("Seen ++ = " + AircraftData.Ships[order].VideoSeen);
    //            isWatchFinish = AircraftData.Ships[order].VideoSeen >= ShipsUpgradeData.GetUpdateShip((EnumDefine.IDShip)order).VideoCount;

    //            if(isWatchFinish)
    //                AircraftData.UpgradeShip(order, GetLevelShip(order), Currency.Video);

    //            return isWatchFinish;
    //    }

    //    return false;
    //}



    #endregion
    #endregion

    public Sprite GetIconReward(RewardsType rewardsType, IDCurrency idCurrency = IDCurrency.Coin, IDShip idShip = IDShip.Ship_01) {
        switch(rewardsType) {
            case RewardsType.Currency:
                return CurrenciesData.Instance.GetCurrency(idCurrency).Icon;
            case RewardsType.Debris:
                return ShipsData.Instance.GetUpgradeShip(idShip).GetIcon(Rank.D);
            case RewardsType.Ship:
                var softData = AircraftData.GetDataShipInfo(idShip);
                return ShipsData.Instance.GetUpgradeShip(idShip).GetIcon(softData.Rank);
            default:
                return null;
        }
    }

    public LevelData GetLevelData(int level) {
        var levelPath = string.Format("Levels/Level {0:00}", level);
        return Resources.Load<LevelData>(levelPath);
    }
}

[Serializable]
public class LevelsDataInfo {
    private string keyLevel = "levelData";
    public List<LevelDataInfo> data = new List<LevelDataInfo>();

    public LevelsDataInfo() {
        data = new List<LevelDataInfo>(72);
    }

    public LevelsDataInfo LoadData() {
        LevelsDataInfo data = this;
        if(PlayerPrefs.HasKey(keyLevel)) {
            var jsonData = PlayerPrefs.GetString(keyLevel);
            data = JsonUtility.FromJson<LevelsDataInfo>(jsonData);
        }
        else {
            Save();
        }
        return data;
    }

    public LevelDataInfo GetLevelDataInfo(int level) {
        if(level < 0 || level >= data.Count) {
            Logs.Log(string.Format("Level {0} not exist", level));
            return new LevelDataInfo(level, EnumDefine.IDModePlay.Easy);
        }
        return data[level];
    }

    public void Save() {
        PlayerPrefs.SetString(keyLevel, JsonUtility.ToJson(this));
    }
}

[Serializable]
public class LevelDataInfo {
    [SerializeField] private int idLevel = 0;
    [SerializeField] private EnumDefine.IDModePlay curMode = EnumDefine.IDModePlay.Easy;
    public LevelDataInfo(int idLevel, EnumDefine.IDModePlay curMode) {
        this.idLevel = idLevel;
        this.curMode = curMode;
    }

    public int ID => idLevel;

    public EnumDefine.IDModePlay CurMode => curMode;
}