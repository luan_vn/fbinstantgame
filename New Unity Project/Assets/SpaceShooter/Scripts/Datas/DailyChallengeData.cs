﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "DailyChallengeData",menuName = "Datas/DailyChallengeData")]
public class DailyChallengeData : ScriptableObject {

    private static DailyChallengeData instance;
    public static DailyChallengeData Instance
    {
        get
        {
            if (instance == null)
            {
                instance = Resources.Load<DailyChallengeData>("DailyChallengeData");
                return instance;
            }
            return instance;
        }

        set
        {
            instance = value;
        }
    }

    public List<DailyChallenge> LsChallenge;
	public List<DailyChallengeReward> Rewards;

	public int RandomLvl(int index) {
		DailyChallenge challenge = LsChallenge[index];
		int random = UnityEngine.Random.Range(challenge.MinLvl, challenge.MaxLvl + 1);
		return random;
	}
}

[Serializable]
public class DailyChallenge {
	public int MinLvl;
	public int MaxLvl;
	public int Reward;
}

[Serializable]
public class DailyChallengeReward {
	public AllRewardType Type;
	public int ExtendId;
	public int Number;
	public Sprite Icon;
	public int Needed;
}