
Up_Drone_02.png
size: 512,512
format: RGBA8888
filter: Linear,Linear
repeat: none
D2_01
  rotate: false
  xy: 2, 198
  size: 88, 147
  orig: 88, 147
  offset: 0, 0
  index: -1
D2_11
  rotate: false
  xy: 170, 350
  size: 88, 44
  orig: 88, 44
  offset: 0, 0
  index: -1
Lv2_01
  rotate: false
  xy: 93, 355
  size: 75, 150
  orig: 75, 150
  offset: 0, 0
  index: -1
Lv2_L_01
  rotate: false
  xy: 2, 87
  size: 63, 109
  orig: 63, 109
  offset: 0, 0
  index: -1
Lv2_L_11
  rotate: false
  xy: 2, 2
  size: 13, 23
  orig: 13, 23
  offset: 0, 0
  index: -1
Lv2_L_21
  rotate: false
  xy: 170, 396
  size: 63, 109
  orig: 63, 109
  offset: 0, 0
  index: -1
Lv2_R_01
  rotate: false
  xy: 235, 396
  size: 63, 109
  orig: 63, 109
  offset: 0, 0
  index: -1
Lv2_R_11
  rotate: false
  xy: 441, 313
  size: 13, 23
  orig: 13, 23
  offset: 0, 0
  index: -1
Lv2_R_21
  rotate: false
  xy: 300, 396
  size: 63, 109
  orig: 63, 109
  offset: 0, 0
  index: -1
Lv4_01
  rotate: false
  xy: 2, 347
  size: 89, 158
  orig: 89, 158
  offset: 0, 0
  index: -1
Lv4_L_01
  rotate: false
  xy: 67, 100
  size: 67, 96
  orig: 67, 96
  offset: 0, 0
  index: -1
Lv4_L_11
  rotate: false
  xy: 2, 27
  size: 52, 58
  orig: 52, 58
  offset: 0, 0
  index: -1
Lv4_R_01
  rotate: false
  xy: 260, 298
  size: 67, 96
  orig: 67, 96
  offset: 0, 0
  index: -1
Lv4_R_11
  rotate: false
  xy: 441, 338
  size: 52, 58
  orig: 52, 58
  offset: 0, 0
  index: -1
Lv5_L_01
  rotate: false
  xy: 365, 398
  size: 65, 107
  orig: 65, 107
  offset: 0, 0
  index: -1
Lv5_L_11
  rotate: false
  xy: 329, 303
  size: 54, 91
  orig: 54, 91
  offset: 0, 0
  index: -1
Lv5_R_01
  rotate: false
  xy: 432, 398
  size: 65, 107
  orig: 65, 107
  offset: 0, 0
  index: -1
Lv5_R_11
  rotate: false
  xy: 385, 305
  size: 54, 91
  orig: 54, 91
  offset: 0, 0
  index: -1
