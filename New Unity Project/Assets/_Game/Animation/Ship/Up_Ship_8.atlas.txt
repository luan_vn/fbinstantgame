
Up_Ship_8.png
size: 512,512
format: RGBA8888
filter: Linear,Linear
repeat: none
Ellipse
  rotate: false
  xy: 441, 105
  size: 54, 54
  orig: 54, 54
  offset: 0, 0
  index: -1
Lv1_L_01
  rotate: false
  xy: 230, 110
  size: 72, 98
  orig: 72, 98
  offset: 0, 0
  index: -1
Lv1_L_31
  rotate: false
  xy: 452, 261
  size: 51, 98
  orig: 51, 98
  offset: 0, 0
  index: -1
Lv1_R_01
  rotate: true
  xy: 304, 95
  size: 72, 98
  orig: 72, 98
  offset: 0, 0
  index: -1
Lv1_R_31
  rotate: false
  xy: 452, 161
  size: 51, 98
  orig: 51, 98
  offset: 0, 0
  index: -1
Lv2_L_01
  rotate: false
  xy: 404, 81
  size: 35, 86
  orig: 35, 86
  offset: 0, 0
  index: -1
Lv2_L_31
  rotate: true
  xy: 2, 12
  size: 47, 78
  orig: 47, 78
  offset: 0, 0
  index: -1
Lv2_L_41
  rotate: false
  xy: 2, 61
  size: 71, 147
  orig: 71, 147
  offset: 0, 0
  index: -1
Lv2_R_01
  rotate: false
  xy: 474, 17
  size: 35, 86
  orig: 35, 86
  offset: 0, 0
  index: -1
Lv2_R_31
  rotate: true
  xy: 82, 12
  size: 47, 78
  orig: 47, 78
  offset: 0, 0
  index: -1
Lv2_R_41
  rotate: false
  xy: 75, 61
  size: 71, 147
  orig: 71, 147
  offset: 0, 0
  index: -1
Lv3_02
  rotate: true
  xy: 299, 8
  size: 85, 82
  orig: 85, 82
  offset: 0, 0
  index: -1
Lv3_L_01
  rotate: true
  xy: 307, 265
  size: 94, 143
  orig: 94, 143
  offset: 0, 0
  index: -1
Lv3_L_11
  rotate: true
  xy: 383, 4
  size: 75, 89
  orig: 75, 89
  offset: 0, 0
  index: -1
Lv3_R_01
  rotate: true
  xy: 307, 169
  size: 94, 143
  orig: 94, 143
  offset: 0, 0
  index: -1
Lv4_01
  rotate: false
  xy: 162, 35
  size: 65, 75
  orig: 65, 75
  offset: 0, 0
  index: -1
Lv4_31
  rotate: true
  xy: 148, 112
  size: 96, 80
  orig: 96, 80
  offset: 0, 0
  index: -1
Lv4_L_01
  rotate: true
  xy: 229, 58
  size: 50, 68
  orig: 50, 68
  offset: 0, 0
  index: -1
Lv4_R_01
  rotate: true
  xy: 229, 6
  size: 50, 68
  orig: 50, 68
  offset: 0, 0
  index: -1
S8_01
  rotate: false
  xy: 2, 210
  size: 303, 297
  orig: 303, 297
  offset: 0, 0
  index: -1
S8_11
  rotate: true
  xy: 307, 361
  size: 146, 194
  orig: 146, 194
  offset: 0, 0
  index: -1

Up_Ship_82.png
size: 128,128
format: RGBA8888
filter: Linear,Linear
repeat: none
Lv3_R_11
  rotate: false
  xy: 2, 2
  size: 75, 89
  orig: 75, 89
  offset: 0, 0
  index: -1
