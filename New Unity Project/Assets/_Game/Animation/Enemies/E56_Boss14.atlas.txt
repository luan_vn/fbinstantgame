
E56_Boss14.png
size: 1024,1024
format: RGBA8888
filter: Linear,Linear
repeat: none
Anten
  rotate: false
  xy: 248, 50
  size: 154, 249
  orig: 154, 249
  offset: 0, 0
  index: -1
Body_01
  rotate: false
  xy: 331, 387
  size: 153, 219
  orig: 153, 219
  offset: 0, 0
  index: -1
Body_11
  rotate: false
  xy: 2, 785
  size: 371, 222
  orig: 371, 222
  offset: 0, 0
  index: -1
Body_21
  rotate: false
  xy: 375, 765
  size: 214, 242
  orig: 214, 242
  offset: 0, 0
  index: -1
Eye
  rotate: false
  xy: 331, 330
  size: 37, 55
  orig: 37, 55
  offset: 0, 0
  index: -1
Head
  rotate: false
  xy: 501, 591
  size: 164, 170
  orig: 164, 170
  offset: 0, 0
  index: -1
Horn
  rotate: false
  xy: 2, 570
  size: 309, 213
  orig: 309, 213
  offset: 0, 0
  index: -1
L_Leg_01
  rotate: false
  xy: 248, 301
  size: 81, 267
  orig: 81, 267
  offset: 0, 0
  index: -1
L_Leg_11
  rotate: false
  xy: 667, 645
  size: 36, 116
  orig: 36, 116
  offset: 0, 0
  index: -1
L_Mouth
  rotate: false
  xy: 989, 971
  size: 19, 36
  orig: 19, 36
  offset: 0, 0
  index: -1
L_Wing_01
  rotate: false
  xy: 2, 330
  size: 244, 238
  orig: 244, 238
  offset: 0, 0
  index: -1
L_Wing_11
  rotate: false
  xy: 749, 763
  size: 155, 244
  orig: 155, 244
  offset: 0, 0
  index: -1
R_Leg_01
  rotate: false
  xy: 906, 740
  size: 81, 267
  orig: 81, 267
  offset: 0, 0
  index: -1
R_Leg_11
  rotate: false
  xy: 705, 645
  size: 36, 116
  orig: 36, 116
  offset: 0, 0
  index: -1
R_Mouth
  rotate: false
  xy: 743, 725
  size: 19, 36
  orig: 19, 36
  offset: 0, 0
  index: -1
R_Wing_01
  rotate: false
  xy: 2, 90
  size: 244, 238
  orig: 244, 238
  offset: 0, 0
  index: -1
R_Wing_11
  rotate: false
  xy: 591, 763
  size: 156, 244
  orig: 156, 244
  offset: 0, 0
  index: -1
Tail_01
  rotate: false
  xy: 313, 608
  size: 186, 155
  orig: 186, 155
  offset: 0, 0
  index: -1
Tail_11
  rotate: false
  xy: 2, 2
  size: 34, 86
  orig: 34, 86
  offset: 0, 0
  index: -1
Tail_31
  rotate: false
  xy: 667, 599
  size: 39, 44
  orig: 39, 44
  offset: 0, 0
  index: -1
