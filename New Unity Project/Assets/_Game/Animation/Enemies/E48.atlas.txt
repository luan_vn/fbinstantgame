
E48.png
size: 256,128
format: RGBA8888
filter: Linear,Linear
repeat: none
arm 1
  rotate: false
  xy: 2, 2
  size: 13, 14
  orig: 13, 14
  offset: 0, 0
  index: -1
arm 2
  rotate: false
  xy: 17, 2
  size: 13, 14
  orig: 13, 14
  offset: 0, 0
  index: -1
body
  rotate: false
  xy: 158, 64
  size: 79, 61
  orig: 79, 61
  offset: 0, 0
  index: -1
hat
  rotate: true
  xy: 89, 33
  size: 92, 67
  orig: 92, 67
  offset: 0, 0
  index: -1
mouth
  rotate: false
  xy: 89, 14
  size: 43, 17
  orig: 43, 17
  offset: 0, 0
  index: -1
swimming_float
  rotate: true
  xy: 2, 18
  size: 107, 85
  orig: 107, 85
  offset: 0, 0
  index: -1
