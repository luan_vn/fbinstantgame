
E16.png
size: 256,64
format: RGBA8888
filter: Linear,Linear
repeat: none
1
  rotate: true
  xy: 2, 8
  size: 48, 61
  orig: 48, 61
  offset: 0, 0
  index: -1
10
  rotate: false
  xy: 161, 18
  size: 37, 38
  orig: 37, 38
  offset: 0, 0
  index: -1
2
  rotate: false
  xy: 65, 18
  size: 55, 38
  orig: 55, 38
  offset: 0, 0
  index: -1
3
  rotate: true
  xy: 65, 2
  size: 14, 15
  orig: 14, 15
  offset: 0, 0
  index: -1
4
  rotate: true
  xy: 82, 2
  size: 14, 15
  orig: 14, 15
  offset: 0, 0
  index: -1
5
  rotate: false
  xy: 241, 38
  size: 12, 18
  orig: 12, 18
  offset: 0, 0
  index: -1
6
  rotate: false
  xy: 241, 18
  size: 12, 18
  orig: 12, 18
  offset: 0, 0
  index: -1
7
  rotate: false
  xy: 122, 18
  size: 37, 38
  orig: 37, 38
  offset: 0, 0
  index: -1
8
  rotate: false
  xy: 200, 38
  size: 39, 18
  orig: 39, 18
  offset: 0, 0
  index: -1
9
  rotate: false
  xy: 200, 18
  size: 39, 18
  orig: 39, 18
  offset: 0, 0
  index: -1
