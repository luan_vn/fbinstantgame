
E29_Boss8.png
size: 1024,512
format: RGBA8888
filter: Linear,Linear
repeat: none
Body
  rotate: false
  xy: 2, 2
  size: 222, 470
  orig: 222, 470
  offset: 0, 0
  index: -1
Face
  rotate: false
  xy: 737, 332
  size: 161, 140
  orig: 161, 140
  offset: 0, 0
  index: -1
L_Gun_01
  rotate: false
  xy: 497, 142
  size: 118, 330
  orig: 118, 330
  offset: 0, 0
  index: -1
L_Wing_01
  rotate: false
  xy: 226, 242
  size: 269, 230
  orig: 269, 230
  offset: 0, 0
  index: -1
R_Gun_01
  rotate: false
  xy: 617, 142
  size: 118, 330
  orig: 118, 330
  offset: 0, 0
  index: -1
R_Wing_01
  rotate: false
  xy: 226, 10
  size: 269, 230
  orig: 269, 230
  offset: 0, 0
  index: -1
