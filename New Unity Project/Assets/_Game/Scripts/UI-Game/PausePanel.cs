﻿using Gemmob;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class PausePanel : Frame {
    [SerializeField] private ButtonBase btnHome = null;
    [SerializeField] private ButtonBase btnResume = null;
    [SerializeField] private SliderChangeOnOff sliderMusic = null;
    [SerializeField] private SliderChangeOnOff sliderSound = null;
    [SerializeField] private SliderChangeOnOff sliderVibration = null;

    protected override void OnInitialize(HUD hud) {
        base.OnInitialize(hud);
        sliderMusic.Initialize(true);
        sliderSound.Initialize(true);
        sliderVibration.Initialize(true);
        btnHome.AddListener(ButtonHome);
        btnResume.AddListener(ButtonResume);
    }

    protected override void OnShow(Action onCompleted = null, bool instant = false) {
        Time.timeScale = 0;
        base.OnShow(onCompleted, instant);
    }

    private void ButtonHome() {
        Time.timeScale = 1f;
        ManagerScene.Instance.Load(IDScene.Home);
    }

    private void ButtonResume() {
        Time.timeScale = 1f;
        Hide();
    }
}
