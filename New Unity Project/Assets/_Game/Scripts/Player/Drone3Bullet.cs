﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Drone3Bullet : ShipBulletNormal
{
    private Vector3 direction = Vector3.up;
    private Transform targetEnemy = null;
    public LayerMask layerCheck;

    public override ShipBulletBase Initialized() {
        direction = Vector3.up;
        targetEnemy = null;
        return base.Initialized();
    }

    protected override void Update() {
        
        if(targetEnemy==null || !targetEnemy.gameObject.activeInHierarchy ) {
            Collider2D a = Physics2D.OverlapCircle(transform.position, 2f, layerCheck);
            if(a!=null && a.transform.CompareTag(TagDefine.Enemy)) {
                targetEnemy = a.gameObject.transform;
            }
        }

        if(targetEnemy!=null && targetEnemy.gameObject.activeInHierarchy ) {
            direction =Vector2.Lerp(direction,(targetEnemy.transform.position - transform.position).normalized, Time.deltaTime * 10f);
            transform.up = direction;
        }
        transform.Translate(direction * speedMovement * Time.deltaTime);

        CheckOutScene();
    }
}
