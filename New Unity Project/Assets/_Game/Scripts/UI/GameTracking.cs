﻿using Gemmob;
using Gemmob.Common;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameTracking : Singleton<GameTracking> {
    protected override void Initialize() {
        prvLocationButtonPress = "null";
        prvLocationMenuShowup = "null";
    }

    private string prvLocationButtonPress;
    private string prvLocationMenuShowup;
    private float timeStartGame;
    private float timeStartTutorial;


    public void TrackButtonPress(string location, string btnID, string gameMode = "null", string level = "null", string wave = "null") {
        //NewTracking.LogButtonPress(prvLocationButtonPress, location, btnID, gameMode, level, wave);
    }

    public void TrackMenuShowup(string location, string gameMode = "null", string level = "null", string wave = "null") {
        if (location.Equals(prvLocationMenuShowup)) {
            return;
        }
        NewTracking.LogMenuShowup(prvLocationMenuShowup, location, gameMode, level, wave);
        prvLocationButtonPress = prvLocationMenuShowup;
        prvLocationMenuShowup = location;
    }

    //internal static void TrackButtonPress(string v) {
    //    throw new NotImplementedException();
    //}

    public void TrackVideoAds(AdsType adsType, string location, string adsStatus, string gameMode = "null", string level = "null") {
      //  NewTracking.LogVideoAds(adsType.ToString(), location, adsStatus, gameMode, level);
    }

    public void TrackDailyQuestCompleted(string id) {
       // NewTracking.LogDailyQuestCompleted(id);
    }

    public void TrackDailyQuestClaim(string id) {
        //NewTracking.LogDailyQuestClaim(id);
    }

    public void TrackAchievementCompleted(string id) {
       // NewTracking.LogAchievementCompleted(id);
    }

    public void TrackAchievementClaim(string id) {
       // NewTracking.LogAchievementQuestClaim(id);
    }

    public void TrackingIapPromotion(string item_category, string item_id, string item_name) {
        //NewTracking.LogIapPromotion(item_category, item_id, item_name);
    }

    public void TrackingIapPurchased(string item_id, string item_name, IapCategory item_category) {
       // NewTracking.LogIapPurchased(item_id, item_name, item_category.ToString());
    }

    public void TrackingEarnVirtualCurrency(VirtualCurrencyName virtualCurrency, string value, string location, string gameMode = "null", string level = "null") {
       // NewTracking.LogEarnVirtualCurrency(virtualCurrency.ToString(), value, location, gameMode, level);
    }

    public void TrackingSpendVirtualCurrency(string itemName, VirtualCurrencyName virtualCurrency, string value, string location, string gameMode = "null", string level = "null") {
       // NewTracking.LogSpendVirtualCurrency(itemName, virtualCurrency.ToString(), value, location, gameMode, level);
    }

    public void TrackingWeaponPieceChange(string pieceId, ItemChangeAction action, int value, string location) {
       // NewTracking.LogWeaponPieceChange(pieceId, action.ToString(), value.ToString(), location);
    }

    public void TrackingItemsChange(ItemChangeAction action, ItemTypeTracking itemType, string id, int quantity, string location, string gameMode = "null", string level = "null") {
       // NewTracking.LogItemsChange(action.ToString(), itemType.ToString(), id, quantity.ToString(), location, gameMode, level);
    }

    public void TrackingCardsChange(ItemChangeAction action, int cardsLevel, string cardsId, CardPurpose purpose) {
       // NewTracking.LogCardsChange(action.ToString(), cardsLevel.ToString(), cardsId, purpose.ToString());
    }

    public void TrackingWeaponUnlocked(string itemsId, UnlockWay unlockWay) {
       // NewTracking.LogWeaponUnlocked(itemsId, unlockWay.ToString());
    }

    public void TrackingWeaponUpgraded(string itemsId, Currency currency, int level) {
       // NewTracking.LogWeaponUpgraded(itemsId, currency.ToString(), level.ToString());
    }

    public void TrackingCampaignStart(int level, int shipId, int shipLevel, int droneId, int droneLevel, int cardId, int cardLevel) {
        timeStartGame = Time.time;
       // NewTracking.LogCampaignStart(level.ToString(), shipId.ToString(), shipLevel.ToString(), droneId.ToString(), droneLevel.ToString(), cardId.ToString(), cardLevel.ToString());
    }

    public void TrackingCampaignFinish(int level, int wave, string status, int firstWinCount, int shipId, int shipLevel, int droneId, int droneLevel, int cardId, int cardLevel) {
        int duration = (int)(Time.time - timeStartGame);
       // NewTracking.LogCampaignFinish(level.ToString(), wave.ToString(), status, firstWinCount.ToString(), duration.ToString(),
           // shipId.ToString(), shipLevel.ToString(), droneId.ToString(), droneLevel.ToString(), cardId.ToString(), cardLevel.ToString());
    }

    public void TrackingEndlessStart(int shipId, int shipLevel, int droneId, int droneLevel, int cardId, int cardLevel) {
        timeStartGame = Time.time;
        //NewTracking.LogEndlessStart(shipId.ToString(), shipLevel.ToString(), droneId.ToString(), droneLevel.ToString(), cardId.ToString(), cardLevel.ToString());
    }

    public void TrackingEndlessFinish(int wave, int shipId, int shipLevel, int droneId, int droneLevel, int cardId, int cardLevel) {
        int duration = (int)(Time.time - timeStartGame);
       // NewTracking.LogEndlessFinish(wave.ToString(), duration.ToString(),
           // shipId.ToString(), shipLevel.ToString(), droneId.ToString(), droneLevel.ToString(), cardId.ToString(), cardLevel.ToString());
    }

    public void TrackingBossStart(int bossId, int shipId, int shipLevel, int droneId, int droneLevel, int cardId, int cardLevel) {
        timeStartGame = Time.time;
        //NewTracking.LogBossStart(bossId.ToString(), shipId.ToString(), shipLevel.ToString(), droneId.ToString(), droneLevel.ToString(), cardId.ToString(), cardLevel.ToString());
    }

    public void TrackingBossFinish(int bossId, string status, int shipId, int shipLevel, int droneId, int droneLevel, int cardId, int cardLevel) {
        int duration = (int)(Time.time - timeStartGame);
       // NewTracking.LogBossFinish(bossId.ToString(), status, duration.ToString(),
          //  shipId.ToString(), shipLevel.ToString(), droneId.ToString(), droneLevel.ToString(), cardId.ToString(), cardLevel.ToString());
    }

    public void TrackingEventStart(string eventType, int shipId, int shipLevel, int droneId, int droneLevel, int cardId, int cardLevel) {
        timeStartGame = Time.time;
        //NewTracking.LogEventStart(eventType, shipId.ToString(), shipLevel.ToString(), droneId.ToString(), droneLevel.ToString(), cardId.ToString(), cardLevel.ToString());
    }

    public void TrackingEventFinish(string eventType, int wave, string status, int shipId, int shipLevel, int droneId, int droneLevel, int cardId, int cardLevel) {
        int duration = (int)(Time.time - timeStartGame);
        //NewTracking.LogEventFinish(eventType, wave.ToString(), status, duration.ToString(),
          //  shipId.ToString(), shipLevel.ToString(), droneId.ToString(), droneLevel.ToString(), cardId.ToString(), cardLevel.ToString());
    }

    public void TrackingTutorialBegin(string id) {
        timeStartTutorial = Time.time;
       // NewTracking.LogTutorialBegin(id);
    }

    public void TrackingTutorialComplete(string id) {
        int duration = (int)(Time.time - timeStartTutorial);
        //NewTracking.LogTutorialComplete(id, duration.ToString());
    }
}

public enum AdsType
{
    Revive, BonusReward, VideoReward, FreeSpin, Interstitial
}

public enum IapCategory
{
    Null, Discount, Bonus
}

public enum VirtualCurrencyName {
    Gold = 0, Gem = 1, Fuel = 3
}

public enum ItemChangeAction {
    Gain, Spend
}

public enum ItemTypeTracking {
    Booster, Ticket
}

public enum CardPurpose {
    None, Fuse, Upgrade
}

public enum Currency {
    Gold, Gem, Iap, Video
}

public enum UnlockWay {
    Iap, Normal, Ticket
}

