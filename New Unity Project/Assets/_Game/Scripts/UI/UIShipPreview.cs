﻿using Spine.Unity;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UIShipPreview : MonoBehaviour {
    [SerializeField] private SkeletonGraphic skeAnim = null;

    public void SetSkin(string skinKey) {
        if(skeAnim == null) {
            Logs.Log(string.Format("SkeletonAnimation null!"));
            return;
        }
        skeAnim.SetSkin(skinKey);
    }
}
