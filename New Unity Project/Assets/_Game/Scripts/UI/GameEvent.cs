﻿using System;
using Gemmob;
using Gemmob.Common;

public class GameEvent {
	public static Action OnMoveModeChanged;
	public static Action OnMusicStateChanged;
	public static Action OnSoundStateChanged;

	public static Action<int> OnGoldChanged;
	public static Action<int> OnGemChanged;
	public static Action<int, int> OnFuelChanged;

	public struct OnStartGame : IEventParams {
		public int itemId;
		public int shipId;
		public int droneId;

		public OnStartGame(int itemId, int shipId, int droneId) {
			this.itemId = itemId;
			this.shipId = shipId;
			this.droneId = droneId;
		}
	}
	public struct OnStartAttack : IEventParams {
	}
	public struct OnStartNormalMode : IEventParams {
	}

	public struct OnStartBossMode : IEventParams {
	}

	public struct OnStartEndlessMode : IEventParams {
	}



	public struct OnRevive : IEventParams {
	}

	public struct OnStopGame : IEventParams {
	}

	public struct OnStartWave : IEventParams {
		public int totalWave;
		public int currentWave;
		public float combatPower;
		public EnumDefine.ModePlay modePlay;
		public WaveType waveType;

		public OnStartWave(int currentWave, int totalWave, float combatPower, EnumDefine.ModePlay modePlay, WaveType waveType) {
			this.currentWave = currentWave;
			this.totalWave = totalWave;
			this.combatPower = combatPower;
			this.modePlay = modePlay;
			this.waveType = waveType;
		}
	}

	public struct OnEndWave : IEventParams {
		public int totalWave;
		public int currentWave;
	}

	public struct OnChangeDrone : IEventParams {
		public int droneId;
	}



	public struct OnWarningBoss : IEventParams {
	}

	public static void ChangeDrone(int droneId) {
		EventDispatcher.Instance.Dispatch(new OnChangeDrone { droneId = droneId });
	}
}
