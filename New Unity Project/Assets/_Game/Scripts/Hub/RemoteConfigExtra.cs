﻿using System;
using System.Collections;
using System.Threading.Tasks;
using UnityEngine;
//using System.Threading.Tasks;
using Gemmob.Common;
using UnityEngine.Events;
using System.Linq;
#if FIREBASE_ENABLE
using Firebase.RemoteConfig;
#endif

public class RemoteConfigExtra : MonoBehaviour {

    public static bool? RemoteFetched;

    // Sale Shop
    public static string SaleCoin;
    public static string SaleGem;
    public static string SaleTime;
    public static bool IsHasSale;

    // PackSale
    public static int SaleType;

    // Gift code
    public static string GiftType;
    public static string Number;
    public static string Code;
    public static string GiftExtendId;

    //starter pack 5 open
    public static bool IsOpenPack5;

    // A|B testing iap
    public static bool IsVideoDoubleIap;

#if FIREBASE_ENABLE
    void Awake() {
        GetGiftCode();
        GetSaleType();
        GetOpenPack5();
        GetVideoDoubleIap();
    }
    void OnEnable() {
        GetGiftCode();
        GetSaleType();
        GetOpenPack5();
        GetVideoDoubleIap();
    }

    private void Fetch() {
        if (RemoteFetched == null || RemoteFetched == false) {
            var settings = FirebaseRemoteConfig.Settings;
            settings.IsDeveloperMode = true;
            FirebaseRemoteConfig.Settings = settings;
            Task fetchTask = FirebaseRemoteConfig.FetchAsync(new TimeSpan(0));
            fetchTask.ContinueWith(task => {
                if (task.IsCanceled || task.IsFaulted) {
                    Debug.Log("[REMOTE-CONFIG] Failed");
                    RemoteFetched = false;
                } else {
                    Debug.Log("[REMOTE-CONFIG] Completed");
                    FirebaseRemoteConfig.ActivateFetched();
                    RemoteFetched = true;
                }
            });
        }
    }

    void GetSale() {
        Fetch();
        StartCoroutine(WaitUntilRemoteFetched(() => {
            SaleCoin = FirebaseRemoteConfig.GetValue("sale_coin").StringValue;
            //Logs.Log(SaleCoin);
            SaleGem = FirebaseRemoteConfig.GetValue("sale_gem").StringValue;
            //Logs.Log(SaleGem);
            SaleTime = FirebaseRemoteConfig.GetValue("sale_time").StringValue;
            //Logs.Log(SaleTime);
            if (SaleCoin == "" && SaleGem == "") {
                IsHasSale = false;
            } else if (SaleCoin == "20" && SaleGem == "30") {
                IsHasSale = true;
            } else if (SaleCoin == "30" && SaleGem == "40") {
                IsHasSale = true;
            }
        }));
    }

    public void GetGiftCode() {
        Fetch();
        StartCoroutine(WaitUntilRemoteFetched(() => {
            Code = FirebaseRemoteConfig.GetValue("GCCode").StringValue;
            Number = FirebaseRemoteConfig.GetValue("GCNumber").StringValue;
            GiftType = FirebaseRemoteConfig.GetValue("GCType").StringValue;
            GiftExtendId = FirebaseRemoteConfig.GetValue("GCExtendId").StringValue;
        }));
    }

    void GetSaleType() {
        Fetch();
        StartCoroutine(WaitUntilRemoteFetched(() => {
            SaleType = Int32.Parse(FirebaseRemoteConfig.GetValue("sale_type").StringValue);
        }));
    }

    void GetOpenPack5() {
        Fetch();
        StartCoroutine(WaitUntilRemoteFetched(() => {
            Debug.Log("Get Remote");
            IsOpenPack5 = FirebaseRemoteConfig.GetValue("OpenPack5").BooleanValue;
        }));
    }

    void GetVideoDoubleIap() {
        Fetch();
        StartCoroutine(WaitUntilRemoteFetched(() => {
            bool hasABTesting = FirebaseRemoteConfig.Keys.ToList<string>().Contains("VideoDoubleIap");
            if (hasABTesting) {
                IsVideoDoubleIap = FirebaseRemoteConfig.GetValue("VideoDoubleIap").BooleanValue;
            } else {
                IsVideoDoubleIap = false;
            }
        }));
    }
#endif
    public static IEnumerator WaitUntilRemoteFetched(UnityAction action, float delayCall = 0f) {
        yield return new WaitUntil(() => RemoteFetched == true);
        yield return new WaitForSeconds(delayCall);
        action();
    }
}

