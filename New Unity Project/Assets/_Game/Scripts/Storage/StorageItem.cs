﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class StorageItem : MonoBehaviour, IPointerDownHandler, IPointerUpHandler {
	[HideInInspector] public AllRewardType Type;
	[HideInInspector] public int Id;
	[SerializeField] private Image _icon;
    [SerializeField] private Image _imgCategory;
	[SerializeField] private Text _txtNumber;
	[SerializeField] private Text _txtInfor;

	public void Initialize(StoragePage storagePage) {

    }

	public void UpdateUI() { 
	
	}
	
	void SetItem(Sprite icon, int number, int infor = 0, bool isActiveInfor = false, int sizeWidth = 160) {
		_icon.sprite = icon;
		_icon.SetSizeFollowWidth(sizeWidth);
		_txtNumber.text = number.ToString();
		_txtInfor.gameObject.SetActive(isActiveInfor);
		_txtInfor.text = infor.ToString();
	}

	public void SetItem(AllRewardType type, int id) {
		Type = type;
		Id = id;
		PlayerInfo player = ManagerData.Instance.PlayerInfo;
		ShipsData shipData = ShipsData.Instance;
		DronesUpgradeData droneData = DronesUpgradeData.Instance;
		AircraftData aircraftData = ManagerData.Instance.AircraftData;
		StartItemData startItemData = ManagerData.Instance.StartItemData;
		EndlessData endlessData = ManagerData.Instance.EndlessData;
		
		switch (type) {
			case AllRewardType.ShipPart:
				//SetItem(shipData.GetUpgradeShip((EnumDefine.IDShip)id).GetIcon, aircraftData.Ships[id].);
                _imgCategory.gameObject.SetActive(true);
				break;
			case AllRewardType.DronePart:
			//	SetItem(droneData.Drones[id].Icon, aircraftData.Drones[id].EvolveItemNum);
                _imgCategory.gameObject.SetActive(true);
                break;
			case AllRewardType.IngameItem:
				SetItem(startItemData.Items[id].Icon, player.NumberItem[id]);
                _imgCategory.gameObject.SetActive(false);
                break;
			case AllRewardType.SkipEndless:
                //SetItem(UIManager.Instance.storagePage.EndlessItemCoin, player.PLayerEndlessInfor.SkipItemNumber[id],endlessData.SkipItems[id].Wave, true, 140);
                _imgCategory.gameObject.SetActive(false);
                break;
		}
	}

	public void OnPointerDown(PointerEventData eventData) {
        if (Type != AllRewardType.IngameItem) {
			string infor;
			if (Type == AllRewardType.SkipEndless) {
				EndlessSkipItem item = ManagerData.Instance.EndlessData.SkipItems[Id];
				infor = string.Format("+ {0} waves\n+{1} scores", item.Wave, item.Score);
				//UIManager.Instance.storagePage.SetItemInfor(transform, infor);
			}
			else {
                //string aircraftName =
                //    Type == AllRewardType.ShipPart
                //        ? ManagerData.Instance.ShipsUpgradeData.GetUpdateShip((EnumDefine.IDShip)Id).Name
                //        : ManagerData.Instance.DronesUpgradeData.Drones[Id].Name;
               // infor = string.Format("Use to evolve\n{0}", aircraftName);
              //  UIManager.Instance.storagePage.SetItemInfor(transform, infor);
            }
		}
	}

	public void OnPointerUp(PointerEventData eventData) {
	//	UIManager.Instance.StoragePage.SetItemInfor(transform, string.Empty, false);
	}
}
