﻿using Gemmob;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ItemRocket : ItemBase
{
    [SerializeField] private BulletRocket rocket;
    private BulletRocket bullet;
    // Start is called before the first frame update

    protected override void ActiveEffect() {
        bullet = rocket.Spawn(transform.position);
        bullet.SetDirect(new Vector2(0.5f, 1f));
        bullet.Init();
        bullet = rocket.Spawn(transform.position);
        bullet.SetDirect(new Vector2(-0.5f, 1f));
        bullet.Init();
        bullet = rocket.Spawn(transform.position);
        bullet.SetDirect(new Vector2(1, 1f));
        bullet.Init();
        bullet = rocket.Spawn(transform.position);
        bullet.SetDirect(new Vector2(-1, 1f));
        bullet.Init();
        bullet = rocket.Spawn(transform.position);
        bullet.SetDirect(new Vector2(-1, 0.5f));
        bullet.Init();
        bullet = rocket.Spawn(transform.position);
        bullet.SetDirect(new Vector2(1, 0.5f));
        bullet.Init();
    }
}
