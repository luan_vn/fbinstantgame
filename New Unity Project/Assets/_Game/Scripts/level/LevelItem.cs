﻿using System.Collections;
using System.Collections.Generic;
using Gemmob;
using Spine.Unity;
using TMPro;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class LevelItem : MonoBehaviour {
    private ButtonBase buttonSelect = null;
    private SelectLevelPage selectLevelPage = null;
    private const bool turnOnColorBossUnActive = true;
    public int Level;
    public TextMeshProUGUI txtLevel;
    public GameObject goLock;
    public GameObject Boss;
    public List<Image> Dots;
    public SkeletonGraphic[] Skeleton;
    [SerializeField] private Image imgRound = null;
    [SerializeField] private Image imgLock = null;
    public void Initialize(SelectLevelPage selectLevelPage) {
        this.selectLevelPage = selectLevelPage;
        buttonSelect = buttonSelect ?? GetComponentInChildren<ButtonBase>();
        if(buttonSelect) { buttonSelect.onClick.AddListener(SelectLevel); }
        if(Boss) { Boss.transform.eulerAngles = Vector3.zero; }
        var colorDeactive = GameConfig.colorLevelCantPlay;
        var unlock = Level <= ManagerData.Instance.PlayerInfo.LevelUnlock;
        Extention.SetText(txtLevel, (Level + 1).ToString());
        Extention.SetActiveObject(goLock, !unlock);
        Extention.SetActiveObject(txtLevel.gameObject, unlock);
        if(unlock) {
            imgRound.color = Color.white;
            imgLock.color = Color.white;
            if(Skeleton != null) { for(int i = 0; i < Skeleton.Length; i++) { Skeleton[i].color = Color.white; } }
            for(int i = 0; i < Dots.Count; i++) { Dots[i].color = Color.white; }
        }
        else {
            if(Skeleton != null) { for(int i = 0; i < Skeleton.Length; i++) { Skeleton[i].color = turnOnColorBossUnActive ? Color.white : colorDeactive; } }
            imgRound.color = colorDeactive;
            imgLock.color = colorDeactive;
            for(int i = 0; i < Dots.Count; i++) { Dots[i].color = colorDeactive; }
        }
    }

    void SelectLevel() {
        selectLevelPage.OnSelectItem(this);
    }
}
