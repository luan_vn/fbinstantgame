﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PopupBuyItem : MonoBehaviour {
	public BuyItemStartGameUI[] BuyItems;
	public Image Main;
	public Text Description;
	public Text MainName;
	[SerializeField] private ButtonAnimation _btnExit;

	void Start() {
		_btnExit.onClick.AddListener(OnBtnExitClick);
	}

	public void OnBtnExitClick() {
        //[Tracking - Position]-[ButtonPress]
      //  string location = UIManager.Instance.StartPage.CurrentNameMenu();
        string btnID = "btn_back";
       // GameTracking.Instance.TrackButtonPress(location, btnID);

        gameObject.SetActive(false);
		//UIManager.Instance.StartPage.ItemPage.gameObject.SetActive(true);

        //[Tracking - Position]-[MenuShowup]
      //  string locationMenu = UIManager.Instance.StartPage.CurrentNameMenu();
       // GameTracking.Instance.TrackMenuShowup(locationMenu);
    }

	public void LoadDataBuy(StartItemInfo data, int index, System.Action backAction) {
		Sprite icon;
		var isBomb = index == 3;
		bool isMissile = false;
		if (isBomb) {
			var card = ManagerData.Instance.GetInformationCard();
			isMissile = card != null && card.Name == CardType.Missiles;
			icon = isMissile ? card.IconInGame : data.Icon;
		} else {
			icon = data.Icon;
		}

		MainName.text = !isMissile ? data.Name : data.Name.Replace("Bomb", "Missile").Replace("bomb", "missile");
		Main.sprite = icon;
		Main.SetNativeSize();
		Main.transform.localScale = Vector2.one * 1.4f;
		Description.text = data.Description;

		for (int i = 0; i < BuyItems.Length; i++)
			if (i < data.Cost.Count) {
				BuyItems[i].SetData(icon, data.Cost[i], index, MainName.text, backAction, isMissile);
				BuyItems[i].gameObject.SetActive(true);
			} else
				BuyItems[i].gameObject.SetActive(false);
	}
}
