﻿using System;
using System.Collections.Generic;
using System.Linq;
using DG.Tweening;
using Gemmob;
using Spine.Unity;
using UnityEngine;
using UnityEngine.UI;

public class SelectLevelPage : Frame {
    public List<LevelItem> Levels = null;
    [Header("Make map")]
    [SerializeField] private RectTransform _group;
    [SerializeField] private GameObject _levelItemPrefab;
    [SerializeField] private GameObject _dotPrefab;
    [SerializeField] private DOTweenPath _levelPath;
    [SerializeField] private Transform _scroll;
    [Header("SELECT")]
    [SerializeField]  private RectTransform _selectRect;
    [SerializeField] private PrebatlePanel prebatlePanel = null;
    [SerializeField]
    private Sprite _normalLevelSprite, _bossLevelSprite;
    [Header("Modes Play")]
    [SerializeField] private ModePlayUI modePlayUI;

    [SerializeField] private ButtonBase btnBack = null;
    protected override void OnInitialize(HUD hud) {
        base.OnInitialize(hud);
        prebatlePanel.Initialize();
        _scroll.eulerAngles = new Vector3(20, 0, 0);
        foreach(var item in Levels) {
            item.Initialize(this);
        }
        prebatlePanel.gameObject.SetActive(false);
        btnBack.onClick.AddListener(ButtonBack);
    }

    protected override void OnShow(Action onCompleted = null, bool instant = false) {
        base.OnShow(onCompleted, instant);
        UIHomeManager.Instance.SetActiveBot(false);
    }

    protected override void OnHide(Action onCompleted = null, bool instant = false) {
        base.OnHide(onCompleted, instant);
        UIHomeManager.Instance.SetActiveBot(true);
    }

    public void OnSelectItem(LevelItem levelItem) {
        var curLevel = ManagerData.Instance.PlayerInfo.LevelUnlock;
        if (levelItem.Level <= curLevel) {
            ManagerGame.Instance.levelSelected = levelItem.Level;
            prebatlePanel.ShowPrebatle();
        }
    }

    private void ButtonBack() {
        SFXHome.Instance.PlaySound(SFXHome.Audio.Click_Button_02);
        Hide();
    }

#if UNITY_EDITOR
    [ContextMenu("InitLevelItems")]
    private void InitLevelItems() {
        Levels.Clear();
        Transform container = _levelItemPrefab.transform.parent;
        // Delete level item
        int count = container.childCount - 1;
        while(container.childCount > 1 && count > 0) {
            DestroyImmediate(container.GetChild(0).gameObject);
            count--;
        }
        Levels.Add(_levelItemPrefab.GetComponent<LevelItem>());

        int numLevelOfGame = FindObjectOfType<GameConfig>().NumLevelOfGame;

        for(int i = 1; i < numLevelOfGame; i++)
            Levels.Add(Instantiate(_levelItemPrefab, container, true).GetComponent<LevelItem>());
        if(_levelPath.wps.Count > 0)
            _levelPath.transform.position = _levelPath.wps[0];

        if(_levelPath.wps.Count == numLevelOfGame) {
            // BossAnimationLoader.Instance.Preload();
            for(int i = 0; i < numLevelOfGame; i++) {
                Vector3 pos = _levelPath.wps[i];
                Levels[i].Level = i;
                Levels[i].transform.position = new Vector3(pos.x, pos.y, transform.position.z);

                Levels[i].txtLevel.text = (i + 1).ToString();
                Levels[i].name = "Level " + (i + 1);
                if((i + 1) % 6 == 0) {
                    //var skeletonAnimation = BossAnimationLoader.Instance.LoadAnimationBossByIndexEditor((i + 1) / 6 - 1);
                    //if (skeletonAnimation != null)
                    //{
                    //    Debug.Log("We have boss");
                    //    var boss = Instantiate(skeletonAnimation, Levels[i].transform);
                    //    boss.transform.localPosition = new Vector2(0, -25f);

                    //    Levels[i].Boss = boss;
                    //}
                }
                else {
                    Levels[i].GetComponent<Image>().sprite = _normalLevelSprite;
                    Levels[i].GetComponent<Image>().SetNativeSize();
                }

                Levels[i].Skeleton = Levels[i].transform.GetComponentsInChildren<SkeletonGraphic>();
            }
        }
        else {
            Tween pathTween = _levelPath.transform.DOPath(_levelPath.wps.ToArray(), .01f, PathType.Linear);
            pathTween.ForceInit();
            for(int i = 0; i < numLevelOfGame; i++) {
                Vector3 pos = pathTween.PathGetPoint(i / (float)numLevelOfGame);
                Levels[i].txtLevel.text = (i + 1).ToString();
                Levels[i].transform.position = new Vector3(pos.x, pos.y, transform.position.z);
            }
        }

        for(int i = Levels.Count - 1; 0 <= i; i--) {
            Levels[i].transform.SetAsLastSibling();
        }

        CreateDots();

        // Set scroll region
        _group.sizeDelta = new Vector2(_group.rect.width, Levels[Levels.Count - 1].GetComponent<RectTransform>().anchoredPosition.y + 1500);
    }
#endif


    private void CreateDots() {
        Transform dotContainer = _dotPrefab.transform.parent;
        // Delete dots
        var count = dotContainer.childCount;
        while(dotContainer.childCount > 1 && count > 0) {
            DestroyImmediate(dotContainer.GetChild(1).gameObject);
            count--;
        }

        foreach(var levelItemUi in Levels)
            levelItemUi.Dots.Clear();

        if(_levelPath.wps.Count > 0)
            _levelPath.transform.position = _levelPath.wps[0];


        var pathTween = _levelPath.transform.DOPath(_levelPath.wps.ToArray(), 1f, PathType.Linear);
        pathTween.ForceInit();
        var number = pathTween.PathLength() * 1.8f;
        List<GameObject> l = new List<GameObject>() { _dotPrefab };
        for(int i = 1; i < number; i++) {
            GameObject obj = Instantiate(_dotPrefab);
            obj.transform.SetParent(dotContainer);
            obj.transform.localScale = Vector3.one;
            l.Add(obj);
        }
        int count2 = 1;
        List<float> list = new List<float>();
        for(int i = 0; i < number; i++) {
            list.Clear();
            for(int j = 0; j < count2; j++)
                list.Add(_levelPath.path.wpLengths[j]);


            if((list.Sum() - _levelPath.path.wpLengths[1]) / pathTween.PathLength() < i / (float)number) {
                count2++;
            }
            Vector3 pos = pathTween.PathGetPoint(i / (float)number);
            l[i].transform.position = new Vector3(pos.x, pos.y, transform.position.z);
            if(count2 - 3 >= 0)
                Levels[count2 - 3].Dots.Add(l[i].GetComponent<Image>());
        }


        float radius = .4f;
        for(var index = 0; index < Levels.Count; index++) {

            var levelItemUi = Levels[index];
            var lst = levelItemUi.Dots.FindAll(s =>
                Vector2.Distance(levelItemUi.transform.position, s.transform.position) <= radius);
            if(lst.Count > 0) {
                foreach(var image in lst) {
                    levelItemUi.Dots.Remove(image);
                    DestroyImmediate(image.gameObject);
                }
            }


            if(index > 0) {
                var lst2 = Levels[index - 1].Dots.FindAll(s =>
                      Vector2.Distance(levelItemUi.transform.position, s.transform.position) <= radius);
                if(lst2.Count > 0) {
                    foreach(var image in lst2) {
                        Levels[index - 1].Dots.Remove(image);
                        DestroyImmediate(image.gameObject);
                    }
                }

            }
        }
    }
}
