﻿using System;
using System.Collections;
using System.Collections.Generic;
using DG.Tweening;
using Gemmob;
using UnityEngine;
using UnityEngine.UI;

public class LoadingPage : Frame {
    public static LoadingPage Instance;

    public bool IsLoading;
    public Animator Anim;
    private Action _callBack;

    [SerializeField] private Image _bg;
    [SerializeField] private Text _loading;

    public void Loading(Action callBack) {
        if(IsLoading) { return; }
        _callBack = callBack;
        IsLoading = true;
        gameObject.SetActive(true);
        Anim.Play("Show", 0, 0);
    }

    public void OnShowLoadingCompleted() {
        // Loading data    
        DOVirtual.DelayedCall(.3f, () => {
            _callBack();
            Anim.Play("Hide", 0, 0);
        }).SetUpdate(true);
    }

    public void OnLoading() {
        // Loading data    
        if(IsLoading) { return; }
        IsLoading = true;
        gameObject.SetActive(true);
        DOVirtual.Float(0f, 1f, .5f, UpdatePage);
    }

    private void UpdatePage(float value) {
        Color newColor = Color.white;
        newColor.a = value;
        _bg.color = newColor;
        _loading.color = newColor;
    }

}
