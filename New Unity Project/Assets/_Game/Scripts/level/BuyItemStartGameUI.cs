﻿using System;
using System.Collections;
using System.Collections.Generic;
using Gemmob.Common;
using UnityEngine;
using UnityEngine.UI;

public class BuyItemStartGameUI : MonoBehaviour {
	public Image Icon;
	public ButtonAnimation ButtonBuy;
	public Text Description;
	public Text SubDescription;
	public Text NumberPlus;
	public Text Cost;

	public void SetData(Sprite icon, ItemPackInfo info, int indexItem, string itemName, Action backToMain, bool isMissile = false) {
		Icon.sprite = icon;
		Icon.SetNativeSize();
		Icon.transform.localScale = Vector2.one * 1.5f;

		Description.text = !isMissile ? info.Description : info.Description.Replace("Bomb", "Missile").Replace("bomb", "missile");
		SubDescription.text = !isMissile ? info.SubDescription : info.SubDescription.Replace("Bomb", "Missile").Replace("bomb", "missile");

		NumberPlus.text = "x" + info.NumberItem;
		Cost.text = info.Cost.ToString();

		ButtonBuy.onClick.RemoveAllListeners();
		ButtonBuy.onClick.AddListener(() => {
            //[Tracking - Position]-[ButtonPress]
           // string location = UIManager.Instance.StartPage.CurrentNameMenu();
            string btnID = "btn_buyGold";
            //GameTracking.Instance.TrackButtonPress(location, btnID);

            //TODO: remove player coin, add item to player
          //  bool isEnough = GameData.Instance.PlayerInfo.UseGold(info.Cost, "startItem", location);

			//if (isEnough) {
				//Events.BuyItem(itemName, Events.Category.Item, info.NumberItem);
				//Events.CurrencyFlow(itemName, -info.Cost);

			//	GameData.Instance.PlayerInfo.AddItem(indexItem, info.NumberItem, location);
				//UIManager.Instance.Announce.StartAnnounce("BUY SUCCESS", string.Format("Buy success {0} item {1} \ncost {2}",
				//	info.NumberItem, itemName, info.Cost), icon);
				//UIManager.Instance.Announce.StartAnnounce("BUY SUCCESS", "Buy success", icon);

				backToMain();
			//} else {
				//UIManager.Instance.Announce.StartAnnounce("FAIL", string.Format("Not enough money ({0}) to buy {2} {1}",
				//	info.Cost, itemName, info.NumberItem), icon);
			//	UIManager.Instance.Announce.StartAnnounce("Not enough money", TypeReward.Coin, false);

                //[Tracking - Position]-[MenuShowup]
               // GameTracking.Instance.TrackMenuShowup(UIManager.Instance.Announce.CurrentNameMenu());
           // }

		});
	}
}
