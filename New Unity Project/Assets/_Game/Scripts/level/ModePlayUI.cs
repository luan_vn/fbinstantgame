﻿using Gemmob;
using System.Collections.Generic;
using UnityEngine;
using Polaris.Tutorial;

public class ModePlayUI : MonoBehaviour {
    [Header("Halloween")]
    [SerializeField] private ModePlayButton _btnHalloween;

    [Header("BossBattle")]
    [SerializeField] private ModePlayButton _btnBossBattle;

    [Header("Endless")]
    [SerializeField] private ModePlayButton _btnEndless;

    [Header("Trial")]
    [SerializeField] private ModePlayButton _btnDailyChallenge;

    [SerializeField] private ButtonAnimation _btnTap;
    private List<ModePlayButton> modeButtons;
    private void Awake() {
        modeButtons = new List<ModePlayButton>();
        modeButtons.Add(_btnHalloween);
        modeButtons.Add(_btnBossBattle);
        modeButtons.Add(_btnEndless);
        modeButtons.Add(_btnDailyChallenge);
        _btnBossBattle.SetListenerAction(ButtonBossBattleClicked);
        _btnDailyChallenge.SetListenerAction(ButtonDailyChallengeClicked);
        _btnEndless.SetListenerAction(ButtonEndlessClicked);
        _btnHalloween.SetListenerAction(ButtonHalloweenClicked);
        _btnTap.onClick.AddListener(TapToHideAll);

    }
    private void OnEnable() {
        var currentLevel = ManagerData.Instance.PlayerInfo.LevelUnlock;
        //UnlockModes(currentLevel);
    }



    public void UnlockModes(int currentPlayerLevel) {
        UnlockBossBattleMode(currentPlayerLevel);
        UnlockHalloweenMode(currentPlayerLevel);
        UnlockDailyMode(currentPlayerLevel);
        UnlockEndlessMode(currentPlayerLevel);
    }

    private bool UnlockEndlessMode(int currentPlayerLevel) {
        bool isUnlockEndleess = ManagerData.Instance.PlayerInfo.UnlockModeData.isUnlockEndlessMode;

        if(isUnlockEndleess) {
            _btnEndless.SetIcon(ManagerData.Instance.UnlockLevelModeData.UnlockLevelEndlessMode.Icon);
            // IngameNotifiController.Instance.SetActiveNotifi(NotifiKey.EndlessMode, !ManagerData.Instance.PlayerInfo.UnlockModeData.hasPlayEndlessMode);
            //  bool canShowTutorial = TutorialController.Instance.ShowTutorial(TutorialKey.UnlockEndless);
            //if(UIManager.Instance.ratingPopup.gameObject.activeInHierarchy && canShowTutorial) {
            //    UIManager.Instance.ratingPopup.HidePageNonAction();
            //}
        }
        else {
            _btnEndless.SetInfo(ManagerData.Instance.UnlockLevelModeData.UnlockLevelEndlessMode.LevelUnlock);
            _btnEndless.SetIconInfor(ManagerData.Instance.UnlockLevelModeData.UnlockLevelEndlessMode.Icon);

        }
        // _btaHalloween.gameObject.SetActive(isUnlockEndleess);
        return isUnlockEndleess;
    }

    private bool UnlockHalloweenMode(int currentPlayerLevel) {
        bool isUnlockHalloween = ManagerData.Instance.PlayerInfo.UnlockModeData.isUnlockHalloweenMode;

        if(isUnlockHalloween) {
            _btnHalloween.SetIcon(ManagerData.Instance.UnlockLevelModeData.UnlockLevelHalloweenMode.Icon);
            //  IngameNotifiController.Instance.SetActiveNotifi(NotifiKey.HalloweenMode, !GameData.Instance.PlayerInfo.UnlockModeData.hasPlayHalloweenMode);
            // bool canShowTutorial = TutorialController.Instance.ShowTutorial(TutorialKey.UnlockSpooky);
            //if (UIManager.Instance.ratingPopup.gameObject.activeInHierarchy && canShowTutorial) {
            //    UIManager.Instance.ratingPopup.HidePageNonAction();
            //}
        }
        else {
            _btnHalloween.SetInfo(ManagerData.Instance.UnlockLevelModeData.UnlockLevelHalloweenMode.LevelUnlock);
            _btnHalloween.SetIconInfor(ManagerData.Instance.UnlockLevelModeData.UnlockLevelHalloweenMode.Icon);
        }
        return isUnlockHalloween;
    }

    private bool UnlockBossBattleMode(int currentPlayerLevel) {
        var isUnlockBattle = ManagerData.Instance.PlayerInfo.UnlockModeData.isUnlockBossBattleMode;
        if(isUnlockBattle) {
            _btnBossBattle.SetIcon(ManagerData.Instance.UnlockLevelModeData.UnlockLevelBossBattleMode.Icon);
            //   IngameNotifiController.Instance.SetActiveNotifi(NotifiKey.BossMode, !GameData.Instance.PlayerInfo.UnlockModeData.hasPlayBossBattleMode);
            //bool canShowTutorial = TutorialController.Instance.ShowTutorial(TutorialKey.UnlockBossBattle);
            //if (UIManager.Instance.ratingPopup.gameObject.activeInHierarchy && canShowTutorial) {
            //    UIManager.Instance.ratingPopup.HidePageNonAction();
            //}
        }
        else {
            _btnBossBattle.SetInfo(ManagerData.Instance.UnlockLevelModeData.UnlockLevelBossBattleMode.LevelUnlock);
            _btnBossBattle.SetIconInfor(ManagerData.Instance.UnlockLevelModeData.UnlockLevelBossBattleMode.Icon);
        }
        return isUnlockBattle;
    }

    private bool UnlockDailyMode(int currentPlayerLevel) {
        var isUnlockBattle = ManagerData.Instance.PlayerInfo.UnlockModeData.isUnlocDailyMode;
        if(isUnlockBattle) {
            _btnDailyChallenge.SetIcon(ManagerData.Instance.UnlockLevelModeData.UnlockLevelTrialMode.Icon);
            // IngameNotifiController.Instance.SetActiveNotifi(NotifiKey.DailyMode, !ManagerData.Instance.PlayerInfo.UnlockModeData.hasPlayDailyMode);
            // bool canShowTutorial = TutorialController.Instance.ShowTutorial(TutorialKey.UnlockTrails);
            //if (UIManager.Instance.ratingPopup.gameObject.activeInHierarchy && canShowTutorial) {
            //    UIManager.Instance.ratingPopup.HidePageNonAction();
            //}
        }
        else {
            _btnDailyChallenge.SetInfo(ManagerData.Instance.UnlockLevelModeData.UnlockLevelTrialMode.LevelUnlock);
            _btnDailyChallenge.SetIconInfor(ManagerData.Instance.UnlockLevelModeData.UnlockLevelTrialMode.Icon);
        }
        return isUnlockBattle;
    }




    private void ButtonEndlessClicked() {
        bool isUnlockEndleess = ManagerData.Instance.PlayerInfo.UnlockModeData.isUnlockEndlessMode;
        if(isUnlockEndleess) {
            ManagerData.Instance.PlayerInfo.UnlockModeData.hasPlayEndlessMode = true;
            ManagerData.Instance.PlayerInfo.Save();
            SoundController.Instance.RunClickSound();
            GamePlayState.Instance.ModePlay = EnumDefine.ModePlay.Endless;

        }
        else {
            bool isOpen = _btnEndless.PlayAnimClick();
            if(isOpen) {
                _btnTap.gameObject.SetActive(true);

            }
        }
        HideAllButton(_btnEndless);

    }

    private void ButtonDailyChallengeClicked() {
        bool isUnlockDaily = ManagerData.Instance.PlayerInfo.UnlockModeData.isUnlocDailyMode;
        if(isUnlockDaily) {
            ManagerData.Instance.PlayerInfo.UnlockModeData.hasPlayDailyMode = true;
            ManagerData.Instance.PlayerInfo.Save();
            SoundController.Instance.RunClickSound();
            GamePlayState.Instance.ModePlay = EnumDefine.ModePlay.Normal;
        }
        else {
            bool isOpen = _btnDailyChallenge.PlayAnimClick();
            if(isOpen) {
                _btnTap.gameObject.SetActive(true);
            }
        }
        HideAllButton(_btnDailyChallenge);

    }

    private void ButtonBossBattleClicked() {

        bool isUnlockBossBattle = ManagerData.Instance.PlayerInfo.UnlockModeData.isUnlockBossBattleMode;
        if(isUnlockBossBattle) {
            //if (Config.IsDebug) {
            //    GameData.Instance.PlayerInfo.AddFuel(20);
            ManagerData.Instance.PlayerInfo.UnlockModeData.hasPlayBossBattleMode = true;
            ManagerData.Instance.PlayerInfo.Save();
            SoundController.Instance.RunClickSound();
            //ShowBossBattleAnnounce();
            //UIManager.Instance.selectLevel.HideToNextToNewPage();
            //UIManager.Instance.BossBattlePage.ShowPage();

            ////[Tracking - Position]-[MenuShowup]
            //string locationMenu = UIManager.Instance.BossBattlePage.CurrentNameMenu();
            //GameTracking.Instance.TrackMenuShowup(locationMenu);
            //}
        }
        else {
            bool isOpen = _btnBossBattle.PlayAnimClick();
            if(isOpen) {
                _btnTap.gameObject.SetActive(true);

            }
        }
        HideAllButton(_btnBossBattle);

    }

    private void ButtonHalloweenClicked() {
        bool isUnlock = ManagerData.Instance.PlayerInfo.UnlockModeData.isUnlockHalloweenMode;
        if(isUnlock) {
            ManagerData.Instance.PlayerInfo.UnlockModeData.hasPlayHalloweenMode = true;
            ManagerData.Instance.PlayerInfo.Save();

        }
        else {
            bool isOpen = _btnHalloween.PlayAnimClick();
            if(isOpen) {
                _btnTap.gameObject.SetActive(true);

            }
        }
        HideAllButton(_btnHalloween);

    }

    private void HideAllButton(ModePlayButton exceptButton = null) {
        foreach(var item in modeButtons) {
            if(exceptButton == null || !item.Equals(exceptButton)) {
                item.HideButton();
            }
        }
    }

    public void TapToHideAll() {
        HideAllButton();
        _btnTap.gameObject.SetActive(false);
    }
}
