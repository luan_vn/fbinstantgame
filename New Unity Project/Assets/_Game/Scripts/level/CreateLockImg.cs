﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CreateLockImg : MonoBehaviour {
	public GameObject LockItem;
	private LevelItem currentLevelItemUi;

	public LevelItem CurrentLevel {
		get { return currentLevelItemUi; }
		set {
			currentLevelItemUi = value;
		}
	}

	[ContextMenu("Add lock")]
	public void CreateLock() {
		LevelItem ui;
		for (int i = 0; i < transform.childCount; i++) {
			ui = transform.GetChild(i).GetComponent<LevelItem>();
			if (ui != null) {
				if (ui.goLock) {

				} else {
					ui.goLock = Instantiate(LockItem, transform.GetChild(i));
					ui.goLock.transform.localPosition = Vector2.zero;
				}
			}
		}
	}
}
