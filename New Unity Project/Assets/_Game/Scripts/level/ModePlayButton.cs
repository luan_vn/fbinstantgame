﻿using UnityEngine;
using UnityEngine.UI;
using DG.Tweening;
public class ModePlayButton : MonoBehaviour {
    [SerializeField] private Image _imgIcon;
    [SerializeField] private Image _imgIconInfor;
    [SerializeField] private Text txtDescription;
    [SerializeField] private RectTransform _body;
    [SerializeField] private RectTransform _rectButton;
    private ButtonAnimation _button;
    private bool isOpen;
    //  private const float hideWidth = 163;
    private const float showWidth = 900;
    private float hideWidth = 163;
    private const float timeShow = 0.5f;


    //private const string ShowAnimClip = "ModePlayButtonShow";
    //private const string HideAnimClip = "ModePlayButtonClose";


    private void Awake() {
        _button = _button ?? GetComponentInChildren<ButtonAnimation>();
        hideWidth = GetComponent<RectTransform>().sizeDelta.x;
    }
    public bool PlayAnimClick() {
        if(isOpen) {
            HideButton();
        }
        else {
            ShowButton();
        }
        return isOpen;
    }
    public void ShowButton() {
        if(!isOpen) {
            isOpen = !isOpen;
            RectTransform rect = gameObject.GetComponent<RectTransform>();
            rect.DOKill();
            rect.DOSizeDelta(new Vector2(showWidth / 2, rect.rect.height), timeShow / 2).SetEase(Ease.Linear).OnComplete(() => {
                ShowInfo();
                rect.DOSizeDelta(new Vector2(showWidth, rect.rect.height), timeShow / 2).SetEase(Ease.Linear).OnComplete(() => {

                });
            });
            //animator.SetBool("IsOpen", true);
        }
    }
    public void HideButton() {
        if(isOpen) {
            isOpen = !isOpen;
            RectTransform rect = gameObject.GetComponent<RectTransform>();
            rect.DOKill();
            rect.DOSizeDelta(new Vector2(showWidth / 2, rect.rect.height), timeShow / 2).SetEase(Ease.Linear).OnComplete(() => {
                HideInfo();
                rect.DOSizeDelta(new Vector2(hideWidth, rect.rect.height), timeShow / 2).SetEase(Ease.Linear).OnComplete(() => {

                });
            });
            //animator.SetBool("IsOpen", false);

        }
    }
    public void SetIcon(Sprite icon) {
        if(icon != null) {
            _imgIcon.sprite = icon;
            _imgIcon.SetNativeSize();
        }
    }
    public void SetInfo(int levelUnlock) {
        txtDescription.text = string.Format("Unlock At Level {0}", levelUnlock);
    }
    public void SetIconInfor(Sprite icon) {
        _imgIconInfor.sprite = icon;
    }

    public void ShowInfo() {
        _body.gameObject.SetActive(true);
    }
    public void HideInfo() {
        _body.gameObject.SetActive(false);
    }
    public void SetListenerAction(UnityEngine.Events.UnityAction action) {
        if(_button == null) {
            _button = _button ?? GetComponentInChildren<ButtonAnimation>();
        }
        if(_button != null)
            _button.onClick.AddListener(action);
    }
}
