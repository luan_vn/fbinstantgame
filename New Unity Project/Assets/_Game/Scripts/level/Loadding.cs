﻿using System;
using System.Collections;
using DG.Tweening;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class Loadding : MonoBehaviour {
    private bool isLoading;
    private Image bg;
    private Text loading;


    private void Awake() {
        Hide();
        bg = gameObject.transform.GetComponentInChildren<Image>();
        loading = gameObject.transform.GetComponentInChildren<Text>();
    }

    public void Hide() {
        gameObject.SetActive(false);
        isLoading = false;
    }

    public void Show() {
        isLoading = true;
        gameObject.SetActive(true);
    }


    private void LoadingTextAnim() {
        Color c;
        float alphaTo = loading.color.a > .5f?0f:1f;
        DOTween.To(x => {
            c = loading.color;
            c.a = x;
            loading.color = c;
        }, loading.color.a, alphaTo, .5f).OnComplete(LoadingTextAnim);
    }


    public void LoadScene(string sceneName, Action onComplete = null) {
        if (isLoading)
            return;
        Show();
        Color c;
        DOTween.To(x => {
            c = bg.color;
            c.a = x;
            bg.color = c;
            loading.color = c;
        }, 0, 1f, .5f).OnComplete(() => {
            LoadingTextAnim();
            StartCoroutine(LoadAsynchronously(sceneName, onComplete));
        });
    }

	public void LoadSceneNoBg(string sceneName, Action onComplete = null) {
		StartCoroutine(LoadAsynchronouslyNoBg(sceneName, onComplete));
	}

	IEnumerator LoadAsynchronouslyNoBg(string sceneIndex, Action onComplete = null) {
		AsyncOperation operation = SceneManager.LoadSceneAsync(sceneIndex);
		var timer = 0f;
		while (!operation.isDone && timer < 1.5f) {
			timer += Time.deltaTime;
			yield return null;
		}

		if (onComplete != null) {
			onComplete.Invoke();
		}

		timer = 0f;
		while (timer < .5f) {
			timer += Time.deltaTime;
			yield return null;
		}
	}

	IEnumerator LoadAsynchronously(string sceneIndex, Action onComplete = null) {
        AsyncOperation operation = SceneManager.LoadSceneAsync(sceneIndex);
        var timer = 0f;
        while (!operation.isDone && timer < 1.5f) {
            timer += Time.deltaTime;
            yield return null;
        }

        if (onComplete != null) {
            onComplete.Invoke();
        }

        timer = 0f;
        while (timer < .5f) {
            timer += Time.deltaTime;
            yield return null;
        }

        DOTween.To(x => {
            var bgColor = bg.color;
            bgColor.a = x;
            bg.color = bgColor;
            loading.color = bgColor;
        }, 1f, 0f, .5f).OnComplete(Hide);
    }
}