﻿using Gemmob;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using static EnumDefine;

public class SkillEquipmentPanel : EquipmentPanelBase {
    private SkillEquipmentItem itemSelected = null;
    private SkillPositionEquipmentItem skillPosSelected = null;
    private ShipEquipmentPanel shipEquipment = null;
    private List<SkillEquipmentItem> listItems = new List<SkillEquipmentItem>();
    [SerializeField] private RectTransform contents = null;
    [SerializeField] private SkillEquipmentItem itemPrefab = null;
    [Header("Skill")]
    [SerializeField] private List<SkillPositionEquipmentItem> skillsPosEquipment = null;

    public override EnumDefine.TypeItem typeItem => EnumDefine.TypeItem.Skill;
    public void Initialize(EquipmentPage equipmentPage, InfoEquipmentPanel infoEquipment, SelectedEquipment selectedEquipment, ShipEquipmentPanel shipEquipment) {
        this.equipmentPage = equipmentPage;
        this.infoEquipment = infoEquipment;
        this.selectedEquipment = selectedEquipment;
        this.shipEquipment = shipEquipment;
        foreach(var item in skillsPosEquipment) {
            item.Initialize(this);
        }
        var ships = SkillsData.Instance.Skills;
        LoadItems(ships);
    }


    public SkillData hardData {
        get {
            if(itemSelected == null) {
                Logs.Log("Not Select Item");
                return null;
            }
            return itemSelected.hardData;
        }
    }

    protected void LoadItems(List<SkillData> dataShip) {
        foreach(var item in dataShip) {
            var itemCreate = itemPrefab.Spawn(contents);
            itemCreate.Initialize(this, item);
            listItems.Add(itemCreate);
        }
    }

    public void Filter(EnumDefine.TypeSkill type, EnumDefine.IDSkill idSkill = EnumDefine.IDSkill.Shield) {
        foreach(var item in listItems) {
            if(item.hardData.TYPE == type) {
                if(!item.gameObject.activeSelf) {
                    item.gameObject.SetActive(true);
                }
            }
            else {
                if(item.gameObject.activeSelf) {
                    item.gameObject.SetActive(false);
                }
            }
        }
        if(type != EnumDefine.TypeSkill.None) {
            OnSelectItem(idSkill); 
        }
        else {
            if(skillPosSelected != null) {
                skillPosSelected.OnUnSelect();
                skillPosSelected = null;
            }
        }
    }

    public void OnSelectItem(EnumDefine.IDSkill idSkill) {
        var item = listItems.Find(x => x.hardData.ID == idSkill);
        OnSelectItem(item);
    }

    public void OnSelectItem(SkillEquipmentItem itemSelected) {
        if(this.itemSelected != null && itemSelected.hardData.ID == this.itemSelected.hardData.ID) {

        }
        else {
            itemSelected.OnSelect();
            if(this.itemSelected != null) { this.itemSelected.OnUnSelect(); }
            this.itemSelected = itemSelected;
        }
        UpdateUI();
    }

    public void UpdateUI() {
        skillPosSelected.UpdateUI(hardData.ID);
        var position = itemSelected.transform.position + Vector3.down * 0.5f;
        var softData = ManagerData.Instance.skillsInfo.GetSkillInfo(hardData.ID);
        if(softData.IsUnlock()) {
            infoEquipment.SetButtonEvovle("MAX", false, null);
            var curLevel = hardData.GetLevelData(softData.Level);
            if(softData.Level + 1 == hardData.MaxLevel) {
                var strPower = string.Format("<color=\"white\">POWER </color><color=\"blue\"> {0}</color>", curLevel.CombatPower);
                infoEquipment.SetInfo(softData.Level, strPower);
                infoEquipment.SetButtonUpgrade("MAX", false, null);
            }
            else {
                var nextLevel = hardData.GetLevelData(softData.Level + 1);
                var increaseCp = nextLevel.CombatPower - curLevel.CombatPower;
                var strPower = string.Format("<color=\"white\">POWER </color><color=\"yellow\"> {0}</color><color=\"green\"> +{1}</color>", curLevel.CombatPower, increaseCp);
                var strRequireUpgrade = string.Format("<sprite=\"coin_icon\" name=\"coin_icon\"> {0}", nextLevel.Coin);
                int coinCurrency = ManagerData.Instance.PlayerInfo.GetCurrency(IDCurrency.Coin);
                infoEquipment.SetInfo(softData.Level, strPower);
                infoEquipment.SetInfo(softData.Level, strPower).SetButtonUpgrade(strRequireUpgrade, true, OnUpgrade);

            }
            var shipEquiped = ManagerData.Instance.PlayerInfo.IsSkillEquiped(hardData.ID, out var shipEquipedInfo);
            if(hardData.TYPE == EnumDefine.TypeSkill.Active) {
                if(shipEquiped) {
                    selectedEquipment.SetButtonInfo("INFO", OnInformation).SetButtonPick("UNPICK", OnUnPick, false).SetActive(true, position);
                }
                else {
                    selectedEquipment.SetButtonInfo("INFO", OnInformation).SetButtonPick("PICK", OnPick, false).SetActive(true, position);
                }
            }
            else {
                if(shipEquiped) {
                    selectedEquipment.SetButtonInfo("INFO", OnInformation).SetButtonPick("EQUIPED", OnEquiped, false).SetActive(true, position);
                }
                else {
                    selectedEquipment.SetButtonInfo("INFO", OnInformation).SetButtonPick("PICK", OnPick, false).SetActive(true, position);
                }
            }
        }
        else {
            var curLevel = hardData.GetLevelData(0);
            var strPower = string.Format("<color=\"white\">POWER </color><color=\"blue\"> {0}</color>", curLevel.CombatPower);
            infoEquipment.SetInfo(softData.Level, strPower).SetButtonUpgrade("BUY", false, null).SetButtonEvovle("BUY", false, null);
            switch(hardData.TYPE) {
                case TypeSkill.Talen:
                    selectedEquipment.SetButtonInfo("INFO", OnInformation).SetButtonPick(string.Empty, null, false).SetActive(true, position);
                    break;
                case TypeSkill.Active:
                    selectedEquipment.SetButtonInfo("INFO", OnInformation).SetButtonPick("BUY", OnBuy).SetActive(true, position);
                    break;
            }
        }
    }

    private void OnInformation() {
        UIHomeManager.Instance.Show<InformationPage>();
    }

    private void OnPick() {
        selectedEquipment.Active(false);
        ManagerData.Instance.PlayerInfo.SetSkillEquiped(skillPosSelected.Direct, true, hardData.ID);


    }

    private void OnUnPick() {
        selectedEquipment.Active(false);
        ManagerData.Instance.PlayerInfo.SetSkillEquiped(skillPosSelected.Direct, false, hardData.ID);

    }

    private void OnEquiped() {

    }

    private void OnBuy() {
        selectedEquipment.Active(false);
        var skillValue = -hardData.GetLevelData(0).Coin;
        ManagerData.Instance.PlayerInfo.AddCurrency(IDCurrency.Coin, skillValue);
        ManagerData.Instance.skillsInfo.BuySkill(hardData.ID);
        UpdateUI();
    }

    public void EquipedShip(bool equiped) {


    }

    private void OnUpgrade() {
        var softData = ManagerData.Instance.skillsInfo.GetSkillInfo(hardData.ID);
        if(softData.IsUnlock()) {
            var curLevel = hardData.GetLevelData(softData.Level);
            if(softData.Level + 1 < hardData.MaxLevel) {
                var nextLevel = hardData.GetLevelData(softData.Level + 1);
                int coinCurrency = ManagerData.Instance.PlayerInfo.GetCurrency(IDCurrency.Coin);
                if(coinCurrency >= nextLevel.Coin) {
                    ManagerData.Instance.PlayerInfo.AddCurrency(IDCurrency.Coin, -nextLevel.Coin);
                    ManagerData.Instance.skillsInfo.UpgradeSkill(hardData.ID);
                    UpdateUI();
                }
                else {
                    Logs.Log(string.Format("You Not Enough {0}", IDCurrency.Coin));
                }
            }
            else {
                Logs.Log("Skill Max");
            }
        }
        else {
            Logs.Log("You Need Unlock Ship");
        }
    }

    private void OnEvolve() {
        var softData = ManagerData.Instance.skillsInfo.GetSkillInfo(hardData.ID);
        if(softData.IsUnlock()) {
            
        }
        else {
            Logs.Log("You Need Unlock Ship");
        }
    }

    public void RegisterEquipShip(Direct direct) {
        var item = skillsPosEquipment.Find(x => x.Direct == direct);
        RegisterEquipSkill(item);
    }

    public void RegisterEquipSkill(SkillPositionEquipmentItem skillPosSelected) {
        selectedEquipment.Active(false);
        if(this.skillPosSelected == null) {
            skillPosSelected?.OnSelect();
            this.skillPosSelected = skillPosSelected;
            ShowSkills();
        }
        else {
            if(this.skillPosSelected.Direct == skillPosSelected.Direct) {
                Logs.Log(string.Format("Direct {0} Selected", skillPosSelected.Direct));
            }
            else {
                skillPosSelected?.OnSelect();
                this.skillPosSelected?.OnUnSelect();
                this.skillPosSelected = skillPosSelected;
                ShowSkills();
            }
        }
    }

    private void ShowSkills() {
        var type = skillPosSelected.Direct == EnumDefine.Direct.Left ? EnumDefine.TypeSkill.Talen : EnumDefine.TypeSkill.Active;
        shipEquipment.Filter(TypeShip.None);
        Filter(type, skillPosSelected.IDSkill);
    }
}
