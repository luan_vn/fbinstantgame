﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class ShipDebrisItem : MonoBehaviour {
    [SerializeField] private EnumDefine.Direct direct = EnumDefine.Direct.Left;
    [SerializeField] protected Image imgIcon = null;
    [SerializeField] protected Image imgRank = null;
    [SerializeField] protected TextMeshProUGUI txtAmount = null;
    [SerializeField] protected ButtonBase btnSelect = null;

    public EnumDefine.Direct Direct => direct;

    public void ChangeShip(EnumDefine.IDShip id) {
        var hardData = ShipsData.Instance.GetUpgradeShip(id);
        var softData = ManagerData.Instance.AircraftData.GetDataShipInfo(id);
        var isEquiped = ManagerData.Instance.PlayerInfo.IsShipEquiped(id, out var shipEquipedInfo);
        SetRank(softData.Rank);
        SetAmount(softData.Debris);
        SetIcon(hardData.GetIcon(softData.Rank));
    }

    public void SetIcon(Sprite sprIcon) {
        if(imgIcon == null) {
            Logs.Log("Image Icon Null");
            return;
        }
        if(sprIcon == null) {
            Logs.Log("Sprite Icon Null");
            return;
        }
        imgIcon.sprite = sprIcon;
        var perfectSize = imgIcon.GetComponent<PerfectSize>();
        if(perfectSize) {
            var sizeFix = sprIcon.rect.size;
            perfectSize.AutoSize(sizeFix);
        }
    }

    public void SetRank(EnumDefine.Rank rank) {
        if(imgRank == null) {
            Logs.Log("Image Rank Null");
            return;
        }
        imgRank.sprite = SpritesData.Instance.GetBorderRank(rank);
    }

    public void SetAmount(int amount) {
        if (txtAmount == null) {
            Logs.Log("Text Amount Null");
            return;
        }
        txtAmount.text = amount.ToString();
    }
}
