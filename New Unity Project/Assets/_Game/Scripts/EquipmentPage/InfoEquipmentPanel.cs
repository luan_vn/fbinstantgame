﻿using System;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class InfoEquipmentPanel : MonoBehaviour {
    private Action onUpgrade = null;
    private Action onEvolve = null;
    [SerializeField] private Sprite sprActive = null;
    [SerializeField] private Sprite sprUnActive = null;
    [SerializeField] private Image imgButtonUpgrade = null;
    [SerializeField] private Image imgButtonEvolve = null;
    [SerializeField] private TextMeshProUGUI txtCombatPower = null;
    [SerializeField] private TextMeshProUGUI txtRequireUpgrade = null;
    [SerializeField] private TextMeshProUGUI txtRequireEvolve = null;
    [SerializeField] private ButtonBase btnUpgrade = null;
    [SerializeField] private ButtonBase btnEvolve = null;
    [SerializeField] private List<ActiveBase> levelsActive = null;

    public void Initialize() {
        btnUpgrade.AddListener(ButtonUpgrade);
        btnEvolve.AddListener(ButtonEvolve);
    }

    private void ButtonUpgrade() {
        onUpgrade?.Invoke();
    }

    private void ButtonEvolve() {
        onEvolve?.Invoke();
    }

    public InfoEquipmentPanel SetInfo(int level, string strPower) {
        txtCombatPower.text = strPower;
        level = level.Claim10();
        for(int i = 0; i < levelsActive.Count; i++) { levelsActive[i].Active(i <= level); }
        return this;
    }

    public InfoEquipmentPanel SetButtonUpgrade(string strRequire, bool activeSpr, Action callBack, bool active = true) {
        onUpgrade = callBack;
        txtRequireUpgrade.text = strRequire;
        imgButtonUpgrade.sprite = activeSpr ? sprActive : sprUnActive;
        return this;
    }

    public InfoEquipmentPanel SetButtonEvovle(string strRequire, bool activeSpr, Action callBack, bool active = true) {
        onEvolve = callBack;
        txtRequireEvolve.text = strRequire;
        imgButtonEvolve.sprite = activeSpr ? sprActive : sprUnActive;
        return this;
    }
}
