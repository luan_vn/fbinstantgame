﻿using Gemmob;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ActiveSkillPow : ActiveSkillComponentBase {
    [SerializeField] private int numberPow = 5;
    [SerializeField] private float timeDelay = 2f;
    [SerializeField] private PowBase powPrefab = null;
    public override EnumDefine.IDSkill idActiveSkill => EnumDefine.IDSkill.Pow;

    public override ActiveSkillComponentBase Initialize() {

        return this;
    }
    public override void StartSkill() {
        StartCoroutine(TimeCreatePow());
    }

    private IEnumerator TimeCreatePow() {
        for(int i = 0; i < numberPow; i++) {
            var powCreate = powPrefab.Spawn(transform.position);
            powCreate.Initialize(15f);
            yield return Yielder.Wait(timeDelay);
        }
    }

    public override void PauseSkill() {
        
    }
}
