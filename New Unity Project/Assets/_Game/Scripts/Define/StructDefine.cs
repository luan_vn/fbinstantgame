﻿using Gemmob;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StructDefine {
    public struct EnemyAttack : IEventParams {

    }

    public struct ShipPossition : IEventParams {
        public EnumDefine.View view;
        public Vector3 possition;
    }

    public struct CurrencyChange : IEventParams {
        public EnumDefine.IDCurrency idCurrency;
        public int amount;
    }

    public struct QuestUpdateValue : IEventParams {
        public QuestType type;
        public int amount;
    }

    public struct QuestUpdateUI : IEventParams {
        public QuestType type;
        public int amount;
    }
}
