﻿using Gemmob;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class MainCanvasScaler : SingletonBind<MainCanvasScaler> {
    [SerializeField] private CanvasScaler mainCanvasScaler = null;
    public CanvasScaler main => mainCanvasScaler;

    private void OnValidate() {
        mainCanvasScaler = transform.GetComponentInChildren<CanvasScaler>();
    }
}
