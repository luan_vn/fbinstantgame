﻿using DG.Tweening;
using Gemmob;
using Spine.Unity;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class ManagerEnemies : SingletonBind<ManagerEnemies> {
    private int currentWave = 0;
    private int countPathDone = 0;
    private int numberEnemiesDie = 0;
    private int totalEnemiesOfWave = 0;
    private const float MinTimeEnemyAttack = .15f;
    private LevelData curLevelData = null;
    private Coroutine increaseNumberEnemyAttack = null;
    private Coroutine enemiesFireBulletCoroutine = null;
    private Coroutine enemiesMoveAttackCoroutineWave = null;
    private List<Coroutine> enemiesMoveAttackCoroutine = null;

    private List<Transform> enemies = new List<Transform>();
    private List<Coroutine> waveCoroutines = new List<Coroutine>();
    private Dictionary<int, GameObject> enemiesPrefab = new Dictionary<int, GameObject>();
    private Dictionary<int, GameObject> bossesPrefab = new Dictionary<int, GameObject>();
    private Dictionary<int, GameObject> rocksPrefab = new Dictionary<int, GameObject>();
    private Dictionary<int, GameObject> giftsPrefab = new Dictionary<int, GameObject>();

    //Drop Iteam
    private int[] dropItems;
    //[SerializeField] private ScoreJump scoreJump;
 
    public void Init() {
        currentWave = 0;
         StartCoroutine(LoadNormal());
    }

    private IEnumerator LoadNormal() {
        var levelSelected = ManagerGame.Instance.levelSelected + 1;
        var levelPath = string.Format("Levels/Level {0:00}", levelSelected);
        curLevelData = Resources.Load<LevelData>(levelPath);

        if(curLevelData.Waves[currentWave].UseWarning) {
            foreach(var pos in curLevelData.Waves[currentWave].WarningPositions) {
                //EffectPool.Instance.ShowWarningWave(pos);
            }
        }

        yield return Yielder.Wait(0.8f);
        UIIngameManager.Instance.GetFrame<UIIngamePage>().ShowNotifyWave(curLevelData.Waves.Count, currentWave);
        yield return Yielder.Wait(1.5f);
        yield return LoadWave(/*hpScale*/1f, curLevelData.Waves[currentWave]);
    }

    public void EnemyDeath(Vector3 Pos) {
         numberEnemiesDie += 1;

        //Drop item
        //if(dropItems != null && dropItems.Length > 0) {
        //    var dropItemId = dropItems[numberEnemiesDie - 1];
        //    if(dropItemId != -1) { OnDropItem(dropItemId, Pos); }
        //}  
        //
        if(numberEnemiesDie == totalEnemiesOfWave) {
            StopWaveCorountine();
            EnemyMoveHandler.Instance.Stop();
            EventDispatcher.Instance.Dispatch<GameEvent.OnEndWave>();
            currentWave += 1;
            numberEnemiesDie = 0;
            totalEnemiesOfWave = 0;
            var timeNotify = UIIngameManager.Instance.GetFrame<UIIngamePage>().ShowNotifyEndWave();
            DOVirtual.DelayedCall(timeNotify, () => {
                if(currentWave >= curLevelData.Waves.Count) {
                    ManagerPlayer.Instance.OnFinish();
                }
                else {
                    StartCoroutine(LoadNormal());
                }
            });
        }
    }

    private IEnumerator LoadPath(float hpScale, PathInfor path, WaveData wave, Action pathDone) {
        int numEnemiesOfWave = path.Cells.Count;
        int countEnemiesOfWaveMoveFinished = 0;
        yield return Yielder.Wait(path.DelayTime);
        // Spawn enemies
        int count = 0;
        var offset = new Vector3(path.Offset.x, path.Offset.y, 0);

        foreach(var pathCell in path.Cells) {
            var eData = wave.Enemies[pathCell.Row].Rows[pathCell.Coll];

            if(eData.EnemyId == -1) { continue; }
            var enemyPrefab = LoadEnemyResource(eData.EnemyId);
            Transform transform = enemyPrefab.Spawn().transform;
            Transform eTrans = transform;
            eTrans.GetComponent<EnemyNormal>().SetItemBase(dropItems[enemies.Count]);
            eTrans.GetComponent<EnemyNormal>().Init(hpScale);

            // Set trang thái enemy nhận dam lúc xuất hiện

            //eTrans.GetComponent<Enemy>().NoDam = wave.NoDam;
            

            var movePath = new List<Vector3>(path.FlipX
                    ? PathManager.Instance.Paths[path.Index].FlipXPositions
                    : PathManager.Instance.Paths[path.Index].Positions);

            for(int i = 0; i < movePath.Count; i++) { movePath[i] += offset; }

            if(wave.IsTeam) {
                //Add matri position
                movePath.Add(GameConfig.GetRowCollPosition(pathCell));

                // Set enemy position
                eTrans.GetComponent<EnemyMoveBase>()
                    .MoveAppear(movePath,
                        path.MoveSpeed, pathCell,
                        path.Distance * count,
                        () => {
                            eTrans.GetComponent<EnemyMoveBase>().SetCanPush(true);
                            countEnemiesOfWaveMoveFinished++;
                        });
                movePath.RemoveAt(movePath.Count - 1);
            }
            else {
                if(wave.DestroyOnFinish) {
                    eTrans.GetComponent<EnemyMoveBase>().MoveAndDestroyOnFinish(
                        movePath, path.MoveSpeed, pathCell,
                        path.Distance * count, () => {
                            countEnemiesOfWaveMoveFinished++;
                        });
                }
                else {
                    eTrans.GetComponent<EnemyMoveBase>().MoveLoop(movePath, path.MoveSpeed, pathCell, path.Distance * count,
                        () => {
                            countEnemiesOfWaveMoveFinished++;
                        });
                }
            }

            var enemyBase = eTrans.GetComponent<EnemyBase>();
            //enemyBase.InitHp(GameData.Instance.EnemiesData.Datas[eData.EnemyId], hpScale);
            count++;
            enemies.Add(enemyBase.transform);
            yield return Yielder.EndOfFrame;
        }

        yield return new WaitUntil(() => countEnemiesOfWaveMoveFinished == numEnemiesOfWave);
        pathDone();
    }

    public IEnumerator EnemiesFireBullet(WaveData wave) {
        float timer = 0;
        int numberEnemyAttack = 1;
        int countTime = 0;
        int numberTime = wave.CombatDiff.TimeIncreaseNumberFire;
        float timeDelay = wave.FireRate;
        //float fireRateDecrease = GetCombatDiff(wave.CombatDiff.FireRateDecrease);
        //bool isIncreaseNumFire = GetCombatDiff(wave.CombatDiff.IsIncreaseNumberFire);
        WaitForSeconds w = Yielder.Wait(timeDelay);
        WaitForSeconds waitEachFire = Yielder.Wait(0.5f);

        yield return w;
        // Random enemy in list
        //EventDispatcher.Instance.Dispatch(new StructDefine.EnemyAttack());

        var list = enemies.FindAll(e => e.GetComponent<EnemyBase>().GetEnemyData().CanAttack).ToList();
        for(int i = 0; i < list.Count; i++) {
            int index = UnityEngine.Random.Range(0, list.Count);
            if(list[index].gameObject.activeInHierarchy) {
                list[index].GetComponent<EnemyAttackBase>().Init();
            }
            yield return waitEachFire;
            list[i].GetComponent<EnemyBase>().StartAttack();

        }

        //if(timer >= wave.CombatDiff.TimeFireRate) {
        //    timeDelay = timeDelay - fireRateDecrease >= MinTimeFireRate ? timeDelay - fireRateDecrease : MinTimeFireRate;

        //    w = Yielders.Get(timeDelay);

        //    timer = 0;
        //    countTime++;

        //    if(isIncreaseNumFire)
        //        if(countTime >= numberTime) {
        //            numberEnemyAttack++;
        //            countTime = 0;
        //        }
        //}
    }

    private IEnumerator LoadWave(float hpScale, WaveData wave) {

        enemies.Clear();

        if(wave.WaveType == WaveType.Enemy) {
            totalEnemiesOfWave += wave.Paths.Sum(s => s.Cells.Count);
            CalculateItemDrop(totalEnemiesOfWave, wave);

            countPathDone = 0;
            StopCoroutines();
            foreach(var path in wave.Paths) {
                var c = StartCoroutine(LoadPath(hpScale, path, wave, () => {
                    countPathDone++;
                }));
                waveCoroutines.Add(c);
            }

            while(countPathDone < wave.Paths.Count) {
                yield return null;
            }

            if(wave.IsTeam) {
                EnemyMoveHandler.Instance.SetMoveEnd(enemies, wave);
                if(wave.NumEnemyAttack > 0) {
                    enemiesMoveAttackCoroutineWave = StartCoroutine(EnemiesMoveAttack(wave));
                }
            }
            //Debug.Log("Size FireRate :" + wave.FireRate);
            if(wave.FireRate > 0) {
                enemiesFireBulletCoroutine = StartCoroutine(EnemiesFireBullet(wave));
            }
        }
        else if(wave.WaveType == WaveType.Rock) {
            //StopCoroutines();

            ////waveCoroutines.Add(StartCoroutine(SpawnRocks(hpScale, EnemyPool.RockType.Small, wave.Rock.NumSmall, wave.Rock)));
            ////waveCoroutines.Add(StartCoroutine(SpawnRocks(hpScale, EnemyPool.RockType.Normal, wave.Rock.NumNormal, wave.Rock)));
            ////waveCoroutines.Add(StartCoroutine(SpawnRocks(hpScale, EnemyPool.RockType.Big, wave.Rock.NumBig, wave.Rock)));
            //StopWaveCorountine();
            //EnemyMoveHandler.Instance.Stop();
            //EventDispatcher.Instance.Dispatch<GameEvent.OnEndWave>();
            //currentWave += 1;
            //numberEnemiesDie = 0;
            //totalEnemiesOfWave = 0;
            //var timeNotify = UIIngameManager.Instance.uiIngame.ShowNotifyEndWave();
            //DOVirtual.DelayedCall(timeNotify, () => {
            //    if(currentWave >= curLevelData.Waves.Count) {
            //        ManagerPlayer.Instance.OnFinish();
            //    }
            //    else {
            //        StartCoroutine(LoadNormal());
            //    }
            //});
            ////yield return Yielder.Wait(wave.Rock.Duration);
            StopCoroutines();
            totalEnemiesOfWave = wave.Rock.GetTotalRock();
            waveCoroutines.Add(StartCoroutine(SpawnRocks(hpScale,RockType.Small, wave.Rock.NumSmall, wave.Rock)));
            waveCoroutines.Add(StartCoroutine(SpawnRocks(hpScale,RockType.Normal, wave.Rock.NumNormal, wave.Rock)));
            waveCoroutines.Add(StartCoroutine(SpawnRocks(hpScale,RockType.Big, wave.Rock.NumBig, wave.Rock)));
            yield return Yielders.Get(wave.Rock.Duration);
        }
        else if(wave.WaveType == WaveType.GiftBox) {
            StopCoroutines();
            waveCoroutines.Add(StartCoroutine(SpawnGiftBox(wave.GiftBox, wave)));
            yield return Yielder.Wait(wave.GiftBox.Duration);
        }
        else if(wave.WaveType == WaveType.Boss) {
            totalEnemiesOfWave = 1;
            yield return Yielder.Wait(1.0f);
            //totalEnemiesOfWave += 1;
            var bossPrefab = LoadBossResource(wave.Boss.BossIndex);
            Transform boss =  bossPrefab.Spawn().transform;
            boss.position = new Vector3(0, 10f);
            boss.GetComponent<BossBase>().IsLive = false;
            //MainCamera.Instance.ShakeCamera(4.5f);
            boss.DOMove(Vector2.up * 2f, 5f).OnComplete(() => {

                boss.GetComponent<BossBase>().Init
                ();
                boss.GetComponent<BossAttackBase>().Init();
            });

            enemies.Add(boss);

            //UIManager.Instance.GamePage.BossHpBar.InitHpBar(boss.GetComponent<EnemyBase>().CurHp, boss.GetComponent<EnemyBase>().NumberBar);

            if(ManagerGame.Instance.ModePlay == EnumDefine.ModePlay.Normal) {
                //RatingController.Instance.IsBoss = true;
            }
        }
    }

    private void StopCoroutines() {
        if(waveCoroutines.Count <= 0) { return; }
        for(int i = 0; i < waveCoroutines.Count; i++) { StopCoroutine(waveCoroutines[i]); }
        waveCoroutines.Clear();
    }

    protected void StopWaveCorountine() {
        if(enemiesFireBulletCoroutine != null) {
            StopCoroutine(enemiesFireBulletCoroutine);
        }

        if(enemiesMoveAttackCoroutineWave != null) {
            StopCoroutine(enemiesMoveAttackCoroutineWave);
        }

        if(enemiesMoveAttackCoroutine != null) {
            for(int i = 0; i < enemiesMoveAttackCoroutine.Count; i++) {
                StopCoroutine(enemiesMoveAttackCoroutine[i]);
            }
            enemiesMoveAttackCoroutine.Clear();
        }
    }

    private GameObject LoadEnemyResource(int idEnemy) {
        var exist = enemiesPrefab.TryGetValue(idEnemy, out var enemyPrefab);
        if(!exist) {
            var path = string.Format("Prefabs/Enemies/Enemy{0}", idEnemy + 1);
            enemyPrefab = Resources.Load<GameObject>(path);
            enemiesPrefab.Add(idEnemy, enemyPrefab);
        }
        return enemyPrefab;
    }
    private GameObject LoadBossResource(int idBoss) {
        var exist = bossesPrefab.TryGetValue(idBoss, out var bossPrefab);
        if(!exist) {
            var path = string.Format("Prefabs/Bosses/Boss{0}", idBoss);
            bossPrefab = Resources.Load<GameObject>(path);
            bossesPrefab.Add(idBoss, bossPrefab);
        }
        return bossPrefab;
    }
    private GameObject LoadRockResource(int idRock) {
        var exist = rocksPrefab.TryGetValue(idRock, out var rockPrefab);
        if(!exist) {
            var path = string.Format("Prefabs/Rocks/Rock{0}", idRock);
            rockPrefab = Resources.Load<GameObject>(path);
            rocksPrefab.Add(idRock, rockPrefab);
        }
        return rockPrefab;
    }

    private GameObject LoadGiftResource(int idGift) {
        var exist = giftsPrefab.TryGetValue(idGift, out var giftPrefab);
        if(!exist) {
            var path = string.Format("Prefabs/Gifts/Gift{0}", idGift);
            giftPrefab = Resources.Load<GameObject>(path);
            giftsPrefab.Add(idGift, giftPrefab);
        }
        return giftPrefab;
    }



    protected virtual void SetBoss(Transform boss, WaveData wave) {
        boss.GetComponent<BossBase>().StartAttack();
        //boss.GetComponent<Boss>().BossWave = wave;
    }

    public void SpawnerScoreJump(Vector2 position, int score) {
        //var a  = scoreJump.Spawn();
        //a.Init(position, score);
        Debug.Log("Hien thi score jump" + score);
    }

    public IEnumerator EnemiesMoveAttack(WaveData wave) {
        float enemyAtkDecrease = GetCombatDiff(wave.CombatDiff.EnemyAttackDecease);
        float timeDelay = wave.LoopTimeAttack;
        float timer = 0;
        WaitForSeconds w = Yielder.Wait(timeDelay);

        while(true) {
            yield return w;
            timer += timeDelay;
            if(enemiesMoveAttackCoroutine == null)
                enemiesMoveAttackCoroutine = new List<Coroutine>();
            enemiesMoveAttackCoroutine.Add(StartCoroutine(EnemyTurnAttack(wave)));

            //if(GamePlayState.Instance.TypeGame == TypeGame.CombatPower)
            if(timer >= wave.CombatDiff.TimeEnemyAttack) {
                timeDelay = timeDelay - enemyAtkDecrease > MinTimeEnemyAttack ? timeDelay - enemyAtkDecrease : MinTimeEnemyAttack;
                w = Yielder.Wait(timeDelay);
                timer = 0;
            }
        }
    }

    float GetCombatDiff(float baseData) {
        //return GameData.Instance.GetPlayerCp() >=
        //       LevelService.Instance.GetById(GamePlayState.Instance.CurrentLevelSelect).CombatPower ? 0
        //    : baseData;
        return 0;
    }

    public IEnumerator EnemyTurnAttack(WaveData wave) {
        WaitForSeconds w = Yielder.Wait(wave.DistanceTimeAttack);
        EnemyMoveBase eMove;
        int count = 0;
        while(count < wave.NumEnemyAttack) {
            yield return w;
            if(enemies.Count > 0) {
                eMove = enemies[UnityEngine.Random.Range(0, enemies.Count)].GetComponent<EnemyMoveBase>();
                if(!eMove.IsAttacking) {
                    eMove.IsAttacking = true;
                    // Attack
                    eMove.MoveAttackPlayer(Ease.OutQuad);
                }
                count++;
            }
        }
    }

    private void CalculateItemDrop(int number, WaveData wave) {
        int numEnemiesCanHaveItem = wave.Items.NumEnemiesCanHaveItem;
        if(numEnemiesCanHaveItem == 0) { numEnemiesCanHaveItem = number; }
        dropItems = new int[number];
        for(int i = 0; i < number; i++) { dropItems[i] = -1; }

        // Create list item
        List<ItemType> items = new List<ItemType>();
        foreach(var itemWaveData in wave.Items.List) {
            for(int i = 0; i < itemWaveData.Num; i++) {
                items.Add(itemWaveData.Type);
            }
        }

        int ranIndex = UnityEngine.Random.Range(0, number);
        for(int i = 0; i < items.Count; i++) {
            while(dropItems[ranIndex] != -1) {
                ranIndex = UnityEngine.Random.Range(0, number);
            }
            dropItems[ranIndex] = (int)items[i];

        }
    }
    protected virtual void OnDropItem(int dropItemId, Vector3 position) {
        var item = ManagerDataIngame.Instance.Items[dropItemId].Spawn(position);
        item.Init();

        //var item = ItemPool.Instance.SpawnItem();
        //item.transform.position = enemyBase.transform.position;
        //item.SetItem(dropItemId);
        //if(TutorialController.Instance.InTutorialLvl && !TutorialController.Instance.CheckHasKey(TutorialKey.UpPower)) {
        //    TutorialController.Instance.ShowTutorial(TutorialKey.UpPower, targetIngame: item.gameObject);
        //    TutorialController.Instance.SetDetailScale(1.4f);
        //}
    }

    private IEnumerator SpawnGiftBox(GiftBoxWaveData giftBoxData, WaveData wave) {
        var range = 5f;
        //var positionY = GameConfig.Instance.BulletMoveRangY;
        totalEnemiesOfWave = giftBoxData.NumGiftBox;
        float time = giftBoxData.Duration / giftBoxData.NumGiftBox;
        WaitForSeconds w = Yielders.Get(time);
        for(int i = 0; i < giftBoxData.NumGiftBox; i++) {
            var giftBox = LoadGiftResource(0).Spawn(new Vector3(UnityEngine.Random.Range(0, range), 8f)).GetComponent<GiftBase>(); 
            enemies.Add(giftBox.transform);
            giftBox.Init();
            yield return w;
        }
        yield return null;
    }

    public Transform GetEnemyClose() {
        List<Transform> enemiesLife = enemies.FindAll(x=>x.GetComponent<EnemyBase>().IsLive=true && x.gameObject.activeInHierarchy);
        if(enemiesLife.Count<1) {
            return null;
        }
        enemiesLife.OrderBy(x => x.position.y);
        Debug.Log("This is Y of Enemy:"+ enemiesLife.First().position.y);
        //enemiesLife.First().localScale  = Vector3.one * 2f;
        return enemiesLife.First();
    }

    public IEnumerator SpawnRocks(float hpScale, RockType rockType, int number, RockData rockData) {
        var range = Mathf.Abs(rockData.Range);
        float time = rockData.Duration / number;
        Vector3 direct = rockData.EndPosition - rockData.StartPosition;
        //Vector3 angle = new Vector3(0, 0, Mathf.Atan2(direct.y, direct.x) * Mathf.Rad2Deg + 90);
        WaitForSeconds w = Yielders.Get(time);
        for(int i = 0; i < number; i++) {
            var rock = LoadRockResource((int)rockType).Spawn(rockData.StartPosition + new Vector3(UnityEngine.Random.Range(-range, range), 0)).GetComponent<RockBase>();
            rock.Init(hpScale);
            rock.SetMoveSpeed(rockData.Speed);
            enemies.Add(rock.transform);
            yield return w;
        }
        yield return null;
    }
}
