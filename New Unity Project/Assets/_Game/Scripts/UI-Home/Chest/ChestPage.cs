﻿using Gemmob;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ChestPage : Frame {
    private List<ChestItem> listChestNormal = null;
    private List<ChestItem> listChestUtimate = null;
    [SerializeField] private ButtonBase btnBack = null;
    [SerializeField] private RectTransform contentChestNormal = null;
    [SerializeField] private RectTransform contentChestUtimate = null;
    [SerializeField] private ChestItem chestNormalPrefab = null;
    [SerializeField] private ChestItem chestUtimatePrefab = null;
    [SerializeField] private ContainsChestPanel containsChest = null;

    protected override void OnInitialize(HUD hud) {
        base.OnInitialize(hud);
        LoadChestNormal();
        LoadChestUtimate();
        containsChest.Initialize();
        containsChest.gameObject.SetActive(false);
        btnBack.onClick.AddListener(ButtonBack);
    }

    protected override void OnShow(Action onCompleted = null, bool instant = false) {
        base.OnShow(onCompleted, instant);
        UIHomeManager.Instance.SetActiveBot(false);
    }

    protected override void OnHide(Action onCompleted = null, bool instant = false) {
        base.OnHide(onCompleted, instant);
        UIHomeManager.Instance.SetActiveBot(true);
    }

    private void LoadChestNormal() {
        listChestNormal = new List<ChestItem>();
        var data = SmallsData.Instance.chests.chestNormal;
        foreach(var item in data) {
            var itemCreate = chestNormalPrefab.Spawn(contentChestNormal);
            itemCreate.Initialize(this, item, containsChest);
            listChestNormal.Add(itemCreate);
        }
    }

    private void LoadChestUtimate() {
        listChestUtimate = new List<ChestItem>();
        var data = SmallsData.Instance.chests.chestUtimate;
        foreach(var item in data) {
            var itemCreate = chestUtimatePrefab.Spawn(contentChestUtimate);
            itemCreate.Initialize(this, item, containsChest);
            listChestUtimate.Add(itemCreate);
        }
    }

    private void ButtonBack() {
        SFXHome.Instance.PlaySound(SFXHome.Audio.Click_Button_02);
        Hide();
    }
}
