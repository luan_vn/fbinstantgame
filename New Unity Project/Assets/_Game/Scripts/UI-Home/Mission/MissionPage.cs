﻿using Gemmob;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class MissionPage : Frame {
    private MissionTag itemSelected = null;
    private List<MissionItem> listItem = null;
    private Dictionary<QuestType, List<MissionItem>> dtnrMissions = new Dictionary<QuestType, List<MissionItem>>();
    [SerializeField] private TextMeshProUGUI txtNameMission = null;
    [SerializeField] private MissionItem itemPrefab = null;
    [SerializeField] private RectTransform contents = null;
    [SerializeField] private List<MissionTag> listMissionTag = new List<MissionTag>();

    protected override void OnInitialize(HUD hud) {
        base.OnInitialize(hud);
        LoadMission();
        LoadMissionTag();
        OnSelectMission(EnumDefine.TypeMission.Mission);
    }

    private void LoadMissionTag() {
        foreach(var item in listMissionTag) {
            item.Initialize(this);
        }
    }

    private void LoadMission() {
        listItem = new List<MissionItem>();
        EventDispatcher.Instance.AddListener<StructDefine.QuestUpdateValue>(QuestUpdateUI);
        var missions = MissionsData.Instance.missions;
        foreach(var mission in missions) {
            foreach(var quest in mission.quests) {
                var itemCreate = itemPrefab.Spawn(contents);
                itemCreate.transform.localScale = Vector3.one;
                itemCreate.Initialize(this, mission.TYPE, quest);
                listItem.Add(itemCreate);
                var hasQuest = dtnrMissions.TryGetValue(quest.Type, out var listMisisons);
                if(hasQuest) {
                    listMisisons.Add(itemCreate);
                }
                else {
                    dtnrMissions.Add(quest.Type, new List<MissionItem>() { itemCreate });
                }
            }
        }
    }

    private void QuestUpdateUI(StructDefine.QuestUpdateValue quest) {
        var isQuest = dtnrMissions.TryGetValue(quest.type, out var listMissions);
        if(isQuest) {
            foreach(var item in listMissions) {
                item.UpdateUI();
            }
        }
    }

    public void OnSelectMission(EnumDefine.TypeMission type) {
        var item = listMissionTag.Find(x => x.TYPE == type);
        if(item == null) { return; }
        OnSelectMission(item);
    }

    public void OnSelectMission(MissionTag itemSelected) {
        if(itemSelected != null) { itemSelected.OnSelect(true); }
        txtNameMission.text = itemSelected.TYPE.ToString().ToUpper();
        if(this.itemSelected != null) { this.itemSelected.OnSelect(false); }
        this.itemSelected = itemSelected;
        Filter(itemSelected.TYPE);
    }

    private void Filter(EnumDefine.TypeMission type) {
        foreach(var item in listItem) {
            item.gameObject.SetActive(item.TYPE == type);
        }
    }
}
