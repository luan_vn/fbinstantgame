﻿using DG.Tweening;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class SliderChangeOnOff : MonoBehaviour {
    private bool isON = false;
    private Tween tweenOnOff =  null;
    [SerializeField] private Slider sliderOnOff = null;
    [SerializeField] private TextMeshProUGUI txtOnOff = null;
    [SerializeField] private Image imgOnOff = null;
    [SerializeField] private ButtonBase btnChange = null;

    public void Initialize(bool isON) {
        this.isON = isON;
        ChangeOnOff(isON);
        btnChange.AddListener(ButtonChange);
    }

    private void ButtonChange() {
        ChangeOnOff(!isON);
    }

    public void ChangeOnOff(bool ON) {
        tweenOnOff.Kill();
        isON = ON;
        tweenOnOff = sliderOnOff.DOValue(ON ? 1f : 0f, 0.1f).OnComplete(() => {
            txtOnOff.text = ON ? "ON" : "OFF";
            imgOnOff.color = ON ? GameConfig.colorON : GameConfig.colorOFF;
        }).SetUpdate(UpdateType.Normal, true);
    }
}
