﻿using DG.Tweening;
using Gemmob;
using Spine.Unity;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AnimationController : MonoBehaviour
{
    // Start is called before the first frame update
    [SerializeField] protected SkeletonAnimation skeleton;
    [SerializeField,SpineAnimation] protected string idel;
    private Tween tween = null;
    private Coroutine coroutine;

    public void SetSkeleton(SkeletonAnimation Skeleton) {
        skeleton = Skeleton;
    }
    public void Init() {

    }

    public void Init(SkeletonAnimation Skeleton) {

        this.skeleton = Skeleton;
    }

    public virtual float SetIdel() {
        if(!string.IsNullOrEmpty(idel)) {
            return SetAnimation(idel, true);
        }
        return 0f; 
    }

    public virtual float SetAnimation(string animationKey, bool loop = false) {
        //if(isSke) {
            skeleton.state.SetAnimation(0, animationKey, loop);
            return skeleton.state.GetCurrent(0).Animation.Duration;
        //}
        //else {
        //    animator.SetTrigger(animationKey);
        //    return GetTimeAnimation(animationKey);
        //}
    }


    protected virtual void OnValidate() {
        skeleton = GetComponentInChildren<SkeletonAnimation>();   
    }

    public void FlashSkeleTon() {
        if(tween==null && coroutine==null && skeleton != null) {
            coroutine = StartCoroutine(IEFlashSkeleTon());
        }       
    }

    protected IEnumerator IEFlashSkeleTon(float timeDuration = 1f, float timeChange = 0.1f) {
        Color yourColor = skeleton.Skeleton.GetColor();
        yourColor = Color.red;
        yourColor.a = 0.7f;
        bool onFlash = true;
        tween = DOVirtual.DelayedCall(timeDuration, () => { tween = null; onFlash = false; });
        while(onFlash) {
            SetColor(yourColor, timeChange);
            yield return Yielder.Wait(timeChange);
            SetColor(Color.white, 0f);
            yield return Yielder.Wait(timeChange);
        }
        coroutine = null;
    }

    public Tween SetColor(Color colorTarget, float time) {
        if(skeleton != null) {
            Color yourColor = skeleton.Skeleton.GetColor();
            return DOTween.To(() => yourColor, (co) => { skeleton.Skeleton.SetColor(co); }, colorTarget, time);
        }
        return null;
    }
}
