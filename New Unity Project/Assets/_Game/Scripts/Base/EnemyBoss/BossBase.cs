﻿
using Gemmob;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
[RequireComponent(typeof(Rigidbody2D)),
 RequireComponent(typeof(BossAttackBase)),
 RequireComponent(typeof(EnemyMoveBase)),
 RequireComponent(typeof(AnimationController))]
public class BossBase : EnemyBase {
    [SerializeField] protected BossAttackBase bAttack;
    [SerializeField] protected AnimationController animCtrl;
    protected HpBarManager hpBarManager = null;
    protected float perHealth = 1f;
    protected override void Awake() {
        base.Awake();
        animCtrl = GetComponent<AnimationController>();
    }

    [ContextMenu("StartInit")]
    public override void Init() {
        hpBarManager = UIIngameManager.Instance.GetFrame<UIIngamePage>().GetHpBarManager();
        hpBarManager.Init(); // Defaul 1 bar 
        perHealth = 1f;
        base.Init();
    }

    protected override void OnValidate() {
        base.OnValidate();
        bAttack = GetComponent<BossAttackBase>();
    }

    public override void StartAttack() {
        base.StartAttack();
        if(enemyData.CanAttack) {
            bAttack.Init();
        }
    }

    public override void TakeDamage(float Damage) {
        animCtrl.FlashSkeleTon();
        curHP -= Damage;
        if(curHP <= 0 && IsLive) {
            MainCamera.Instance.ShakeCamera(1f);
            EnemyDie();
            return;
        }
        perHealth = curHP / totalHP;
        if(hpBarManager != null) {
            hpBarManager.UpDateInfo(perHealth);
        }

    }

    public override void EnemyDie() {
        hpBarManager.SetActive(false);
        bAttack.KillTween();
        base.EnemyDie();
    }
}
