﻿using DG.Tweening;
using Gemmob;
using Spine.Unity.Playables;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BossAttackBase : MonoBehaviour {
    [SerializeField] protected List<ProcessAttack> processAttacks;
    [SerializeField] protected StyleAttack styleAttack;
    [SerializeField] protected AnimationController anim;
    [SerializeField] public float timeNextProcess=1f;
    [SerializeField] public float speed = 2f;
    [SerializeField] protected float rangeMove = 2f;
    private int indexProcess = 0;
    private bool canMove = true; // canMove of Process Attack
    private float curSpeed = 2f;
    private ProcessAttack process;
    private Tween tween;
    [ContextMenu("StartInit")]
    public virtual void Init() {
        curSpeed = speed;
        runProcess(indexProcess);
    }

    protected void runProcess(int IndexProcess) {
        process =  processAttacks[IndexProcess];
        canMove = process.GetCanMove();
        process.Init();
    }

    public virtual void NextProcess() {
        //set move and anim
        anim.SetIdel();
        canMove = true;

        tween = DOVirtual.DelayedCall(timeNextProcess, () => {
            indexProcess = GetNextIndex(indexProcess);
            runProcess(indexProcess);
        });
    }


    protected virtual int GetNextIndex(int IndexProcess) {
        Debug.Log("Da chay xong process:"+IndexProcess);
        int a = IndexProcess;
        if(StyleAttack.ORDER == styleAttack) {
            a++;
            if(a >= processAttacks.Count) {
                return 0;
            }
            return a;
        }
        else {           
            do {
                a = UnityEngine.Random.Range(0, processAttacks.Count);
            } while(a == indexProcess);
            return a;
        }
    }

    protected virtual void Update() {
        if(canMove) {
            Vector2 pos = transform.position;
            pos.x += curSpeed * Time.deltaTime;
            transform.position = pos;
            if(transform.position.x > rangeMove) {
                curSpeed = speed * -1;
            }
            else if(transform.position.x < -rangeMove) {
                curSpeed = speed;
            }
        }
    }
    protected enum StyleAttack {
        ORDER,
        RANDOM
    }

    private void OnValidate() {
        anim = GetComponent<AnimationController>();
    }

    public ProcessAttack GetProcess() {
        return process;
    }

    public void KillTween() {
        process.KillTween();
        tween.Kill();  
    }


}
