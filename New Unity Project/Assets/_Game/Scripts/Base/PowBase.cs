﻿using DG.Tweening;
using Gemmob;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PowBase : MonoBehaviour {
    [SerializeField] private float speedRadius = 10f;
    [SerializeField] private float speedRotate = -50f;
    [SerializeField] private Transform tf = null;
    [SerializeField] private Collider2DBase coll2D = null;
    [SerializeField] private DamagePerSecendsBase damagePerSecends = null;
    public void Initialize(float timeLive) {
        tf.localPosition = Vector3.zero;
        damagePerSecends.Initialize(coll2D);
        DOVirtual.DelayedCall(timeLive, () => {
            Remove();
        });
    }

    public void Remove() {
        damagePerSecends.Remove();
        gameObject.Recycle();
    }

    private void Update() {
        transform.localEulerAngles += new Vector3(0, 0, speedRotate * Time.deltaTime);
        tf.localPosition += new Vector3(0f, speedRadius * Time.deltaTime, 0f);
    }
}
