﻿using DG.Tweening;
using Gemmob;
using Spine.Unity;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(Rigidbody2D)), RequireComponent(typeof(ShipStatsBase)), RequireComponent(typeof(ShipMovementBase))]
public class ShipBehaviorBase : BehaviorBase {
    protected bool isSuper = false;
    protected GameObject goTail = null;
    protected Tween tweenMaxlevel = null;
    [SerializeField] protected ShipStatsBase shipStats = null;
    [SerializeField] protected ShipColliderBase shipCollider = null;
    [SerializeField] protected ShipMovementBase shipMovement = null;
    [SerializeField] protected GameObject efExplosion = null;
    [Space, Header("Super")]
    [SerializeField] protected GameObject goSuperEf = null;
    [SerializeField] private ParticleSystem psSuperGlowEf = null;
    [SerializeField] private ParticleSystem psSuperLoopEf = null;
    [SerializeField] private ParticleSystem psGetItem = null;


    [Header("FixData")]
    [SerializeField] private EnumDefine.IDBullet idBullet;
    
    [SerializeField]private EnumDefine.TypeAttack typeAttack;

    public ShipColliderBase GetShipCollider() {
        return shipCollider;
    }

    public void Initialized() {
        shipStats.Initialize(this);
        shipCollider.Initialize(this, shipStats);
        shipMovement.Initialize();
        SetLevel(level);
        OnMoveStart();
        SetSupperTime();
        CreateTail();
    }

   
    public void Initialized(bool isProtect) {
        //Start - get protect 
        if(isProtect) {
            //StartCoroutine(GetProtect(3f));
            StartCoroutine(FlashSkeleTon(3f, 0.1f));
            shipCollider.EnebleCollider = false;
            DOVirtual.DelayedCall(3f,()=> { shipCollider.EnebleCollider = true;});
            ActiveSkill(EnumDefine.IDSkill.Shield);
        }
        else {
            OnMoveStart();
        }
        //End
        shipStats.Initialize(this);
        shipCollider.Initialize(this, shipStats);
        shipMovement.Initialize();
        SetLevel(level);
        SetSupperTime();
        CreateTail();
    }

    IEnumerator GetProtect(float timeProtect) {
        
        StartCoroutine(FlashSkeleTon(timeProtect, 0.1f));
        yield return Yielder.Wait(timeProtect);
        shipCollider.EnebleCollider = true;
    }



    public void OnStartAttack() {
        status = StatusShip.Live;
        shipMovement.EnableMove(true);
        Attack();
    }

    public void OnMoveStart() {
        status = StatusShip.Hello;
        var maxSize = MainCamera.Instance.max;
        var target = new Vector3(0f, -maxSize.y + 5f);
        transform.DOMove(target, 1.2f).OnComplete(() => {   
            transform.DOMove(new Vector3(0f, -maxSize.y + 2f), 3f).OnComplete(()=> {ManagerPlayer.Instance.DroneMoveStart();});
        });
    }

    public void OnFinishAttack() {
        status = StatusShip.Finish;
        PauseAttack();
        shipMovement.EnableMove(false);
        DOVirtual.DelayedCall(2f, () => {
            var origin = transform.position;
            var targetDown = origin + Vector3.down * 1.8f;
            transform.DOMove(targetDown, 0.5f).OnComplete(() => {
                var targetUp = new Vector3(origin.x, MainCamera.Instance.max.y + 2f, origin.z);
                transform.DOMove(targetUp, 1f).OnComplete(() => {
                    UIIngameManager.Instance.OnVictoria();
                });
            });
        });
    }

    public void OnDeath() {
        status = StatusShip.Death;
        goTail.Recycle();
        PauseAttack();
        shipMovement.EnableMove(false);
        efExplosion.Spawn(transform.position);
        gameObject.Recycle();
    }

    public void OnRevive() {
        OnMoveStart();
        shipStats.OnRevive();
    }

    protected void SetSupperTime() {
        var timeSuper = rankAttackData.GetAttackRank(rank).timeSuper;
        //ParticleSystem.MainModule mainGlow = psSuperGlowEf.main;
        //mainGlow.duration = timeSuper;
        //mainGlow.startLifetime = timeSuper - 1;
        ParticleSystem.MainModule mainLoop = psSuperLoopEf.main;
        mainLoop.duration = timeSuper;
    }

    protected override void ChangeLevel(int level) {
        if(status == StatusShip.Live) {
            SetLevel(level);
            Logs.Log("Level: " + this.level.ToString());
            UIIngameManager.Instance.GetFrame<UIIngamePage>().SetLevelShip(level);
            ActiveSuper();
            if(isSuper) {

            }
            else {
                PauseAttack();
                Attack();
            }
        }
    }

    protected void ActiveSuper() {
        var active = level + 1 >= MaxLevelInRank();
        if(active) {
            TimeSuper();
            SetLevel(MaxLevelInRank() / 2);
        }
    }

    protected override void SetLevel(int level) {
        base.SetLevel(level);
        UIIngameManager.Instance.GetFrame<UIIngamePage>().SetLevelShip(level);
    }

    protected void CreateTail() {
        var tail = ManagerDataIngame.Instance.GetTailShip((int)IDShip);
        goTail = tail.Spawn(transform);
        goTail.transform.localPosition = Vector3.zero;
    }

    private void TimeSuper() {
        if(isSuper) { StopSuper(); }
        isSuper = true;
        goSuperEf?.SetActive(true);
        tweenMaxlevel = DOVirtual.DelayedCall(5f, () => {
            isSuper = false;
            goSuperEf?.SetActive(false);
            ChangeLevel(level);
        });
    }

    protected void StopSuper() {
        isSuper = false;
        tweenMaxlevel.Kill();
        goSuperEf?.SetActive(false);
    }

    public void UpLevel(bool max = false) {
        if(max) { ChangeLevel(MaxLevelInRank() - 1); }
        else {
            if(level < MaxLevelInRank() - 1) {
                //ParticleSystem.MainModule mainGlow = psSuperGlowEf.main;
                //mainGlow.duration = 2f;
                //mainGlow.startLifetime = 1f;
                ChangeLevel(level + 1);
            }
        }
    }

    protected virtual void Update() {
#if UNITY_EDITOR
        if(Input.GetKeyDown(KeyCode.F1)) {
            UpLevel();
        }
        if(Input.GetKeyDown(KeyCode.F2)) {
            if(rank + 1 < MaxRank()) {
                ChangeRank(rank + 1);
            }
        }
#endif
    }

    //Start_Tung
    public void EffectGetItem() {
        psGetItem.Play();
    }
    //public void GetAttackComponent(ShipAttackerBase shipAttackBase, float time) {
    //    PauseAttack();
    //    var com = shipAttackBase.Spawn(transform);
    //    com.transform.localPosition = Vector3.zero;
    //    com.Initialize(null,null,null);
    //    DOVirtual.DelayedCall(time,()=> { Attack(); com.Recycle();});
    //}
    //End

#if UNITY_EDITOR
    [ContextMenu("Bullet Possition")]
    private void GetDataOlder() {
        for(int i = 0; i < bulletLevelsPositionData.BulletLvlByRank.Count; i++) {
            for(int j = 0; j < bulletLevelsPositionData.BulletLvlByRank[i].LvlPosInfors.Count; j++) {
                for(int k = 0; k < bulletLevelsPositionData.BulletLvlByRank[i].LvlPosInfors[j].Infors.Count; k++) {
                    
                    rankAttackData.ranks[i].levels[j].attacks[0].dataAttack[k].offset.y = 0.6f;
                    if(k == 0) {
                        rankAttackData.ranks[i].levels[j].attacks[0].dataAttack[k].offset.x = 0.3f;
                    }
                    if(k == 1) {
                        rankAttackData.ranks[i].levels[j].attacks[0].dataAttack[k].offset.x = -0.3f;
                    }
                }
            }
            UnityEditor.EditorUtility.SetDirty(rankAttackData);
            UnityEditor.AssetDatabase.SaveAssets();
            UnityEditor.AssetDatabase.Refresh();
        }
    }
    [ContextMenu("SetUpData")]
    private void SetUpData07() {
        for(int i = 0; i < rankAttackData.ranks.Count; i++) {
            for(int j = 0; j < rankAttackData.ranks[i].levels.Count; j++) {
                for(int k = 0; k < rankAttackData.ranks[i].levels[j].attacks.Count; k++) {
                    if(EnumDefine.TypeAttack.Laser == typeAttack) {
                        rankAttackData.ranks[i].levels[j].attacks[k].typeAttack = typeAttack;
                        rankAttackData.ranks[i].levels[j].attacks[k].withBulletLaser = 0.1f + j * 0.05f;
                    }
                    else {
                        rankAttackData.ranks[i].levels[j].attacks[k].typeAttack = typeAttack;
                    }
                    for(int m = 0; m < rankAttackData.ranks[i].levels[j].attacks[k].dataAttack.Count; m++) {
                        rankAttackData.ranks[i].levels[j].attacks[k].dataAttack[m].idBullet = idBullet;

                       
                        if(EnumDefine.TypeAttack.Laser == typeAttack) {
                            if(m == 0) {
                                rankAttackData.ranks[i].levels[j].attacks[k].dataAttack[m].offset.x = 0.2f;
                            }
                            if(m == 1) {
                                rankAttackData.ranks[i].levels[j].attacks[k].dataAttack[m].offset.x = -0.2f;
                            }
                            rankAttackData.ranks[i].levels[j].attacks[k].dataAttack[m].offset.y = 0f;
                        }
                        else {
                            rankAttackData.ranks[i].levels[j].attacks[k].dataAttack[m].offset.y = 0.4f;
                        }
                    }                   
                }
            }
            UnityEditor.EditorUtility.SetDirty(rankAttackData);
            UnityEditor.AssetDatabase.SaveAssets();
            UnityEditor.AssetDatabase.Refresh();
        }
    }
#endif

}
