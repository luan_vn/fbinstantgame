﻿using Gemmob;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class ShipAttackerBase : MonoBehaviour, IAttack {
    protected TypeAttackData attackData = null;
    protected ShipBulletsData bulletData = null;
    protected BehaviorBase ship = null;
    [SerializeField] protected bool isPause = false;
    public abstract EnumDefine.TypeAttack typeAttack { get; }


    public virtual ShipAttackerBase Initialize(TypeAttackData attackData, ShipBulletsData bulletData, BehaviorBase Ship) {
        this.attackData = attackData;
        this.bulletData = bulletData;
        ship = Ship;
        return this;
    }

    public virtual void StartAttack() {
        Fire();
    }

    public virtual void PauseAttack() { 
    
    }

    protected virtual void Fire() {
        if(ship != null) {
            ship.PlayPsFire();
        }
        foreach(var attackData in attackData.dataAttack) {
            if(attackData.timeDelay == 0) {
                CreateBullet(attackData);
            }
            else {
                StartCoroutine(TimeDelay(attackData.timeDelay, attackData));
            }
        }
    }

    protected virtual IEnumerator TimeDelay(float time, AttackData attackData) {
        yield return Yielder.Wait(time);
        CreateBullet(attackData);
    }

    protected virtual void CreateBullet(AttackData attackData) {
        var bulletPrefab = bulletData.GetBullet(attackData.idBullet);
        if(bulletPrefab == null) { return; }
        var origin = transform.position;
        var possition = origin + attackData.offset;
        var bulletCreate = bulletPrefab.Spawn(possition);
        bulletCreate.transform.localEulerAngles = attackData.eulerAngle;
        bulletCreate.Initialized();
    }

    private void OnEnable() {
        StopAllCoroutines();
    }
}
