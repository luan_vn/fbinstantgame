﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using static EnumDefine;

public abstract class ActiveSkillComponentBase : MonoBehaviour {
    public abstract IDSkill idActiveSkill { get; }

    public abstract ActiveSkillComponentBase Initialize();

    public abstract void StartSkill();

    public abstract void PauseSkill();
}
