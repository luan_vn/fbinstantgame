﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using Gemmob;

public class Collider2DBase : MonoBehaviour {
    protected bool collEnable = true;
    protected Action<Collider2D> eventTriggerEnter = null;
    protected Action<Collider2D> eventTriggerExit = null;
    [SerializeField] protected Collider2D coll = null;
    public Bounds bounds => coll.bounds;

    public void AddTriggerEnterListener(Action<Collider2D> action) {
        eventTriggerEnter += action;
    }

    public void RemoveTriggerEnterListener(Action<Collider2D> action) {
        eventTriggerEnter -= action;
    }

    public void AddTriggerExitListener(Action<Collider2D> action) {
        eventTriggerExit += action;
    }

    public void RemoveTriggerExitListener(Action<Collider2D> action) {
        eventTriggerExit -= action;
    }

    public virtual bool EnebleCollider {
        get {
            return collEnable;
        }
        set {
            collEnable = value;
        }
    }

    protected virtual void OnTriggerEnterCollider2D(Collider2D collider2D) {
        eventTriggerEnter?.Invoke(collider2D);
    }

    protected virtual void OnTriggerExitCollider2D(Collider2D collider2D) {
        eventTriggerExit?.Invoke(collider2D);
    }

    protected void OnTriggerEnter2D(Collider2D collision) {
        if(collEnable) { OnTriggerEnterCollider2D(collision); }
    }

    protected void OnTriggerExit2D(Collider2D collision) {
        if(collEnable) { OnTriggerExitCollider2D(collision); }
    }


    protected virtual void OnValidate() {
        coll = GetComponent<Collider2D>();
        //if (coll == null) {
        //    var boxCollider = gameObject.AddComponent<BoxCollider2D>();
        //    boxCollider.size = Vector2.one * 0.5f;
        //    coll = boxCollider;
        //}
    }
}
