﻿using Gemmob;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TestEnemyMove : MonoBehaviour
{
    // Start is called before the first frame update
    [SerializeField] private int enemyID;
    [SerializeField] private int partID;
    [SerializeField] private bool flipXPos;
    [SerializeField] private float speed;

    private GameObject enemy = null;

    [ContextMenu("StartFly")]
    public void StartFly() {
        GameObject enemy = ResourceLogger.Load<GameObject>("Prefabs/Enemies/Enemy" + (enemyID + 1) + "/Enemy" + (enemyID + 1)).Spawn(transform.position);
        var movePath = new List<Vector3>(flipXPos
                    ? PathManager.Instance.Paths[partID].FlipXPositions
                    : PathManager.Instance.Paths[partID].Positions);
        enemy.GetComponent<EnemyMoveBase>().MoveAppear(movePath, speed, new Cell() { Coll = 3,Row = 3}, 0.3f * 0, () => { enemy.GetComponent<EnemyNormal>().Init(); });

    }
}
