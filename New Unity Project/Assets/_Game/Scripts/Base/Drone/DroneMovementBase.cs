﻿
using Gemmob;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DroneMovementBase : MonoBehaviour {
    [SerializeField] private float distancePlayer = 1f;
    [SerializeField] private float speed = 8f;
    protected bool enableMove = false;
    protected EnumDefine.Direct archon = EnumDefine.Direct.Left;


    public virtual void Initialize(EnumDefine.Direct archon) {
        enableMove = false;
        this.archon = archon;
    }

    public void EnableMove(bool enable) {
        enableMove = enable;
    }

    internal virtual Vector3 GetTarget() {
        var shipPossition = ManagerPlayer.Instance.GetShipPossition();
        Vector3 offsetPlayer = archon.ConvertDirect() * distancePlayer;
        Vector3 golTagget ;
        if(shipPossition.view == EnumDefine.View.Hide) {
            golTagget =  transform.position; 
        }
        else {
            golTagget = shipPossition.possition + offsetPlayer; 
        }
        Vector2 sizeCamera = MainCamera.Instance.max;
        golTagget.x = Mathf.Clamp(golTagget.x, -sizeCamera.x+0.5f, sizeCamera.x-0.5f);
        // golTagget.y = Mathf.Clamp(golTagget.y, -sizeCamera.y+0.5f, sizeCamera.y-0.5f);
        return golTagget;
    }

    protected virtual void MoveTarget(Vector3 target) {
        //var origin = transform.position;
        //var velocity = Vector3.zero;
        //transform.position = Vector3.SmoothDamp(origin, target, ref velocity, 0.08f);
        transform.position = Vector3.Lerp(transform.position,target,speed*Time.deltaTime);
    }

    protected virtual void Update() {
        if(enableMove) {
            MoveTarget(GetTarget());
        }
    }
}
