﻿using Gemmob.API.Analytis;
using System;
using System.Collections.Generic;
#if FIREBASE_ENABLE
using Firebase.Analytics;
#endif
using UnityEngine;

public class Events {
    public class ParameterBuilder {
#if FIREBASE_ENABLE
			private readonly List<Parameter> parameters = new List<Parameter>();
#endif

			public static ParameterBuilder Create() {
				return new ParameterBuilder().Add(KeyVersion, Version);
			}

			public ParameterBuilder Add(string parameterName, string parameterValue) {
#if FIREBASE_ENABLE
                if (!string.IsNullOrEmpty(parameterValue)) {
					parameters.Add(new Parameter(parameterName, parameterValue));
				}
#endif
				return this;
			}

			public ParameterBuilder Add(string parameterName, long parameterValue) {
#if FIREBASE_ENABLE
				parameters.Add(new Parameter(parameterName, parameterValue));
#endif
				return this;
			}

			public ParameterBuilder Add(string parameterName, double parameterValue) {
#if FIREBASE_ENABLE
				parameters.Add(new Parameter(parameterName, parameterValue));
#endif
				return this;
			}

#if FIREBASE_ENABLE
			public Parameter[] Build() {
				return parameters.ToArray();
			}
#endif
    }

    public const int DefaultLongValue = 1;
    public const int Version = 1;
    public const string KeyVersion = "version";

    public static void Log(string name, string parameterName, string parameterValue) {
#if FIREBASE_ENABLE
			FirebaseService.Instance.Log(name, parameterName, parameterValue);
#endif
    }

    public static void Log(string name, string parameterName, double parameterValue) {
#if FIREBASE_ENABLE
			FirebaseService.Instance.Log(name, parameterName, parameterValue);
#endif
    }

    private static void LogErrorDebug(Exception e) {
        Debug.LogErrorFormat("Error when LogEvent firebase {0}", e.Message);
    }

    public static void Log(string name, string parameterName, long parameterValue) {
#if FIREBASE_ENABLE
            FirebaseService.Instance.Log(name, parameterName, parameterValue);
#endif
    }

    public static void Log(string name, ParameterBuilder parameterBuilder) {
#if FIREBASE_ENABLE
			FirebaseService.Instance.Log(name, parameterBuilder.Build());
#endif
    }

    public static void Log(string name, string parameterName) {
#if FIREBASE_ENABLE
			FirebaseService.Instance.LogCount(name, parameterName);
#endif
    }

    public static void Log(string name, string parameterName, int parameterValue) {
#if FIREBASE_ENABLE
			FirebaseService.Instance.Log(name, parameterName, parameterValue);
#endif
    }


#if FIREBASE_ENABLE
		public static void Log(string name, params Parameter[] parameters) {
			FirebaseService.Instance.Log(name, parameters);
		}
#endif

    public static void Log(string name) {
#if FIREBASE_ENABLE
			FirebaseService.Instance.Log(name);
#endif
    }


    public static void LogError(string error) {
#if FIREBASE_ENABLE
			if (FirebaseService.Instance.Available) {
				Log("log_error", FirebaseAnalytics.ParameterItemName, error);
			} else {
				Logs.LogWarning("[FIREBASE] Not available when call Events.LogError");
			}
#endif
    }

    public static void LogException(Exception exception) {
#if FIREBASE_ENABLE
			if (FirebaseService.Instance.Available) {
				Log("log_exception", FirebaseAnalytics.ParameterItemName, exception == null ? null : exception.Message);
			} else {
				Logs.LogWarning("[FIREBASE] Not available when call Events.LogException");
			}

#endif
    }

    public static void LogWarning() {
        Log("log_warning");
    }

    public static void LogClickOnUI(string name = null) {
#if FIREBASE_ENABLE
			if (FirebaseService.Instance.Available) {
				Log(FirebaseAnalytics.EventSelectContent, name);
			} else {
				Logs.LogWarning("[FIREBASE] Not available when call Events.LogClickOnUI");
			}
#endif
    }

    public static void LogOpenScreen(string name) {
#if FIREBASE_ENABLE
			FirebaseService.Instance.LogOpenScreen(name);
#endif
    }

    public static void LogUserLogin(string signUpMethod = null) {
#if FIREBASE_ENABLE
			if (FirebaseService.Instance.Available) {
				Log(FirebaseAnalytics.EventLogin, signUpMethod);
			} else {
				Logs.LogWarning("[FIREBASE] Not available when call Events.LogUserLogin");
			}
#endif
    }

    private static void Action(string eventName, string itemName, string category = null, long quantity = 1) {
        Logs.LogFormat("[EVENTS] {0} itemName={1}, category={2}, quantity={3}", eventName, itemName, category,
            category);

#if FIREBASE_ENABLE
			if (FirebaseService.Instance.Available) {
				Log(eventName, ParameterBuilder.Create()
					.Add(FirebaseAnalytics.ParameterItemCategory, category)
					.Add(FirebaseAnalytics.ParameterItemName, itemName)
					.Add(FirebaseAnalytics.ParameterQuantity, quantity)
				);
			} else {
				Logs.LogWarning("[FIREBASE] Not available when call Events.Action");
			}
#endif
    }

    //private static void Action(string eventName, string itemName, Category category, long quantity = 1) {
    //          Action(eventName, itemName, category.ToString().ToLower(), quantity);
    //}

    public static void ItemAction(string action, string itemName, string category = null, long quantity = 1) {
        Action("item_" + action, itemName, category, quantity);
    }

    //public static void ItemAction(string action, string itemName, Category category, long quantity = 1) {
    //	Action("item_" + action, itemName, category, quantity);
    //}

    public static void BuyItem(string itemName, string category = null, long quantity = 1) {
        Action("item_buy", itemName, category, quantity);
    }

    //public static void BuyItem(string itemName, Category category, long quantity = 1) {
    //	Action("item_buy", itemName, category, quantity);
    //}


    public static void SellItem(string itemName, string category = null, long quantity = 1) {
        Action("item_sell", itemName, category, quantity);
    }

    //public static void SellItem(string itemName, Category category, long quantity = 1) {
    //	Action("item_sell", itemName, category, quantity);
    //}

    public static void BuildItem(string itemName, string category = null, long quantity = 1) {
        Action("item_build", itemName, category, quantity);
    }

    //public static void BuildItem(string itemName, Category category, long quantity = 1) {
    //	Action("item_build", itemName, category, quantity);
    //}

    public static void UseItem(string itemName, string category = null, long quantity = 1) {
        Action("item_use", itemName, category, quantity);
    }

    //public static void UseItem(string itemName, Category category, long quantity = 1) {
    //	Action("item_use", itemName, category, quantity);
    //}


    public static void TryItem(string itemName, string category = null, long quantity = 1) {
        Action("item_try", itemName, category, quantity);
    }

    //public static void TryItem(string itemName, Category category, long quantity = 1) {
    //	Action("item_try", itemName, category, quantity);
    //}


    public static void ClickItem(string itemName, string category = null, long quantity = 1) {
        Action("item_click", itemName, category, quantity);
    }

    //public static void ClickItem(string itemName, Category category, long quantity = 1) {
    //	Action("item_click", itemName, category, quantity);
    //}


    public static void UpgradeItem(string itemName, string category = null, long quantity = 1) {
        Action("item_up", itemName, category, quantity);
    }

    //public static void UpgradeItem(string itemName, Category category, long quantity = 1) {
    //	Action("item_up", itemName, category, quantity);
    //}

    public static void RewardItem(string itemName, string category = null, long quantity = 1) {
        Action("item_reward", itemName, category, quantity);
    }

    //public static void RewardItem(string itemName, Category category,
    //	long quantity = 1) {
    //	Action("item_reward", itemName, category, quantity);
    //}

    public static void TutorialBegin(string itemName) {
#if FIREBASE_ENABLE
			if (FirebaseService.Instance.Available) {
				Log(FirebaseAnalytics.EventTutorialBegin, FirebaseAnalytics.ParameterItemName, itemName);
			} else {
				Logs.LogWarning("[FIREBASE] Not available when call Events.TutorialBegin");
			}
#endif
    }

    public static void TutorialComplete(string itemName) {
#if FIREBASE_ENABLE
			if (FirebaseService.Instance.Available) {
				Log(FirebaseAnalytics.EventTutorialComplete, FirebaseAnalytics.ParameterItemName, itemName);
			} else {
				Logs.LogWarning("[FIREBASE] Not available when call Events.TutorialComplete");
			}
#endif
    }

    private static void LogLevel(string eventName, int passed, string levelName, long score = 1,
        string mode = null) {
        Logs.LogFormat("[EVENTS] {0} passed={1}, levelName={2}, score={3}, mode ={4}", eventName, passed, levelName,
            score, mode);
#if FIREBASE_ENABLE
			if (FirebaseService.Instance.Available) {
				Log(eventName, ParameterBuilder.Create()
					.Add(FirebaseAnalytics.ParameterLevelName, levelName)
					.Add(FirebaseAnalytics.ParameterItemCategory, mode)
					.Add(FirebaseAnalytics.ParameterScore, score)
					.Add("passed", passed)
				);
			} else {
				Logs.LogWarning("[FIREBASE] Not available when call Events.LogLevel");
			}
#endif
    }

    public static void LevelStart(string levelName, string mode) {
        LevelStart(levelName, DefaultLongValue, mode);
    }

    public static void LevelStart(string levelName, long score = DefaultLongValue, string mode = null) {
#if FIREBASE_ENABLE
			var numberOfCompletedLevel = Users.GetNumberOfCompletedLevel(levelName);
			if (numberOfCompletedLevel == 0) {
				Users.SetLastLevelStart(levelName);
			}

			Users.SetLevel(levelName);
			if (FirebaseService.Instance.Available) {
				LogLevel(FirebaseAnalytics.EventLevelStart, numberOfCompletedLevel, levelName, score, mode);
			} else {
				Logs.LogWarning("[FIREBASE] Not available when call Events.LogLevel");
			}
#endif
    }

    public static void LevelReplay(string levelName, string mode) {
        LevelReplay(levelName, DefaultLongValue, mode);
    }

    public static void LevelReplay(string levelName, long score = DefaultLongValue, string mode = null) {
        LogLevel("level_replay", Users.GetNumberOfCompletedLevel(levelName), levelName, score, mode);
    }

    public static void LevelGameOver(string levelName, string mode) {
        LevelGameOver(levelName, DefaultLongValue, mode);
    }

    public static void LevelGameOver(string levelName, long score = DefaultLongValue, string mode = null) {
        var numberOfCompletedLevel = Users.GetNumberOfCompletedLevel(levelName);
        if (numberOfCompletedLevel == 0) {
            Users.SetLastLevelFailed(levelName);
        }

        LogLevel("level_failed", numberOfCompletedLevel, levelName, score, mode);
    }

    public static void LevelComplete(string levelName, string mode) {
        LevelComplete(levelName, DefaultLongValue, mode);
    }

    public static void LevelComplete(string levelName, long score = DefaultLongValue, string mode = null) {
#if FIREBASE_ENABLE
			var numberOfCompletedLevel = Users.SetLevelComplete(levelName);
			if (FirebaseService.Instance.Available) {
				LogLevel(FirebaseAnalytics.EventLevelEnd, numberOfCompletedLevel, levelName, score, mode);
			} else {
				Logs.LogWarning("[FIREBASE] Not available when call Events.LevelComplete");
			}
#endif
    }

    public static void CurrencyFlow(string itemName, double price, string currencyName) {
#if FIREBASE_ENABLE
			if (FirebaseService.Instance.Available) {
				Log(price > 0
						? FirebaseAnalytics.EventEarnVirtualCurrency
						: FirebaseAnalytics.EventSpendVirtualCurrency,
					ParameterBuilder.Create()
						.Add(FirebaseAnalytics.ParameterItemName, itemName)
						.Add(FirebaseAnalytics.ParameterValue, price)
						.Add(FirebaseAnalytics.ParameterVirtualCurrencyName, currencyName)
				);
			} else {
				Logs.LogWarning("[FIREBASE] Not available when call Events.CurrencyFlow");
			}

#endif
    }

    public static void LogIap(string eventName, string itemName, string action) {
        Logs.LogFormat("[EVENTS] {0} itemName={1}, action={2}", eventName, itemName, action);

#if FIREBASE_ENABLE
			if (FirebaseService.Instance.Available) {
				Log(eventName, ParameterBuilder.Create()
					.Add(FirebaseAnalytics.ParameterItemName, itemName)
					.Add(FirebaseAnalytics.ParameterItemCategory, action)
				);
			} else {
				Logs.LogWarning("[FIREBASE] Not available when call Events.LogIap");
			}
#endif
    }

    public static void LogIap(string itemName, string action) {
        LogIap("iap", itemName, action);
    }

    public static void LogIapStats(string itemName, string action) {
        LogIap("iap_stats", itemName, action);
    }

    private static void InternalLogAds(string eventName, string control, string type, string action,
        string position) {
        Logs.LogFormat("[EVENTS] {0} control={1}, type={2}, action={3}", eventName, control, type, action);
#if FIREBASE_ENABLE
			if (FirebaseService.Instance.Available) {
				Log(eventName, ParameterBuilder.Create()
					.Add(FirebaseAnalytics.ParameterLocation, position)
					.Add(FirebaseAnalytics.ParameterItemCategory, action)
					.Add(FirebaseAnalytics.ParameterContentType, type)
					.Add(FirebaseAnalytics.ParameterItemName, control)
					.Add(FirebaseAnalytics.ParameterItemName, control)
				);
			} else {
				Logs.LogWarning("[FIREBASE] Not available when call Events.InternalLogAds");
			}
#endif
    }

    public static void LogAds(string control, string type, string action, string position) {
        InternalLogAds("ads", control, type, action, position);
    }

    public static void LogAdsStats(string control, string type, string action, string position) {
        InternalLogAds("ads_stats", control, type, action, position);
    }
}