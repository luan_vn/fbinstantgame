﻿//using Gemmob.API.Ads;
using System;
using System.Diagnostics;
#if ADS_ENABLE
using GoogleMobileAds.Api;
#endif

public partial class Mediation {
    public const string BannerTopCondition = "ADS_BANNER_TOP";
    public const string BannerBottomCondition = "ADS_BANNER_BOTTOM";

#if ADS_ENABLE
    BannerAdmob admobBannerTop, admobBannerBottom;
#else
    //Ad admobBannerTop, admobBannerBottom;
#endif

    [Conditional(BannerTopCondition)]
    private void InitBannerTop() {
#if ADS_ENABLE
        admobBannerTop = new BannerAdmob(AdmobInfo.banner, Ad.Type.BannerTop, AdPosition.Top);
        Logs.Log("[Mediation] BannerTop initialized.");
#endif
    }

    [Conditional(BannerBottomCondition)]
    private void InitBannerBottom() {
#if ADS_ENABLE
        admobBannerBottom = new BannerAdmob(AdmobInfo.banner, Ad.Type.BannerBottom, AdPosition.Bottom);
        Logs.Log("[Mediation] BannerBottom initialized.");
#endif
    }

    [Conditional(BannerTopCondition)]
    public void ShowBannerTop(string position, Action onCompleted = null, Action onFailed = null, float delayTime = 0) {
       // Show(position, admobBannerTop, null, onCompleted, onFailed, delayTime);
    }

    [Conditional(BannerBottomCondition)]
    public void ShowBannerBottom(string position, Action onCompleted = null, Action onFailed = null, float delayTime = 0) {
        //Show(position, admobBannerBottom, null, onCompleted, onFailed, delayTime);
    }


    [Conditional(BannerTopCondition)]
    public void RequestBannerTop() {
        //if (admobBannerTop != null) admobBannerTop.Request();
    }

    [Conditional(BannerBottomCondition)]
    public void RequestBannerBottom() { 
       // if (admobBannerBottom != null) admobBannerBottom.Request();
    }

    [Conditional(BannerTopCondition)]
    public void HideBannerTop() {
        //if (admobBannerTop != null) admobBannerTop.Hide();
    }

    [Conditional(BannerBottomCondition)]
    public void HideBannerBottom() {
       //if (admobBannerBottom != null) admobBannerBottom.Hide();
    }

    public float BannerTopHeight {
        get {
#if ADS_ENABLE && ADS_BANNER_TOP
            return (admobBannerTop != null) ? admobBannerTop.GetHeight() : 0;
#else
            return 0;
#endif
        }
    }

    public float BannerBottomHeight {
        get {
#if ADS_ENABLE && ADS_BANNER_BOTTOM
            return admobBannerBottom != null ? admobBannerBottom.GetHeight() : 0;
#else
            return 0;
#endif
        }
    }

    public bool HasBannerTop {
        get {
#if ADS_ENABLE && ADS_BANNER_TOP
            return admobBannerTop != null && admobBannerTop.IsLoaded;
#else
            Logs.Log("Your project config is not useBanner.");
            return false;
#endif
        }
    }

    public bool HasBannerBottom {
        get {
#if ADS_ENABLE && ADS_BANNER_BOTTOM
            return admobBannerBottom != null && admobBannerBottom.IsLoaded;
#else
            Logs.Log("Your project config is not useBanner.");
            return false;
#endif
        }
    }

}
