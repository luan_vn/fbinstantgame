﻿using UnityEngine.Events;

namespace Gemmob {
    [System.Serializable]
    public class FrameEvent : UnityEvent<Frame> { }
}
