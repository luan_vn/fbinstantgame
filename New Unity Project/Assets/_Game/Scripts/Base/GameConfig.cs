﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameConfig : MonoBehaviour {
    public static float BulletMoveRangX = 3;
    public static float BulletMoveRangY = 3;
    public static int numberNodeX = 7;
    public static int numberNodeY = 7;
    public static float distanceX = 0.8f;
    public static float distanceY = 0.8f;
    public static GameConfig Instance;
    public int NumLevelOfGame = 20;
    public static int MoveMode = 1; //0: center 1: offset
    public static Color colorON = Color.white;
    public static Color colorOFF = new Color(63f/255f, 63f/255f, 63f/255f, 1f);

    public static Color colorLevelCantPlay = new Color(51f/225f, 51f/225f, 51f/225f, 1f);

    public const string PageShowAnim = "Show";
    public const string PageHideAnim = "Hide";

    public static Vector3 customCenter = new Vector3(0f, 0f, 0f);

    public static float Sensitive {
        get { return PlayerPrefs.GetFloat("Sensitive", 1); }
        set { PlayerPrefs.SetFloat("Sensitive", value); }
    }

    public static Vector3 GetRowCollPosition(Cell cell) {
        var x = distanceX * (cell.Coll - numberNodeX/2);
        var y = cell.Row * distanceY;
        return new Vector3(x, y) + customCenter;
    }

    public T ItemPool<T>(T item, List<T> lsItem, Transform parent) where T : MonoBehaviour {
        for(int i = 0; i < lsItem.Count; i++) {
            if(!lsItem[i].gameObject.activeInHierarchy) {
                lsItem[i].gameObject.SetActive(true);
                return lsItem[i];
            }
        }

        T newItem = Instantiate(item, parent);
        newItem.gameObject.SetActive(true);
        lsItem.Add(newItem);
        return newItem;
    }

    public Transform ItemPool(Transform item, List<Transform> lsItem, Transform parent) {
        for(int i = 0; i < lsItem.Count; i++) {
            if(!lsItem[i].gameObject.activeInHierarchy) {
                lsItem[i].gameObject.SetActive(true);
                lsItem[i].SetParent(parent);
                return lsItem[i];
            }
        }

        Transform newItem = Instantiate(item, parent);
        newItem.gameObject.SetActive(true);
        lsItem.Add(newItem);
        return newItem;
    }
}
