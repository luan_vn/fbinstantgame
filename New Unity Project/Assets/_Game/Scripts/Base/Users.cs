﻿using UnityEngine;

namespace Gemmob.API.Analytis {
	public class Users {
		private const string StatsLevelCompleted = "stats_level_end";
		private const string IapCounter = "iap_counter";
		private const string LastLevelStart = "last_level_start";
		private const string Level = "level";
		private const string LastLevelCompleted = "last_level_end";
		private const string LastLevelFailed = "last_level_failed";

		public static int GetNumberOfCompletedLevel(string levelName) {
			var key = string.Join("_", StatsLevelCompleted, levelName);
			var completeValue = PlayerPrefs.GetInt(key, 0);
			return completeValue;
		}


		public static int SetLevelComplete(string levelName) {
			var numberOfCompletedLevel = GetNumberOfCompletedLevel(levelName);
			if (numberOfCompletedLevel == 0) {
				SetUserProperty(LastLevelCompleted, levelName);
			}

			var totalOfCompleted = numberOfCompletedLevel + 1;
			PlayerPrefs.SetInt(string.Join("_", StatsLevelCompleted, levelName), totalOfCompleted);
			return numberOfCompletedLevel;
		}

		public static void SetLevel(string levelName) {
			SetUserProperty(Level, levelName);
		}

		public static void SetLastLevelStart(string levelName) {
			if (GetNumberOfCompletedLevel(levelName) == 0) {
				SetUserProperty(LastLevelStart, levelName);
			}
		}

		public static void SetLastLevelFailed(string levelName) {
			if (GetNumberOfCompletedLevel(levelName) == 0) {
				SetUserProperty(LastLevelFailed, levelName);
			}
		}

		public static void IncrementIapCounter() {
			var counter = PlayerPrefs.GetInt(IapCounter, 0) + 1;
			PlayerPrefs.SetInt(IapCounter, counter);
			SetUserProperty(IapCounter, counter.ToString());
		}

		public static void SetUserProperty(string key, string value) {
			//FirebaseService.Instance.SetUserProperty(key, value);
		}
	}
}