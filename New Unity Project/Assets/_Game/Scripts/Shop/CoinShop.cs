﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Assets._Game.Scripts.Shop
{
    public enum CoinShopType
    {
        Coin, Gem, HotDeal
    }

}