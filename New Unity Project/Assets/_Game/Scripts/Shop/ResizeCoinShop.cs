﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.UI;

public class ResizeCoinShop : MonoBehaviour {
	private Vector2 _contentSize;
	[SerializeField] private GameObject _descriptPanel;
	[SerializeField] private Image _saleEvent;
	[SerializeField] private VerticalLayoutGroup _shopShow;
	[SerializeField] private RectTransform _content, conten;

	[SerializeField] private Text _txtTime;

	[Header("For setting")]
	[SerializeField] private Sprite _tksgivingIcon;
	[SerializeField] private Sprite _xMasIcon;

	[ContextMenu("Init")]
	void Init() {
		_saleEvent = transform.Find("SaleEvent").GetComponent<Image>();
		_shopShow = transform.GetComponent<VerticalLayoutGroup>();
		_content = transform.Find("Content").GetComponent<RectTransform>();
		conten = transform.Find("Content").GetComponent<RectTransform>();

		_txtTime = transform.Find("SaleEvent/txtTime").GetComponent<Text>();
	}

	void Start() {
		//CheckHasSale();
	}

	void ResizeContent(float horizontal, float vertical, bool followScreen = true) {
		if (followScreen) {
			Vector2 curResolution = MainCanvasScaler.Instance.main.referenceResolution;
			if (Mathf.CeilToInt(Screen.width * curResolution.y) != Mathf.CeilToInt(Screen.height * curResolution.x)) {
				_content.SetSizeWithCurrentAnchors(RectTransform.Axis.Horizontal, (Screen.width / curResolution.x) * horizontal);
				_content.SetSizeWithCurrentAnchors(RectTransform.Axis.Vertical, (Screen.height / curResolution.y) * vertical);
				conten.SetSizeWithCurrentAnchors(RectTransform.Axis.Horizontal, (Screen.width / curResolution.x) * horizontal);
				conten.SetSizeWithCurrentAnchors(RectTransform.Axis.Vertical, (Screen.height / curResolution.y) * vertical);
			}
		}
		else {
			_content.SetSizeWithCurrentAnchors(RectTransform.Axis.Horizontal, horizontal);
			_content.SetSizeWithCurrentAnchors(RectTransform.Axis.Vertical, vertical);
			conten.SetSizeWithCurrentAnchors(RectTransform.Axis.Horizontal, horizontal);
			conten.SetSizeWithCurrentAnchors(RectTransform.Axis.Vertical, vertical);
		}
	}

	void SalePackSetting(Sprite titleIcon) {
		_saleEvent.gameObject.SetActive(true);
		_shopShow.childControlHeight = false;
		_saleEvent.sprite = titleIcon;
		_txtTime.text = RemoteConfigExtra.SaleTime;
		ResizeContent(1030f, 1041.7f);
	}

	void CheckHasSale() {
		if (RemoteConfigExtra.SaleCoin == "" && RemoteConfigExtra.SaleGem == "") {
			_saleEvent.gameObject.SetActive(false);
			_shopShow.childControlHeight = true;
		} else if (RemoteConfigExtra.SaleCoin == "20" && RemoteConfigExtra.SaleGem == "30") {
			SalePackSetting(_tksgivingIcon);
		} else if (RemoteConfigExtra.SaleCoin == "30" && RemoteConfigExtra.SaleGem == "40") {
			SalePackSetting(_xMasIcon);
		}

		//for (int i = 0; i < UIManager.Instance.CoinShopPage.LsShopCoinItem.Count; i++) {
		//	UIManager.Instance.CoinShopPage.LsShopCoinItem[i].SetSale();
		//}

	}

	public void SetActiveDescription(bool active) {
		_descriptPanel.SetActive(active);
        StartCoroutine(RemoteConfigExtra.WaitUntilRemoteFetched(()=> {
            if(RemoteConfigExtra.IsVideoDoubleIap) {
                _descriptPanel.GetComponentInChildren<Text>().text = "Double the amount by watching video ads after first time purchase. (Rewarded Video excluded)";
            } else {
                _descriptPanel.GetComponentInChildren<Text>().text = "Double the amount for first time purchase each bundle (Rewarded Video excluded)";
            }

        }));
		if (_contentSize == Vector2.zero) {
			_contentSize = _content.sizeDelta;
			_contentSize = conten.sizeDelta;

		}
		ResizeContent(1030, active ? 1075.5f : _contentSize.y, false);
	}
}
