﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "ShopCoinData", menuName = "Datas/ShopCoinData")]
public class ShopCoinData : ScriptableObject {

    private static ShopCoinData instance;
    public static ShopCoinData Instance {
        get {
            if(instance == null) {
                instance = Resources.Load<ShopCoinData>("ShopCoinData");
                return instance;
            }
            return instance;

        }
    }

    public List<ShopCoinInfor> Coins = new List<ShopCoinInfor>();
    public List<ShopCoinInfor> Gem = new List<ShopCoinInfor>();
    public List<ShopCoinInfor> HotDeal = new List<ShopCoinInfor>();

    public SpecialCoinShop C;
    public SpecialCoinShop G;
    public SpecialCoinShop H;

}

[Serializable]
public class ShopCoinInfor {
    public AllRewardType EarnType;
    public Sprite Icon;
    public string Content;
    public string Iap;
    public int Value;
    public float Price;
    public ShopCoinBuyType BuyType;
}

[Serializable]
public class SpecialCoinShop {
    public Sprite Icon;
    public string Content;
    public string Iap;
    public int Value;
    public float OldPrice;
    public string Sale;
    public float Price;
    public EnumDefine.ShopType TypeShop;
    public ShopCoinBuyType BuyType;
}
//public enum ShopCoinBuyType {
//    Gold, Gem, Dolar, Ads
//}