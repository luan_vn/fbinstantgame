﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using Gemmob.Common;

public class AdsModify {
	public static void ShowRewardVideo(string position, AdsType adsType, string location, string gameMode, string level, Action onAdCompleted = null,
		Action onSkipOrFailed = null) {
		Action newCompleted = () => {
			if (onAdCompleted != null) onAdCompleted();
			ManagerData.Instance.PlayerInfo.AddWatchedAds();
			GameTracking.Instance.TrackVideoAds(adsType, location, "complete", gameMode, level);
			//Tracking.LogRewardAds(adsKey, true);
		};
		Action newSkipOrFailed = () => {
			if (onSkipOrFailed != null) onSkipOrFailed();
			GameTracking.Instance.TrackVideoAds(adsType, location, "fail", gameMode, level);
			//Tracking.LogRewardAds(adsKey, false);
		};
		//Mediation.Instance.ShowRewardVideo(position, newCompleted, newSkipOrFailed);
	}

	public static void ShowInterstitial(string position, string location, string gameMode, string level, Action onAdCompleted = null) {
		if (!ManagerData.IsRemoveAds) {
			Action newCompleted = () => {
				if (onAdCompleted != null) onAdCompleted();
				GameTracking.Instance.TrackVideoAds(AdsType.Interstitial, location, "complete", gameMode, level);
			};
			//Mediation.Instance.ShowInterstitial(position, newCompleted);
		}
	}

	public static void ShowInterstitialWithPrecent(float precent, string position, Action onAdCompleted = null) {
		if (!ManagerData.IsRemoveAds) {
			float random = UnityEngine.Random.Range(0, 99);
			if (random <= precent) {
				Action newCompleted = () => {
					if (onAdCompleted != null) onAdCompleted();

				};
				//Mediation.Instance.ShowInterstitial(position, newCompleted);
			}
		}
	}
}
