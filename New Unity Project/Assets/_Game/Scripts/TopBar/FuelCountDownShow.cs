﻿using Gemmob;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class FuelCountDownShow : MonoBehaviour
{
    public Text TimeShow;

    void Start()
    {
        StartCoroutine(LoadData());
    }

    IEnumerator LoadData()
    {
        while (!CountDownController.Instance)
            yield return Yielders.Get(.05f);

        var countDown = CountDownController.Instance.CountDownFuel;

        countDown.OnTick += () =>
        {
            TimeShow.text = ManagerData.Instance.PlayerInfo.Fuel < PlayerInfo.MaximumFuel ? countDown.GetTime() : "";
        };

        countDown.OnStateChanged += () =>
        {
            TimeShow.gameObject.SetActive(ManagerData.Instance.PlayerInfo.Fuel < PlayerInfo.MaximumFuel);
            TimeShow.text = "";
        };

        TimeShow.gameObject.SetActive(ManagerData.Instance.PlayerInfo.Fuel < PlayerInfo.MaximumFuel);
    }
}
