﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Boss7 : BossBase
{
    // Start is called before the first frame update
    [Header("EnemyCollider 2")]
    [SerializeField] protected EnemyCollider2D enemyCol2D2;

    public override void Init() {
        enemyCol2D2.Init(this);
        base.Init();
    }
}
