﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;
public class Boss11 : BossBase {
    // Start is called before the first frame update
    [SerializeField] protected List<PathEnemy> Paths;
    [SerializeField] protected float timeDrop = 5f;
    protected float preHealthDrop = 0.9f;

    public override void Init() {
        foreach(PathEnemy p in Paths) {
            p.path.transform.localPosition = Vector3.zero;
        }
        preHealthDrop = Paths[0].health;
        base.Init();
    }

    public override void TakeDamage(float Damage) {
        base.TakeDamage(Damage);
        if(perHealth <= preHealthDrop) {
            CheckDropPath();
        }

    }

    private void CheckDropPath() {
        for(int i = 0; i < Paths.Count; i++) {
            if(Paths[i].health >= (curHP / totalHP)) {
                Paths[i].path.transform.DOMove(-MainCamera.Instance.max + Vector2.down * 2f, timeDrop);
                Paths.Remove(Paths[i]);
            }
            else {
                preHealthDrop = Paths[i].health;
                break;
            }
        }
    }
    [System.Serializable]
    public class PathEnemy {
        public GameObject path;
        public float health;
    }
}
