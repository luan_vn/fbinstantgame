﻿using DG.Tweening;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class B6BulletAttack3 : BulletBase
{
    [SerializeField] private float timeTarget = 1.5f;
    protected override void Active() {
        transform.DOMove(ManagerPlayer.Instance.GetShipPossition().possition,1.5f).OnComplete(()=> {RecycleBullet();});
    }
}
