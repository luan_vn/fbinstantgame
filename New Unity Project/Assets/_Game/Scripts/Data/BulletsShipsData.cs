﻿using System.Collections;
using System.Collections.Generic;
using System;
using UnityEngine;

[CreateAssetMenu(fileName = "BulletLevelShip_?", menuName = "Data/ShipsBulletLevel")]
public class BulletsShipsData : ScriptableObject {
    [SerializeField] private List<BulletRankData> ranks = new List<BulletRankData>();

    public BulletRankData GetBulletRank(EnumDefine.Rank rank) {
        var buletRank = ranks.Find(x => x.rank == rank);
        return buletRank == null ? new BulletRankData() : buletRank;
    }
}

[Serializable]
public class BulletRankData {
    public EnumDefine.Rank rank = EnumDefine.Rank.D;
    public List<BulletLevelData> levels = new List<BulletLevelData>();
    public BulletLevelData GetBulletLevel(int level) {
        var bulletLevel = levels.Find(x => x.level == level);
        return bulletLevel == null ? new BulletLevelData() : bulletLevel;
    }
}

[Serializable]
public class BulletLevelData {
    public int level = 0;
    public List<BulletData> bullets = new List<BulletData>();
}

[Serializable]
public class BulletData {
    public bool isParent = false;
    public float delayTime = 0f;
    public Vector3 offset = Vector2.zero;
    public Vector3 eulerAngle = Vector3.zero;
    public ShipBulletBase bulletPrefab = null;
}

