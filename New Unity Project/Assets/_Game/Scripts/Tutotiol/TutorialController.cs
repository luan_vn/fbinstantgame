﻿using System;
using System.Collections.Generic;
using UnityEngine;

namespace Polaris.Tutorial {
	public class TutorialController : MonoBehaviour {
		private static TutorialController _instance;
		private const string _doneKey = "TutorialDoneKeys";
		private const string _tutorialKey = "TutorialKey";
		private const string _stageKey = "TutorialStage";
		private List<string> _lsDoneKey;
		private int _currentStage;
		private bool _isShowingTutorial;
		public bool InTutorialLvl;

        private TutorialKey curretnKey = TutorialKey.None;
		#region Properties

		public static TutorialController Instance {
			get {
				if (_instance == null) {
					GameObject obj = new GameObject(typeof(TutorialController).Name);
					_instance = obj.AddComponent<TutorialController>();
					DontDestroyOnLoad(obj);
				}
				return _instance;
			}
		}

		public bool IsShowingTutorial {
			get { return _isShowingTutorial; }
		}

		#endregion

		void Awake() {
			LoadKey();
			_currentStage = PlayerPrefs.GetInt(_stageKey);
		}

		//public bool ShowTutorial(TutorialKey key, Action endTutorialExtend = null, GameObject targetIngame = null) {
		//	TutorialInfor infor = Array.Find(TutorialUI.Instance.Register.Infors, _ => _.Key == key);
		//	if (infor == null) {
		//		Log(string.Format("[Tutorial Register] Not have key: {0}", key));
		//		return false;
		//	}

		//	if (CheckHasKey(key)) return false;

  //          if(IsInThisTutorial(key)) {
  //              return false;
  //          }

		//	if (CheckHasKey(infor.NotShowWhenHasKey) && infor.NotShowWhenHasKey != TutorialKey.None) {
		//		_lsDoneKey.Add(key.ToString());
		//		SaveKey();
		//		Log(string.Format("[Tutorial Controller] Cant show {0} because {1}", key, infor.NotShowWhenHasKey));
		//		return false;
		//	}
		//	if (CheckHasKey(infor.NeedToDoneKey)) {
		//		_isShowingTutorial = true;
  //              curretnKey = key;
		//		Action endTutorial = () => {
  //                  GameTracking.Instance.TrackingTutorialComplete(infor.Key.ToString());
		//			_lsDoneKey.Add(key.ToString());
		//			_isShowingTutorial = false;
  //                  curretnKey = TutorialKey.None;

  //                  if (infor.SaveEndStage) SaveKey();
		//			if (endTutorialExtend != null) endTutorialExtend.Invoke();
		//		};
		//		if (infor.DescriptInfor.Length < 1) {
		//			//_lsDoneKey.Add(key.ToString());
		//			endTutorial.Invoke();
		//		} else {
  //                  GameTracking.Instance.TrackingTutorialBegin(infor.Key.ToString());
		//			TutorialUI.Instance.ShowTutorial(infor, endTutorial, targetIngame);
  //                  //_lsDoneKey.Add(key.ToString());
		//		}
  //              return true;
		//	} else {
		//		Log(string.Format("[Tutorial Controller] Need to done {0} first!", infor.NeedToDoneKey));
  //              return false;
		//	}
		//}

		public void SetDetailScale(float scale) {
			TutorialUI.Instance.SetDetailScale(scale);
		}

		public bool IsInThisTutorial(TutorialKey key) {
            //TutorialInfor infor = Array.Find(TutorialUI.Instance.Register.Infors, _ => _.Order == _currentStage);
            //if (infor == null) return false;
            //return infor.Key == key;
            return curretnKey == key;

        }

		void LoadKey() {
			string data = PlayerPrefs.GetString(_doneKey);
			if (!data.Equals(string.Empty)) {
				_lsDoneKey = JsonHelper.FromJson<string>(data);
			}
			if (_lsDoneKey == null) {
				_lsDoneKey = new List<string>();
			}

			if (PlayerPrefs.GetInt(_tutorialKey) == 1) {
				PlayerPrefs.DeleteKey(_tutorialKey);
				Array allKey = Enum.GetValues(typeof(TutorialKey));
				for (int i = 0; i < allKey.Length; i++) {
					if ((TutorialKey) allKey.GetValue(i) != TutorialKey.None &&
					    (TutorialKey) allKey.GetValue(i) < TutorialKey.UnlockEndless) {
						_lsDoneKey.Add(allKey.GetValue(i).ToString());
					}
				}
				SaveKey();
			}
		}

		void SaveKey() {
			PlayerPrefs.SetString(_doneKey, JsonHelper.ToJson(_lsDoneKey));
		}

		public bool CheckHasKey(TutorialKey key) {
			if (key == TutorialKey.None) {
				return true;
			}

			return _lsDoneKey.Exists(doneKey => doneKey.Equals(key.ToString()));
		}

		void Log(string message) {
#if UNITY_EDITOR
			Debug.Log(message);
#endif
		}
	}
}
