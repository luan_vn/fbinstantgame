﻿using System;
using UnityEngine;
using UnityEngine.UI;
using DG.Tweening;
using Spine.Unity;
using UnityEngine.Events;

namespace Polaris.Tutorial {
	public class TutorialUI : MonoBehaviour {
		public static TutorialUI Instance;
		public TutorialRegister Register;
		[SerializeField] private Text _txtDescript;
		[SerializeField] private Button _btnScreen;
		[SerializeField] private Image _detail;
		[SerializeField] private TutorialPointer _pointer;
        [SerializeField] private SkeletonGraphic _character;
        [SerializeField] private RectTransform _borderDescription;
		private int _descriptIndex;
		private TutorialInfor _currentInfor;
		private GameObject _trueTarget;
		private GameObject _target;
		private Tween _txtTween;
		private bool _isDoneTween;
		private float _timeScale = 1;

		public bool HasDescription {
			get { return _descriptIndex < _currentInfor.DescriptInfor.Length - 1; }
		}

		public TutorialDescriptInfor CurDescript {
			get { return _currentInfor.DescriptInfor[_descriptIndex]; }
		}

		private void Awake() {
			Instance = this;
		}

		public void ShowTutorial(TutorialInfor infor, Action endTutorialAction, GameObject targetIngame) {
			_btnScreen.onClick.RemoveAllListeners();
			_btnScreen.onClick.AddListener(() => OnScreenClick(endTutorialAction, targetIngame));
			_btnScreen.gameObject.SetActive(true);
			_descriptIndex = 0;
			_currentInfor = infor;
			Pause();
			ShowDescription(endTutorialAction, targetIngame);
		}

		void Pause() {
			if (CurDescript.IsPause) {
				_timeScale = Time.timeScale;
				Time.timeScale = 0;
			}
		}

		#region Highlight
		void CreateHighlight(Action endTutorialAction, GameObject targetIngame) {
			switch (CurDescript.HighlightType) {
				case HighlightType.Target:
					CreateTarget(endTutorialAction, targetIngame);
					_detail.gameObject.SetActive(false);
					_pointer.SetRootPos(CurDescript.PointerPos, _target.transform.position);
					break;
				case HighlightType.Detail:
					CreateTarget(endTutorialAction, targetIngame);
					CreateDetail();
					_pointer.SetRootPos(CurDescript.PointerPos, _target.transform.position);
					break;
				case HighlightType.None:
					if (_target) {
						_target.SetActive(false);
					}
					_detail.gameObject.SetActive(false);
					_pointer.Active(false);
					break;
			}
		}

		void CreateTarget(Action endTutorialAction, GameObject targetIngame) {
			if (CurDescript.TargetType == TargetType.KeepOldTarget || CurDescript.TargetType == TargetType.None) {
				return;
			}
			if (_target) {
				Destroy(_target.gameObject);
			}
			_trueTarget = CurDescript.TargetType == TargetType.GetIngame ? targetIngame : CurDescript.Target;
			var spriteRenderer = _trueTarget.GetComponentsInChildren<SpriteRenderer>();
            if (spriteRenderer != null) {
                if (spriteRenderer.Length == 0) {
                    _target = Instantiate(_trueTarget, _btnScreen.transform);
                } else {
                    _target = Instantiate(_trueTarget, null);
                    foreach (var sprite in _target.GetComponentsInChildren<SpriteRenderer>()) {
                        sprite.sortingLayerName = "UI";
                        sprite.sortingOrder = 100;
                    }
                }
            }
            var skeletonGraphic = _target.GetComponent<SkeletonGraphic>();
            if(skeletonGraphic != null) {
                skeletonGraphic.Initialize(true);
                _trueTarget.GetComponent<SkeletonGraphic>().Initialize(true);
                skeletonGraphic.color = Color.white;
            }
			SetTargetClickEvent(endTutorialAction, targetIngame);
			_target.transform.position = _trueTarget.transform.position;
			_target.SetActive(true);
		}

		void CreateDetail() {
			_detail.sprite = CurDescript.Detail;
			_detail.SetNativeSize();
			_detail.transform.position = _target.transform.position;
			_detail.gameObject.SetActive(true);
		}
        #endregion

        #region Description

        void ShowDescription(Action endTutorialAction, GameObject targetIngame) {
            CreateHighlight(endTutorialAction, targetIngame);
            SetPositionCharacter();

            if (_txtTween != null) {
                _txtTween.Kill(true);
            }
            _isDoneTween = false;
            _txtDescript.text = string.Empty;
            _txtTween = _txtDescript.DOText(CurDescript.Description, 50f, false).SetEase(Ease.Linear).SetSpeedBased(true).SetUpdate(true).OnComplete(() => {
                //_descriptIndex += 1;
                _isDoneTween = true;
            });
            //_descriptIndex += 1;

        }

        void SetPositionCharacter() {
            if(CurDescript.ShowLeft) {
                _character.initialFlipX = true;
                _character.Initialize(true);
                RectTransform rect = _character.GetComponent<RectTransform>();
                rect.anchoredPosition = new Vector2(-950, 0);

                _borderDescription.offsetMin = new Vector2(270, _borderDescription.offsetMin.y);
                _borderDescription.offsetMax = new Vector2(90, _borderDescription.offsetMax.y);
            }
            else {
                _character.initialFlipX = false;
                _character.Initialize(true);

                RectTransform rect = _character.GetComponent<RectTransform>();
                rect.anchoredPosition = new Vector2(-100, 0);

                _borderDescription.offsetMin = new Vector2(0, _borderDescription.offsetMin.y);
                _borderDescription.offsetMax = new Vector2(-180, _borderDescription.offsetMax.y);
            }
        }

		void NextDescription(Action endTutorialAction, GameObject targetIngame) {
			if (_isDoneTween) {
				
			} else {
				_txtTween.Complete(true);
			}
            _descriptIndex += 1;
            ShowDescription(endTutorialAction, targetIngame);
        }

		#endregion

		#region Button event

		void ClickEvent(Action endTutorialAction, GameObject targetIngame, EndType endType, DescriptType descriptType) {
            if (!_isDoneTween && descriptType == DescriptType.TapToNext) {
                _txtTween.Complete(true);
                _isDoneTween = true;
                return;
            }
            if (!HasDescription) {
				if (_currentInfor.EndType == endType) {
					_btnScreen.gameObject.SetActive(false);
					_target.gameObject.SetActive(false);
					Time.timeScale = _timeScale;
					if (endTutorialAction != null) {
						endTutorialAction.Invoke();
					}
				}
			} else {
                
				if (CurDescript.DescriptType == descriptType) {
					NextDescription(endTutorialAction, targetIngame);
				}
			}
		}

		void OnScreenClick(Action endTutorialAction, GameObject targetIngame) {
			ClickEvent(endTutorialAction, targetIngame, EndType.EndDescription, DescriptType.TapToNext);
		}

		void OnTargetClick(Action endTutorialAction, GameObject targetIngame) {
			ClickEvent(endTutorialAction, targetIngame, EndType.ByClick, DescriptType.ClickTarget);
		}

		protected virtual void SetTargetClickEvent(Action endTutorialAction, GameObject targetIngame) {
			//Button targetBtn = _target.GetComponent<Button>();
			//if (!targetBtn) {
			//	targetBtn = _target.AddComponent<Button>();
			//}
			//if (_trueTarget.GetComponent<Button>()) {
			//	targetBtn.onClick = _trueTarget.GetComponent<Button>().onClick;
			//}
			//if (CurDescript.SideBtn) {
			//	targetBtn.onClick = CurDescript.SideBtn.GetComponent<Button>().onClick;
			//}
			//targetBtn.onClick.AddListener(() => OnTargetClick(endTutorialAction, targetIngame));

			ButtonAnimation targetBtn = _target.GetComponent<ButtonAnimation>();
			if (!targetBtn) {
				targetBtn = _target.AddComponent<ButtonAnimation>();
			}
			if (_trueTarget.GetComponent<ButtonAnimation>()) {
				targetBtn.onClick = _trueTarget.GetComponent<ButtonAnimation>().onClick;
			}
			if (CurDescript.SideBtn) {
				targetBtn.onClick = CurDescript.SideBtn.GetComponent<Button>().onClick;
			}
            // Debug.LogError(targetBtn);
            UnityAction action = null;
            action= (() => {
                OnTargetClick(endTutorialAction, targetIngame);
                targetBtn.onClick.RemoveListener(action);

            });

            targetBtn.onClick.AddListener(action);
		}

		#endregion

		#region Other

		public void SetDetailScale(float scale) {
			_detail.transform.localScale = Vector2.one * scale;
		}

		#endregion
	}
}
