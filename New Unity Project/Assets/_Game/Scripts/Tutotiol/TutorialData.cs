﻿using System;
using System.Collections.Generic;
using UnityEngine;

namespace Polaris.Tutorial {

	[CreateAssetMenu(fileName ="TutorialData",menuName ="Datas/TutorialData")]
	public class TutorialData : ScriptableObject {
		public List<TutorialInfor> Data;
	}
	
	[Serializable]
	public class TutorialInfor {
		public TutorialKey Key;
		public int Order;
		public TutorialKey NeedToDoneKey = TutorialKey.None;
		public TutorialKey NotShowWhenHasKey = TutorialKey.None;
		public TutorialDescriptInfor[] DescriptInfor = new TutorialDescriptInfor[0];
		public bool IsSkip = true;
		public bool SaveEndStage = true;
		public EndType EndType;
#if UNITY_EDITOR
		public bool IsShow = true;
#endif
	}

	[Serializable]
	public class TutorialDescriptInfor {
		public string Description;
		public GameObject Target;
		public GameObject SideBtn;
		public Sprite Detail;
		public TargetType TargetType;
		public bool IsPause;
        public bool ShowLeft;
		public HighlightType HighlightType;
		public DescriptType DescriptType;
		public PointerPos PointerPos;
#if UNITY_EDITOR
		public bool IsShow = true;
#endif
	}

	public enum DescriptType {
		TapToNext, ClickTarget
	}

	public enum HighlightType {
		Target, Detail, Both, None
	}

	public enum EndType {
		ByClick, EndDescription, Auto
	}

	public enum TargetType {
		NeedTarget, KeepOldTarget, GetIngame, None
	}

	/// <summary>
	/// Đăng ký key cho tutorial (không được xóa key chỉ được thêm mới)
	/// </summary>
	public enum TutorialKey {
		None = - 1, AutoIngame, ShipCore, UpPower, ShowBomb, EndTutorialLvl,
		ToHangar, UpgradeShip, ToLootBox, OpenBox, UnlockEndless, DetailEndless,
        UnlockTrails, DetailTrails, UnlockSpooky, UnlockSpookyWeekendOnly,
        UnlockSpookyWeekendCountDown, UnlockBossBattle, OpenCard, UseCard
    }
}
