﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class SensitiveScript : MonoBehaviour
{
    [SerializeField]
    private Slider _slider;

    void Awake()
    {
        if (_slider == null)
            _slider = GetComponent<Slider>();
        _slider.minValue = 1;
        _slider.maxValue = 2f;
        _slider.value = GameConfig.Sensitive;

        _slider.onValueChanged.AddListener(arg0 =>
        {
            GameConfig.Sensitive = arg0;
            //if (PlayerController.Instance != null)
            //{
            //    PlayerController.Instance.GetComponent<PlayerMovement>().MaxMoveSpeed = GameConfig.Sensitive;
            //}
        });
    }

    void OnEnable()
    {
        _slider.value = GameConfig.Sensitive;
    }
}
