﻿using Gemmob;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class DailyRewardPage : Frame {
    private List<DailyRewardItem> listItem = null;
    [SerializeField] private ButtonBase btnClose = null;
    [SerializeField] private ButtonAnimation btnClaim = null;
    [SerializeField] private DailyRewardItem itemPrefab = null;
    [SerializeField] private RectTransform container = null;

    protected override void OnInitialize(HUD hud) {
        base.OnInitialize(hud);
        LoadDailyRewards();
        btnClose.onClick.AddListener(ButtonClose);
        btnClaim.onClick.AddListener(ButtonClaim);
    }

    protected override void OnShow(Action onCompleted = null, bool instant = false) {
        base.OnShow(onCompleted, instant);
        UIHomeManager.Instance.SetActiveBot(false);
    }

    protected override void OnHide(Action onCompleted = null, bool instant = false) {
        base.OnHide(onCompleted, instant);
        UIHomeManager.Instance.SetActiveBot(true);
    }

    private void LoadDailyRewards() {
        listItem = new List<DailyRewardItem>();
        var items = DailyRewardData.Instance.Items;
        for(int i = 0; i < items.Count; i++) {
            var itemCreate = itemPrefab.Spawn(container);
            itemCreate.transform.localScale = Vector3.one;
            itemCreate.Initialize(this, i, items[i]);
            listItem.Add(itemCreate);
        }
    }

    public void ButtonClaim() {
        SFXHome.Instance.PlaySound(SFXHome.Audio.Click_Button_02);
        var hasRewards = ManagerData.Instance.dailyRewardInfor.HasRewards(out var day);
        if(hasRewards) {
            var item = listItem.Find(x => x.dayIndex == day);
            if(item == null) {
                Logs.LogError("WTF NULL");
                return;
            }
            Logs.LogError(string.Format("Recive {0}", item.hardData.RewardType));
            ManagerData.Instance.dailyRewardInfor.OnClaimed();
            item.UpdateUI();
        }
        else {
            Logs.Log("DailyRewards Claimed");
        }
    }

    private void ButtonClose() {
        SFXHome.Instance.PlaySound(SFXHome.Audio.Click_Button_02);
        Hide();
    }
}
